webpackJsonp([1,4],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export GAME01_Inputs */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME01; });
var GAME01_Inputs = (function () {
    function GAME01_Inputs() {
        this.situations = [];
        this.locations = [];
        this.personas = [];
    }
    return GAME01_Inputs;
}());

var GAME01 = (function () {
    function GAME01(projectID, sessionID) {
        this.controller = { activeScreen: 1, maxScreens: 1, activePersona: 0 };
        var gameInstance = {
            gameInstanceID: "",
            projectID: projectID,
            sessionID: sessionID,
            type: "GAME01",
            name: "User Story Poems",
            status: "waiting for inputs",
            description: "",
            gameTemplate: "minigame-user-story-poems"
        };
        this.gameInputs = new GAME01_Inputs;
        this.gameResults = [];
        this.gameInstance = gameInstance;
    }
    return GAME01;
}());

//# sourceMappingURL=gameDataModel.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export GAME05_Inputs */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME05; });
var GAME05_Inputs = (function () {
    function GAME05_Inputs() {
        this.userStories = null;
        this.voteCardsAvailable = [50, 10, 10, 10, 5, 5, 5, 5];
    }
    return GAME05_Inputs;
}());

var GAME05 = (function () {
    function GAME05(projectID, sessionID) {
        this.controller = { activeScreen: 1, maxScreens: 1 }; //define the controller data required for this game
        this.activeUsers = null;
        var gameInstance = {
            gameInstanceID: "",
            projectID: projectID,
            sessionID: sessionID,
            type: "GAME05",
            name: "User Story Silent Auction",
            status: "waiting for inputs",
            description: "",
            gameTemplate: "minigame-user-story-silent-auction"
        };
        this.gameInputs = new GAME05_Inputs;
        this.gameResults = [];
        this.gameInstance = gameInstance;
    }
    return GAME05;
}());

//# sourceMappingURL=gameDataModel.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export GAME03_Inputs */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME03; });
var GAME03_Inputs = (function () {
    function GAME03_Inputs() {
        this.userStories = null;
    }
    return GAME03_Inputs;
}());

var GAME03 = (function () {
    function GAME03(projectID, sessionID) {
        this.controller = { activeScreen: 1, maxScreens: 1, timerOn: false }; //define the controller data required for this game
        this.activeUsers = null;
        var gameInstance = {
            gameInstanceID: "",
            projectID: projectID,
            sessionID: sessionID,
            type: "GAME03",
            name: "User Story Tinder",
            status: "waiting for inputs",
            description: "",
            gameTemplate: "minigame-user-story-tinder"
        };
        this.gameInputs = new GAME03_Inputs;
        this.gameResults = [];
        this.gameInstance = gameInstance;
    }
    return GAME03;
}());

//# sourceMappingURL=gameDataModel.js.map

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiskTypeAtrifacts; });
var RiskTypeAtrifacts = (function () {
    function RiskTypeAtrifacts() {
        this.artifacts = this.getArtifacts();
    }
    RiskTypeAtrifacts.prototype.getArtifacts = function () {
        this.artifacts = [
            { name: 'Budget', avatarUrl: 'http://artifacts.playworkshops.com.au/risk-avatars/budget.png' },
            { name: 'Stakeholders', avatarUrl: 'http://artifacts.playworkshops.com.au/risk-avatars/stakeholders.png' },
            { name: 'Management', avatarUrl: 'http://artifacts.playworkshops.com.au/risk-avatars/management.png' },
            { name: 'Timeline', avatarUrl: 'http://artifacts.playworkshops.com.au/risk-avatars/timeline.png' },
        ];
        return this.artifacts;
    };
    return RiskTypeAtrifacts;
}());

//# sourceMappingURL=risks.component.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MAXI01_Inputs */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MAXI01; });
var MAXI01_Inputs = (function () {
    function MAXI01_Inputs() {
        this.RFIDhotspots = [];
    }
    return MAXI01_Inputs;
}());

var MAXI01 = (function () {
    function MAXI01(projectID, sessionID) {
        this.controller = { activeScreen: 1, maxScreens: 1, activeCards: [] };
        var gameInstance = {
            gameInstanceID: "",
            projectID: projectID,
            sessionID: sessionID,
            type: "MAXI01",
            name: "Roadmap Explorer",
            status: "waiting for setup to complete",
            description: "",
            gameTemplate: "maxigame-roadmap-explorer"
        };
        this.gameInputs = new MAXI01_Inputs;
        this.gameResults = null;
        this.gameInstance = gameInstance;
    }
    return MAXI01;
}());

//# sourceMappingURL=gameDataModel.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__maxigame_roadmap_explorer_controller_controller_component__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__maxigame_roadmap_explorer_interface_maxigame_component__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__maxigame_roadmap_explorer_master_master_component__ = __webpack_require__(311);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaxigameModule", function() { return MaxigameModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




//Minigames



var routes = [
    //MINIGAMES
    { path: 'maxigame-roadmap-explorer', component: __WEBPACK_IMPORTED_MODULE_6__maxigame_roadmap_explorer_master_master_component__["a" /* MAXI01MasterComponent */], pathMatch: 'full' },
];
var MaxigameModule = (function () {
    function MaxigameModule() {
    }
    return MaxigameModule;
}());
MaxigameModule.routes = routes;
MaxigameModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["c" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forChild(routes),
        ],
        declarations: [
            // Minigames
            __WEBPACK_IMPORTED_MODULE_4__maxigame_roadmap_explorer_controller_controller_component__["a" /* MAXI01ControllerComponent */],
            __WEBPACK_IMPORTED_MODULE_5__maxigame_roadmap_explorer_interface_maxigame_component__["a" /* MAXI01interfaceComponent */],
            __WEBPACK_IMPORTED_MODULE_6__maxigame_roadmap_explorer_master_master_component__["a" /* MAXI01MasterComponent */],
        ],
        providers: [],
    })
], MaxigameModule);

//# sourceMappingURL=maxigame.module.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__system_gameFooter_game_footer_component__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_hammerjs__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__minigame_user_story_poems_controller_controller_component__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__minigame_user_story_poems_game_minigame_component__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__minigame_user_story_poems_master_master_component__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__minigame_user_story_tinder_controller_controller_component__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__minigame_user_story_tinder_game_minigame_component__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__minigame_user_story_tinder_master_master_component__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__minigame_user_story_silent_auction_master_master_component__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__minigame_user_story_silent_auction_controller_controller_component__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__minigame_user_story_silent_auction_game_minigame_component__ = __webpack_require__(316);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MinigameModule", function() { return MinigameModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






//Minigames









var routes = [
    //MINIGAMES
    { path: 'minigame-user-story-poems', component: __WEBPACK_IMPORTED_MODULE_8__minigame_user_story_poems_master_master_component__["a" /* GAME01MasterComponent */], pathMatch: 'full' },
    { path: 'minigame-user-story-tinder', component: __WEBPACK_IMPORTED_MODULE_11__minigame_user_story_tinder_master_master_component__["a" /* GAME03MasterComponent */], pathMatch: 'full' },
    { path: 'minigame-user-story-silent-auction', component: __WEBPACK_IMPORTED_MODULE_12__minigame_user_story_silent_auction_master_master_component__["a" /* GAME05MasterComponent */], pathMatch: 'full' },
];
var MinigameModule = (function () {
    function MinigameModule() {
    }
    return MinigameModule;
}());
MinigameModule.routes = routes;
MinigameModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["c" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forChild(routes),
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__system_gameFooter_game_footer_component__["a" /* GameFooterComponent */],
            // Minigames
            __WEBPACK_IMPORTED_MODULE_6__minigame_user_story_poems_controller_controller_component__["a" /* GAME01ControllerComponent */],
            __WEBPACK_IMPORTED_MODULE_7__minigame_user_story_poems_game_minigame_component__["a" /* GAME01gameComponent */],
            __WEBPACK_IMPORTED_MODULE_8__minigame_user_story_poems_master_master_component__["a" /* GAME01MasterComponent */],
            __WEBPACK_IMPORTED_MODULE_9__minigame_user_story_tinder_controller_controller_component__["a" /* GAME03ControllerComponent */],
            __WEBPACK_IMPORTED_MODULE_10__minigame_user_story_tinder_game_minigame_component__["a" /* GAME03gameComponent */],
            __WEBPACK_IMPORTED_MODULE_11__minigame_user_story_tinder_master_master_component__["a" /* GAME03MasterComponent */],
            __WEBPACK_IMPORTED_MODULE_13__minigame_user_story_silent_auction_controller_controller_component__["a" /* GAME05ControllerComponent */],
            __WEBPACK_IMPORTED_MODULE_14__minigame_user_story_silent_auction_game_minigame_component__["a" /* GAME05gameComponent */],
            __WEBPACK_IMPORTED_MODULE_12__minigame_user_story_silent_auction_master_master_component__["a" /* GAME05MasterComponent */]
        ],
        providers: [],
    })
], MinigameModule);

//# sourceMappingURL=minigame.module.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(34);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HoldingScreenComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HoldingScreenComponent = (function () {
    function HoldingScreenComponent(service, router) {
        this.service = service;
        this.router = router;
        this.showSpawnScreen = false;
        this.gamesForSession = [];
        this.showUtilityLoader = false;
    }
    HoldingScreenComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub_sessionLoader = this.service._SESSION_LIVE.subscribe(function (res) {
            _this.SESSION_LIVE_STREAM = res[0];
            if (_this.service.logonData.userType == 'admin') {
                _this.loadGameList();
            }
        });
    };
    HoldingScreenComponent.prototype.loadGameList = function () {
        var _this = this;
        this.sub_gameLoader = this.service.getGamesForSession(this.SESSION_LIVE_STREAM._id).subscribe(function (res) {
            _this.gamesForSession = res.reverse();
        });
    };
    HoldingScreenComponent.prototype.updateActivegameInstance = function (gameInstanceID) {
        this.service.screenLockout = { lock: true, message: "Updating active game..." };
        this.service.updateActiveGame(gameInstanceID, this.service.logonData.sessionID);
    };
    HoldingScreenComponent.prototype.ngOnDestroy = function () {
        if (this.sub_sessionLoader) {
            this.sub_sessionLoader.unsubscribe();
        }
        if (this.sub_gameLoader) {
            this.sub_gameLoader.unsubscribe();
        }
    };
    return HoldingScreenComponent;
}());
HoldingScreenComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'holding-screen',
        template: __webpack_require__(517),
        styles: [__webpack_require__(495)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object])
], HoldingScreenComponent);

var _a, _b;
//# sourceMappingURL=holding-screen.component.js.map

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(34);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = (function () {
    function LoginComponent(service, router, route) {
        this.service = service;
        this.router = router;
        this.route = route;
        this.validationInProgress = false;
        this.loginFailure = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        console.log("SENT ACCESSKEY FOR LOGIN:", this.route.snapshot.params['accessKey']);
        if (this.route.snapshot.params['accessKey']) {
            this.doLogin(this.route.snapshot.params['accessKey']);
        }
    };
    LoginComponent.prototype.doLogin = function (inputKey) {
        var _this = this;
        // TODO security validation check
        this.validationInProgress = true;
        this.sub_login = this.service.doLogin_S(inputKey).subscribe(function (res) {
            if (res[0] && res[0].sessionID) {
                _this.router.navigate(['/waiting']);
                _this.service.startSessionLoop(res[0].sessionID); // Start the session polling loops
                _this.service.userType = res[0].userType; // populate the shares service user data
                _this.service.logonData = res[0];
                if (_this.service.debug) {
                    console.log("USER LOGIN", res[0], _this.service.userType);
                }
            }
            else {
                _this.validationInProgress = false;
                _this.loginFailure = true;
                console.log("Login Failue", _this.loginFailure);
            }
            ;
        });
    };
    LoginComponent.prototype.ngOnDestroy = function () {
        if (this.sub_login) {
            this.sub_login.unsubscribe();
        }
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-login',
        template: __webpack_require__(518),
        styles: [__webpack_require__(496)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* ActivatedRoute */]) === "function" && _c || Object])
], LoginComponent);

var _a, _b, _c;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__managePersonas_utillity_component__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__manageRisks_utility_component__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__manageGoals_utility_component__ = __webpack_require__(324);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UtillitiesModule", function() { return UtillitiesModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




//Utillities



var routes = [
    //UTILLITIES
    { path: 'personas', component: __WEBPACK_IMPORTED_MODULE_4__managePersonas_utillity_component__["a" /* PersonaUtilityComponent */], pathMatch: 'full' },
    { path: 'risks', component: __WEBPACK_IMPORTED_MODULE_5__manageRisks_utility_component__["a" /* RisksUtilityComponent */], pathMatch: 'full' },
    { path: 'goals', component: __WEBPACK_IMPORTED_MODULE_6__manageGoals_utility_component__["a" /* GoalsUtilityComponent */], pathMatch: 'full' },
];
var UtillitiesModule = (function () {
    function UtillitiesModule() {
    }
    return UtillitiesModule;
}());
UtillitiesModule.routes = routes;
UtillitiesModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["c" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forChild(routes),
        ],
        declarations: [
            // Utillities
            __WEBPACK_IMPORTED_MODULE_4__managePersonas_utillity_component__["a" /* PersonaUtilityComponent */],
            __WEBPACK_IMPORTED_MODULE_5__manageRisks_utility_component__["a" /* RisksUtilityComponent */],
            __WEBPACK_IMPORTED_MODULE_6__manageGoals_utility_component__["a" /* GoalsUtilityComponent */],
        ],
        providers: [],
    })
], UtillitiesModule);

//# sourceMappingURL=utillities.module.js.map

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(34);
/* unused harmony export liveGameData */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjectService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var liveGameData = (function () {
    function liveGameData(liveGameInputs, liveGameResults, liveGameController) {
        this.liveGameInputs = null;
        this.liveGameResults = "Open";
        this.liveGameController = "";
        this.liveGameInputs = liveGameInputs;
        this.liveGameResults = liveGameResults;
        this.liveGameController = liveGameController;
    }
    return liveGameData;
}());

var ProjectService = (function () {
    function ProjectService(http, router) {
        this.http = http;
        this.router = router;
        this.debug = true; // Controll the out put of debug info to the console
        this._SESSION_LIVE = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["ReplaySubject"](1);
        this._GAME_LIVE = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["ReplaySubject"](1);
        this.options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestOptions */]({ withCredentials: true });
        this.userType = "undefined";
        this.serviceData = { sessionCall: 0, gameCall: 0 }; // 0 do not call service, 1 ready to call, 2 call in progress
        this.systemMessage = { latestMessage: "System messages appear here", latestMessageTime: 0, gameDataLoaded: false, sessionDataLoaded: false }; //holds the systel messages fro the admin top strap
        this.screenLockout = { lock: false, message: "Processing Data" }; //lock the screen for data saves. Lock is activated through app.comnent.html
        this.chartColourSet = { dark: ["#4c2c50", "#0089cb", "#f15f2d", "#fbb03f", "#35a899", "#f4decb"], light: ["rgba(76,44,80, .5)", "rgba(0,137,203, .5)", "rgba(241,95,45,.5)", "rgba(251,176,63,.5)", "rgba(53,168,153, .5)", "rgba(248,238,231,.5)"] };
    }
    ProjectService.prototype.postSystemMessage = function (message) {
        this.systemMessage.latestMessage = message;
        this.systemMessage.latestMessageTime = Math.round((new Date()).getTime() / 1000); // Unix time
    };
    // SESSION CALLS
    ProjectService.prototype.doLogin_S = function (accessKey) {
        var postData = { "accessKey": accessKey };
        this.postSystemMessage("Session authentication in progress...");
        return this.http.post(this.sessionAPIurl + "/doLogin", postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    //LIVE DATA CALLS
    ProjectService.prototype.startSessionLoop = function (sessionID) {
        var _this = this;
        this.serviceData.sessionCall = 1;
        this.serviceData.gameCall = 1; // Allow the service ang Game loops
        this.sessionLoopCheck(true, sessionID); // Initial data call
        __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].interval(6000).subscribe(function () { return _this.sessionLoopCheck(false, sessionID); }); // Start the polling loop
    };
    ProjectService.prototype.sessionLoopCheck = function (forceUpdate, sessionID) {
        var _this = this;
        this.systemMessage.gameDataLoaded = false;
        this.systemMessage.sessionDataLoaded = false; //reset data loaded visual indicators
        // Loop for session data
        if (this.serviceData.sessionCall == 1 || forceUpdate == true) {
            this.serviceData.sessionCall = 2; // set session call to in progress and disallow and new calls;
            this.getSessionControllerData_S(sessionID).subscribe(function (res) {
                if (_this.serviceData.sessionCall != 0) {
                    _this._SESSION_LIVE.next(res);
                }
                ;
                ; // To prevent slow call overlapping a stop to the service update
                _this.systemMessage.sessionDataLoaded = true;
                _this.serviceData.sessionCall = 1; //ready;
                if (res[0].activeGameInstance) {
                    _this.gameLoopCheck(res[0].activeGameInstance, true); //Load the game instance data is there is an active game intance in the session
                }
                else {
                    if (_this.serviceData.sessionCall != 0) {
                        _this._GAME_LIVE.next([]);
                    } //else make sure the data is cleared so the game screens reset
                }
            });
        }
    };
    ProjectService.prototype.gameLoopCheck = function (gameInstandID, forceUpdate) {
        var _this = this;
        if (this.serviceData.gameCall == 1 || forceUpdate == true) {
            this.serviceData.gameCall = 2; //in progress;
            this.getGameControllerData_S(gameInstandID).subscribe(function (res) {
                if (_this.serviceData.gameCall != 0) {
                    _this._GAME_LIVE.next(res);
                }
                ; // To prevent slow call overlapping a stop to the 
                _this.screenLockout.lock = false;
                _this.systemMessage.gameDataLoaded = true;
                _this.serviceData.gameCall = 1; //ready;
            });
        }
    };
    ProjectService.prototype.getSessionControllerData_S = function (sessionID) {
        // ...using get request
        return this.http.get(this.sessionAPIurl + "/getSessionController/sessionID/" + sessionID, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.getGameControllerData_S = function (activeGameInstanceID) {
        // ...using get request
        return this.http.get(this.sessionAPIurl + "/getGameController/gameID/" + activeGameInstanceID, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    //PROJECT ADMIN CALLS
    ProjectService.prototype.spawnProject_S = function (data) {
        this.postSystemMessage("spawning a new Game: " + data);
        return this.http.post(this.sessionAPIurl + "/createproject/", { "data": data }, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.getProject = function (projectID) {
        return this.http.post(this.sessionAPIurl + "/getProject", { "projectID": projectID }, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.saveResultsToProject = function (resultData, projectID) {
        var _this = this;
        this.screenLockout = { lock: true, message: "Saving data to project..." + projectID };
        this.postSystemMessage("Saving data to project world: " + projectID);
        this.saveResultsToProject_S(resultData, projectID).subscribe(function (res) {
            console.log("SAVED PROJECT DATA:", projectID, resultData);
            _this.postSystemMessage("Data saved to game world: " + projectID);
            _this.screenLockout.lock = false;
        });
    };
    ProjectService.prototype.saveResultsToProject_S = function (resultData, projectID) {
        this.postSystemMessage("Saving data to project world: " + projectID);
        return this.http.post(this.sessionAPIurl + "/storeResultsToProject", { "resultData": resultData, "projectID": projectID }, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    // GAME ADMIN CALLS
    ProjectService.prototype.updateGameControllerData = function (newGameControllerData, gameInstanceID, sessionID) {
        var _this = this;
        console.log(newGameControllerData);
        //this.controllerServiceData.gameDATA.controller = newGameControllerData // This overides the loacl varible to provide immeidate feeback to the user until the service updates
        this.updateGameControllerData_S(newGameControllerData, gameInstanceID).subscribe(function (res) {
            _this.postSystemMessage("New game controller data sent");
        });
    };
    ProjectService.prototype.updateActiveGame = function (newGameInstance, sessionID) {
        var _this = this;
        this.updateActiveGameInstance_S(newGameInstance, sessionID).subscribe(function (res) {
            _this.postSystemMessage("New game active, sending update to session devices");
            _this._GAME_LIVE.next([]); // clear the lcoal varible also sot eh screens reset
            _this.screenLockout.lock = false;
            _this.sessionLoopCheck(true, sessionID); // call an update to controllers
            _this.serviceData.sessionCall = 1; // Stop the api calls interfeering with the load
            _this.serviceData.gameCall = 1; // Stop the api calls interfeering with the load
        });
    };
    ProjectService.prototype.saveGameInputData_S = function (gameInputData, gameID) {
        var postData = { "gameInputs": gameInputData, gameID: gameID };
        this.postSystemMessage("Saving new game input data...");
        return this.http.post(this.sessionAPIurl + "/updateGameInputs", postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw('Server error'); });
    };
    ProjectService.prototype.exitGame = function () {
        this.screenLockout = { lock: true, message: "Closing game..." };
        this.updateActiveGame(null, this.logonData.sessionID);
        this.router.navigate(['waiting']);
    };
    ProjectService.prototype.spawnGame = function (gameData, sessionID) {
        var _this = this;
        this.screenLockout = { lock: true, message: "Spawning Game..." };
        this.spawnGame_S(gameData, sessionID).subscribe(function (res) {
            gameData.gameInstance.gameInstanceID = res['insertedIds'][0];
            _this.screenLockout.lock = false;
            _this.postSystemMessage("Game spawned" + gameData.gameInstance.gameInstanceID);
            // add game to session in database and update the active game instance
            _this.addGameToSession_S(gameData.gameInstance.gameInstanceID, gameData.gameInstance.type, null, gameData.gameInstance.description, sessionID).subscribe(function (res) {
                _this.postSystemMessage("Onboarding game (" + res[0] + ") now active, sending update to session devices");
                //this.updateActiveGame(gameData.gameInstance.gameInstanceID, sessionID)
            });
        });
    };
    ProjectService.prototype.spawnGame_S = function (gameData, sessionID) {
        this.postSystemMessage("spawning a new Game: " + gameData.gameInstance.name);
        return this.http.post(this.sessionAPIurl + "/createGame/sessionID/" + sessionID, { "gameData": gameData }, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.addGameToSession_S = function (gameInstanceID, gameTemplate, dateAddedToSession, gameDescription, sessionID) {
        this.postSystemMessage("Adding game to session: " + gameInstanceID);
        return this.http.post(this.sessionAPIurl + "/addGameToSession/sessionID/" + sessionID, { "gameInstanceID": gameInstanceID, "gameTemplate": gameTemplate, "dateAdded": dateAddedToSession, "gameDescription": gameDescription }, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.updateActiveGameInstance_S = function (newGameInstance, sessionID) {
        var postData = { "newGameInstance": newGameInstance };
        this.postSystemMessage("Game update in progress...");
        return this.http.post(this.sessionAPIurl + "/updateSessionActiveGameInstance/sessionID/" + sessionID, postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.spawnSession = function (reqProjectID, sessionObject) {
        var postData = { projectID: reqProjectID };
        return this.http.post(this.sessionAPIurl + "/createSession/sessionID/" + sessionObject.sessionInstance.sessionID, postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.storeGameResultToSession = function (resultData, sessionID) {
        var _this = this;
        this.screenLockout = { lock: true, message: "Saving results to session store..." };
        this.serviceData.sessionCall = 0; // Stop the api calls interfeering with the load
        this.serviceData.gameCall = 0; // Stop the api calls interfeering with the load
        this.storeGameResultToSession_S(resultData, sessionID).subscribe(function (res) {
            _this.postSystemMessage("Saving game results to session");
            _this.screenLockout.lock = false;
            console.log("Saved game results to session", resultData, res);
            _this.sessionLoopCheck(true, sessionID); // call an update to controllers to restart the session heartbeat
        });
    };
    ProjectService.prototype.storeGameResultToSession_S = function (resultData, sessionID) {
        var postData = { "data": resultData };
        this.postSystemMessage("Game results save in Progress...");
        return this.http.post(this.sessionAPIurl + "/saveToSessionStore/sessionID/" + sessionID, postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.updateGameScreen = function (newScreenID, sessionObject) {
        var _this = this;
        this.serviceData.sessionCall = 0; // Stop the api calls interfeering with the load
        this.serviceData.gameCall = 0; // Stop the api calls interfeering with the load
        this.setLoadingFlags(false, true, sessionObject); // Set the flags for the laoding screens
        //this.serviceData.gameDATA.controller.activeScreen = newScreenID  // This overides the loacl varible to provide immeidate feeback to the user until the service updates
        this.setLoadingFlags(false, true, sessionObject);
        this.updateGameScreen_S(newScreenID, sessionObject).subscribe(function (res) {
            _this.postSystemMessage("New screen active: " + newScreenID);
            _this.sessionLoopCheck(true, sessionObject); // call an update to controllers
        });
    };
    ProjectService.prototype.updateGameScreen_S = function (newScreenID, sessionID) {
        var postData = { "newScreen": newScreenID };
        console.log("screen update called");
        this.postSystemMessage("Screen update in progress...");
        return this.http.post(this.sessionAPIurl + "/updateScreen/gameID/" + sessionID, postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.updateGameControllerData_S = function (newGameControllerData, gameInstanceID) {
        var postData = { "newData": newGameControllerData };
        console.log("Game controller update called");
        this.postSystemMessage("Game controller update in progress...");
        return this.http.post(this.sessionAPIurl + "/updateGameControllerData/gameID/" + gameInstanceID, postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.inactivateGameInstance = function (gameInstanceID, sessionID) {
        var _this = this;
        if (sessionID === void 0) { sessionID = null; }
        this.screenLockout = { lock: true, message: "Updating game status..." };
        this.postSystemMessage("Disabling game...");
        this.updateGameInstanceStatus_S(gameInstanceID, "ready").subscribe(function (res) {
            _this.postSystemMessage("Game disabled, update sent to all devices");
        });
        // set the status flag of the gameInstance to CLOSED
    };
    ProjectService.prototype.activateGameInstance = function (gameInstanceID, sessionID) {
        var _this = this;
        if (sessionID === void 0) { sessionID = null; }
        this.screenLockout = { lock: true, message: "Changing active game..." };
        this.postSystemMessage("Activating game...");
        this.updateGameInstanceStatus_S(gameInstanceID, "active").subscribe(function (res) {
            _this.postSystemMessage("Game activated, update sent to all devices");
        });
        // set the status flag of the gameInstance to OPEN
    };
    ProjectService.prototype.closeGameInstance = function (gameInstanceID, sessionID) {
        var _this = this;
        if (sessionID === void 0) { sessionID = null; }
        this.screenLockout = { lock: true, message: "Closing game..." };
        this.postSystemMessage("Closing game...");
        this.updateGameInstanceStatus_S(gameInstanceID, "closed").subscribe(function (res) {
            _this.postSystemMessage("Game closed, update sent to all devices");
        });
        this.activateGameInstance(null, this.logonData.sessionID);
        this.router.navigate([]);
        // set the status flag of the gameInstance to OPEN
    };
    ProjectService.prototype.updateGameInstanceStatus_S = function (gameID, newStatus) {
        var postData = { "gameID": gameID, "newStatus": newStatus };
        return this.http.post(this.sessionAPIurl + "/updateGameStatus", postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ProjectService.prototype.getGamesForSession = function (sessionID) {
        return this.http.get(this.sessionAPIurl + "/getGamesForSession/sessionID/" + sessionID, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    //redundent
    ProjectService.prototype.setLoadingFlags = function (sessionLoading, gameLoading, sessionObject) {
    };
    // updateSessionTemplate(newSessionPart, sessionObject){
    // 	this.controllerServiceData.sessionCall = 0 // Stop the api calls interfeering with the load
    // 	this.controllerServiceData.gameCall = 0 // Stop the api calls interfeering with the load
    // 	this.setLoadingFlags(false, true, sessionObject) // Set the flags for the laoding screens
    // 	this.controllerServiceData.sessionDATA.activeSessionPart = newSessionPart; // This overides the loacl varible to provide immeidate feeback to the user until the service updates
    // 	//sessionObject.gameInstances = null;
    // 	//sessionObject.activeGameInstance = null  // This overides the loacl varible to provide immeidate feeback to the user until the service updates
    // 	this.updateSessionPart_S(newSessionPart, sessionObject).subscribe(res => {
    // 		this.systemMessage = "Screen template updated to Part " + newSessionPart;
    // 		this.sessionLoopCheck(true, sessionObject) // can an update to controllers
    // 	})
    // }
    // updateSessionPart_S(newSessionPart, sessionObject): Observable <any[]> {
    // 		var postData = {"newSessionPart" : newSessionPart};
    // 		this.systemMessage = "Screen update in progress...";
    // 		return this.http.post(this.sessionAPIurl + "/updateActiveSessionGame/sessionID/" + sessionObject.sessionInstance.sessionID, postData, this.options)
    // 		.map((res:Response) => res.json())
    // 		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    // 	}
    // PLAYER CALLS
    ProjectService.prototype.storeResultToGameInstance = function (gameInstanceID, resultToAdd, userID) {
        var _this = this;
        this.storeResultToGameInstance_S(gameInstanceID, resultToAdd).subscribe(function (res) {
            _this.postSystemMessage("Single Game Result Saved");
            console.log("Result saved", res);
        });
    };
    ProjectService.prototype.storeResultToGameInstance_S = function (gameID, result) {
        var postData = { "gameID": gameID, "result": result };
        console.log(postData);
        return this.http.post(this.sessionAPIurl + "/saveResult/game/" + gameID, postData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    return ProjectService;
}());
ProjectService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */]) === "function" && _b || Object])
], ProjectService);

var _a, _b;
//# sourceMappingURL=service.js.map

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JiraService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var JiraService = (function () {
    function JiraService(http) {
        this.http = http;
        this.options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestOptions */]({ withCredentials: true });
        this.activeProjectKey = "IN";
        this.issueTypeIds = { story: "10001" };
        this.baseUrl = 'http://35.190.160.166:8080';
    }
    JiraService.prototype.queryJira = function (JqlQuery) {
        return this.http.get(this.baseUrl + '/rest/api/2/search?maxResults=1000&jql=' + JqlQuery, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    JiraService.prototype.getLoggedInUser = function () {
        var _this = this;
        this.getLoggedInUser_S().subscribe(function (res) {
            _this.loggedInUser = res;
            console.log("LOGGED IN USER", _this.loggedInUser);
        });
    };
    JiraService.prototype.addTicket = function (ticketData) {
        return this.http.post(this.baseUrl + '/rest/api/2/issue', ticketData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    JiraService.prototype.addTickets = function (ticketData) {
        return this.http.post(this.baseUrl + '/rest/api/2/issue/bulk', ticketData, this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    JiraService.prototype.getLoggedInUser_S = function () {
        return this.http.get(this.baseUrl + '/rest/api/2/myself', this.options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error'); });
    };
    return JiraService;
}());
JiraService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], JiraService);

var _a;
//# sourceMappingURL=JIRAservice.js.map

/***/ }),

/***/ 298:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./maxigames/maxigame.module": [
		148
	],
	"./minigames/minigame.module": [
		149
	],
	"./utillities/utillities.module": [
		152
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 298;


/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(327);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(34);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = (function () {
    function AppComponent(projectService, router) {
        this.projectService = projectService;
        this.router = router;
        this.title = 'Play Workshops';
        this.ENV = window.location.host;
        this.prodAPIurl = "http://sessions.playworkshops.com.au:3001"; //PROD
        this.stageAPIurl = "http://dev.sessions.playworkshops.com.au:3001"; //STAGE
        this.devAPIurl = "http://dev.sessions.playworkshops.com.au:3001";
        this.isLogging = true;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.warn("APP INIT");
        console.warn(" > Environment: " + this.ENV);
        switch (this.ENV) {
            case 'localhost:4200':
                this.projectService.sessionAPIurl = this.devAPIurl;
                console.warn(" > using LOCAL Play API uri: " + this.projectService.sessionAPIurl);
                break;
            case 'dev.sessions.playworkshops.com.au':
                this.projectService.sessionAPIurl = this.stageAPIurl;
                console.warn(" > using DEV Play API uri: " + this.projectService.sessionAPIurl);
                break;
            default:
                this.isLogging = false;
                this.projectService.sessionAPIurl = this.prodAPIurl;
                console.warn(" > using PROD Play API uri: PRODUCTION");
        }
        if (!this.projectService.logonData) {
            this.router.navigate(['login']);
        }
        this.sub_sessionLoader = this.projectService._SESSION_LIVE.subscribe(function (res) {
            if (_this.isLogging) {
                console.log("APP SESSION", res);
            }
            if (res[0].activeGameInstance) {
                _this.loadData();
            }
            else {
                _this.projectService.screenLockout.lock = false;
                _this.router.navigate(['waiting']);
            }
        });
    };
    AppComponent.prototype.loadData = function () {
        var _this = this;
        this.sub_gameLoader = this.projectService._GAME_LIVE.subscribe(function (res) {
            if (_this.isLogging) {
                console.log("APP GAME", res);
            }
            _this.projectService.screenLockout.lock = false;
            if (res[0]) {
                _this.router.navigate([res[0].gameInstance.gameTemplate]);
            }
        });
    };
    AppComponent.prototype.getLastMessageTime = function () {
        var now = Math.round((new Date()).getTime() / 1000); //Unix time now
        return now - this.projectService.systemMessage.latestMessageTime;
    };
    AppComponent.prototype.ngOnDestroy = function () {
        if (this.sub_sessionLoader) {
            this.sub_sessionLoader.unsubscribe();
        }
        if (this.sub_gameLoader) {
            this.sub_gameLoader.unsubscribe();
        }
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(502),
        styles: [__webpack_require__(480)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object])
], AppComponent);

var _a, _b;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppConfig; });
var AppConfig = (function () {
    function AppConfig() {
    }
    return AppConfig;
}());

//# sourceMappingURL=app.config.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_routes__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__minigames_minigame_module__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__maxigames_maxigame_module__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__utillities_utillities_module__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__system_holdingScreen_holding_screen_component__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__system_gameSpawner_game_spawner_component__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__system_utilityLoader_utility_loader_component__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_config__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__system_login_login_component__ = __webpack_require__(151);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// import { CustomErrorHandler  } from './custom-error-handler';
// import { environment } from 'environments/environment';

// App is our top level component







// import { APP_RESOLVER_PROVIDERS } from './app.resolver';


// Application wide providers
var APP_PROVIDERS = [
    //   ...APP_RESOLVER_PROVIDERS,
    __WEBPACK_IMPORTED_MODULE_15__app_config__["a" /* AppConfig */]
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_16__system_login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_12__system_holdingScreen_holding_screen_component__["a" /* HoldingScreenComponent */],
            __WEBPACK_IMPORTED_MODULE_13__system_gameSpawner_game_spawner_component__["a" /* GameSpawnerComponent */],
            __WEBPACK_IMPORTED_MODULE_14__system_utilityLoader_utility_loader_component__["a" /* UtilityLoaderComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_9__minigames_minigame_module__["MinigameModule"],
            __WEBPACK_IMPORTED_MODULE_10__maxigames_maxigame_module__["MaxigameModule"],
            __WEBPACK_IMPORTED_MODULE_11__utillities_utillities_module__["UtillitiesModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_routes__["a" /* ROUTES */], {
                useHash: true,
                preloadingStrategy: __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* PreloadAllModules */]
            })
        ],
        providers: [
            // { provide: ErrorHandler, useClass: CustomErrorHandler },
            __WEBPACK_IMPORTED_MODULE_3__JIRAservice__["a" /* JiraService */],
            __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClientModule */],
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__system_login_login_component__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__system_holdingScreen_holding_screen_component__ = __webpack_require__(150);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ROUTES; });


var ROUTES = [
    // {
    //   path: '', redirectTo: '', pathMatch: 'full'
    // },
    {
        path: 'login/key/:accessKey', component: __WEBPACK_IMPORTED_MODULE_0__system_login_login_component__["a" /* LoginComponent */], pathMatch: 'full'
    },
    {
        path: 'login', component: __WEBPACK_IMPORTED_MODULE_0__system_login_login_component__["a" /* LoginComponent */], pathMatch: 'full'
    },
    {
        path: 'waiting', component: __WEBPACK_IMPORTED_MODULE_1__system_holdingScreen_holding_screen_component__["a" /* HoldingScreenComponent */], pathMatch: 'full'
    },
    {
        path: 'minigames', loadChildren: './minigames/minigame.module#MinigameModule'
    },
    {
        path: 'maxigames', loadChildren: './maxigames/maxigame.module#MaxigameModule'
    },
    {
        path: 'utillities', loadChildren: './utillities/utillities.module#UtillitiesModule'
    },
];
//# sourceMappingURL=app.routes.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MAXI01ControllerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MAXI01ControllerComponent = (function () {
    function MAXI01ControllerComponent(service, JIRAservice) {
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.screenController = { screen: null };
    }
    MAXI01ControllerComponent.prototype.ngOnInit = function () { };
    // IN-GAME MANAGEMENT
    // GAME ADMIN  
    MAXI01ControllerComponent.prototype.openGame = function () {
        this.service.activateGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    MAXI01ControllerComponent.prototype.closeGame = function () {
        this.service.inactivateGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    MAXI01ControllerComponent.prototype.lockGame = function () {
        this.service.closeGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    MAXI01ControllerComponent.prototype.exitGame = function () {
        this.service.exitGame();
    };
    return MAXI01ControllerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])('GameLive'),
    __metadata("design:type", Object)
], MAXI01ControllerComponent.prototype, "GAME_LIVE_STREAM", void 0);
MAXI01ControllerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'MAXI01controller',
        template: __webpack_require__(503),
        styles: [__webpack_require__(481)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__JIRAservice__["a" /* JiraService */]) === "function" && _b || Object])
], MAXI01ControllerComponent);

var _a, _b;
//# sourceMappingURL=controller.component.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MAXI01interfaceComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// Define the results output structure fot his game
var MAXI01interfaceComponent = (function () {
    function MAXI01interfaceComponent(fb) {
        this.fb = fb;
        this.gameInstanceEnd = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.saveUserInput = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.screenController = { screen: 0 };
    }
    MAXI01interfaceComponent.prototype.ngOnInit = function () {
    };
    MAXI01interfaceComponent.prototype.getActiveView = function () {
        var _this = this;
        var result = this.GAME_LIVE_STREAM.gameInputs.RFIDhotspots.find(function (hotspot) {
            return hotspot.RFIDcardID == _this.GAME_LIVE_STREAM.controller.activeCards[0];
        });
        if (result) {
            return result.data;
        }
        else {
            return [];
        }
    };
    return MAXI01interfaceComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])('GameLive'),
    __metadata("design:type", Object)
], MAXI01interfaceComponent.prototype, "GAME_LIVE_STREAM", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], MAXI01interfaceComponent.prototype, "gameInstanceEnd", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], MAXI01interfaceComponent.prototype, "userType", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], MAXI01interfaceComponent.prototype, "saveUserInput", void 0);
MAXI01interfaceComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'MAXI01minigame',
        template: __webpack_require__(504),
        styles: [__webpack_require__(482)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */]) === "function" && _a || Object])
], MAXI01interfaceComponent);

var _a;
//# sourceMappingURL=maxigame.component.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__gameDataModel__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MAXI01MasterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MAXI01MasterComponent = (function () {
    function MAXI01MasterComponent(service, JIRAservice) {
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.userType = "user"; //default to the user
        this.screenController = { screen: null };
    }
    MAXI01MasterComponent.prototype.ngOnInit = function () {
        this.userType = this.service.logonData.userType;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* MAXI01 */](this.service.logonData.projectID, this.service.logonData.sessionID);
        this.loadGameData();
    };
    MAXI01MasterComponent.prototype.loadGameData = function () {
        var _this = this;
        this.service._GAME_LIVE.subscribe(function (res) {
            _this.GAME_LIVE_STREAM = res[0];
        });
    };
    // SPAWN GAME 
    MAXI01MasterComponent.prototype.spawnNewGame = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* MAXI01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); // crate a blank game
        this.service.spawnGame(this.spawnGameData, this.service.logonData.sessionID); //save the game to the database
        this.screenController.screen = null; // close the spawn screen
    };
    // MANAGE GAME DATA
    MAXI01MasterComponent.prototype.editGameData = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* MAXI01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.spawnGameData.gameInputs = JSON.parse(JSON.stringify(this.GAME_LIVE_STREAM.gameInputs));
        this.screenController.screen = 'gameData';
    };
    // loadFromJira(){
    //     this.spawnGameData = new MAXI01(this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID);  //reset the object for the new data
    //     // this.spawnGameData.gameInputs.personas = [];
    // }
    // loadFromOW(){
    //     this.spawnGameData = new MAXI01(this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID);  //reset the object for the new data
    //     this.service.getProject(this.GAME_LIVE_STREAM.gameInstance.projectID).subscribe( res => {
    //         if(res[0]){
    //             const projectData = res[0].data;
    //             this.spawnGameData.gameInputs.RFIDhotspots = projectData.hotspots
    //         }
    //     })
    // }
    MAXI01MasterComponent.prototype.loadFromGame = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* MAXI01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.spawnGameData.gameInputs = this.GAME_LIVE_STREAM.gameInputs;
        this.spawnGameData.gameInputs.RFIDhotspots = [{
                RFIDcardID: "D127DFDB",
                data: " SIMONA SMELLS"
            }];
    };
    MAXI01MasterComponent.prototype.removeFromGameData = function (itemIndex, itemObject) {
        this.spawnGameData.gameInputs[itemObject].splice(itemIndex, 1);
    };
    MAXI01MasterComponent.prototype.addToGameData = function (adddata, itemObject) {
        if (!this.spawnGameData.gameInputs[itemObject]) {
            this.spawnGameData.gameInputs[itemObject] = [];
        } /// add the parent object is it doesnt exist
        this.spawnGameData.gameInputs[itemObject].push(adddata);
    };
    MAXI01MasterComponent.prototype.updateGameData = function () {
        var _this = this;
        this.service.saveGameInputData_S(this.spawnGameData.gameInputs, this.GAME_LIVE_STREAM._id).subscribe(function (res) {
            console.log("Game Inputs Saved");
            _this.screenController.screen = null;
            _this.service.inactivateGameInstance(_this.GAME_LIVE_STREAM._id, _this.service.logonData.sessionID);
        });
    };
    MAXI01MasterComponent.prototype.saveUserEntry = function (entryData) {
        this.service.storeResultToGameInstance_S(this.GAME_LIVE_STREAM._id, [entryData]).subscribe(function (res) { console.log(res); });
    };
    MAXI01MasterComponent.prototype.exitGame = function () {
        this.service.exitGame();
    };
    return MAXI01MasterComponent;
}());
MAXI01MasterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'MAXI01game-master',
        template: __webpack_require__(505),
        styles: [__webpack_require__(483)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__JIRAservice__["a" /* JiraService */]) === "function" && _b || Object])
], MAXI01MasterComponent);

var _a, _b;
//# sourceMappingURL=master.component.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME01ControllerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GAME01ControllerComponent = (function () {
    function GAME01ControllerComponent(service, JIRAservice) {
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.screenController = { screen: null, showprintSituations: false };
    }
    GAME01ControllerComponent.prototype.ngOnInit = function () { };
    // IN-GAME MANAGEMENT
    GAME01ControllerComponent.prototype.updateActivePersona = function (newActivePersonaID) {
        this.GAME_LIVE_STREAM.controller['activePersona'] = newActivePersonaID;
        this.service.updateGameControllerData(this.GAME_LIVE_STREAM.controller, this.GAME_LIVE_STREAM._id, this.GAME_LIVE_STREAM._id);
    };
    GAME01ControllerComponent.prototype.activePersonaUserStories = function () {
        var _this = this;
        if (this.GAME_LIVE_STREAM) {
            var storiesForActivePersona = this.GAME_LIVE_STREAM.gameResults.filter(function (story) { return story.personaID == _this.GAME_LIVE_STREAM.controller['activePersona']; });
            return storiesForActivePersona;
        }
        else {
            return null;
        }
    };
    GAME01ControllerComponent.prototype.activePersonaData = function () {
        var _this = this;
        return this.GAME_LIVE_STREAM.gameInputs.personas.filter(function (persona) { return persona.ID == _this.GAME_LIVE_STREAM.controller['activePersona']; })[0];
    };
    // GAME ADMIN  
    GAME01ControllerComponent.prototype.openGame = function () {
        this.service.activateGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME01ControllerComponent.prototype.closeGame = function () {
        this.service.inactivateGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME01ControllerComponent.prototype.lockGame = function () {
        this.service.closeGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME01ControllerComponent.prototype.exitGame = function () {
        this.service.exitGame();
    };
    return GAME01ControllerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])('GameLive'),
    __metadata("design:type", Object)
], GAME01ControllerComponent.prototype, "GAME_LIVE_STREAM", void 0);
GAME01ControllerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME01controller',
        template: __webpack_require__(506),
        styles: [__webpack_require__(484)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__JIRAservice__["a" /* JiraService */]) === "function" && _b || Object])
], GAME01ControllerComponent);

var _a, _b;
//# sourceMappingURL=controller.component.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dataModels__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__gameDataModel__ = __webpack_require__(103);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME01gameComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// Define the results output structure fot his game
var GAME01gameComponent = (function () {
    function GAME01gameComponent(fb) {
        this.fb = fb;
        this.gameInstanceEnd = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.saveUserInput = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.screenController = { saveSessionConfirm: false, addForm: { show: true }, screen: 'gameInstructions', resultsExist: false };
        this.storyForm = this.fb.group({
            created: [""],
            creator: [""],
            location: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* Validators */].required],
            situation: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* Validators */].required],
            want: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* Validators */].required],
            reason: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* Validators */].required],
            vote: [0],
            personaName: [""],
            personaID: ["",],
        });
    }
    GAME01gameComponent.prototype.ngOnInit = function () {
        this.GAME_LOCAL = new __WEBPACK_IMPORTED_MODULE_3__gameDataModel__["a" /* GAME01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID);
        console.log(this.userType);
        console.log("------GAME01 GAME LIVE STREAM------", this.GAME_LIVE_STREAM);
    };
    //PLAYER CALLS
    GAME01gameComponent.prototype.reverseArray = function (data) {
        return data.reverse();
    };
    GAME01gameComponent.prototype.activePersonaUserStories = function () {
        var _this = this;
        if (this.GAME_LIVE_STREAM) {
            var storiesForActivePersona = this.GAME_LIVE_STREAM.gameResults.filter(function (story) { return story.personaID == _this.GAME_LIVE_STREAM.controller['activePersona']; });
            return storiesForActivePersona;
        }
        else {
            return null;
        }
    };
    GAME01gameComponent.prototype.saveUserEntry = function (entryData) {
        this.saveUserInput.emit(entryData);
    };
    GAME01gameComponent.prototype.activePersonaData = function () {
        var _this = this;
        return this.GAME_LIVE_STREAM.gameInputs.personas.filter(function (persona) { return persona.ID == _this.GAME_LIVE_STREAM.controller['activePersona']; })[0];
    };
    GAME01gameComponent.prototype.changeScreen = function (newScreen) {
        this.screenController.screen = newScreen;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    };
    GAME01gameComponent.prototype.addUserStory = function (event) {
        // add user story to this device's results set
        if (this.storyForm.valid) {
            var newStory = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["d" /* OW_userStory */];
            newStory.personaName = this.activePersonaData().name;
            newStory.personaID = this.activePersonaData().ID;
            // newStory.projectID = this.SESSION.activeInstance.projectID
            newStory.location = this.storyForm.controls.location.value;
            newStory.situation = this.storyForm.controls.situation.value;
            newStory.want = this.storyForm.controls.want.value;
            newStory.reason = this.storyForm.controls.reason.value;
            // this.screenController.addForm.show = false
            this.GAME_LOCAL.gameResults.push(this.storyForm.value);
            this.storyForm.reset();
            this.saveUserInput.emit(newStory);
            this.screenController.addForm.show = true;
        }
    };
    return GAME01gameComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])('GameLive'),
    __metadata("design:type", Object)
], GAME01gameComponent.prototype, "GAME_LIVE_STREAM", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], GAME01gameComponent.prototype, "gameInstanceEnd", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], GAME01gameComponent.prototype, "userType", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], GAME01gameComponent.prototype, "saveUserInput", void 0);
GAME01gameComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME01minigame',
        template: __webpack_require__(507),
        styles: [__webpack_require__(485)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormBuilder */]) === "function" && _a || Object])
], GAME01gameComponent);

var _a;
//# sourceMappingURL=minigame.component.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dataModels__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gameDataModel__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_uuid__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_uuid___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_uuid__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME01MasterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var GAME01MasterComponent = (function () {
    function GAME01MasterComponent(service, JIRAservice) {
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.userType = "user"; //default to the user
        this.screenController = { screen: null, saveInProgress: false };
    }
    GAME01MasterComponent.prototype.ngOnInit = function () {
        this.userType = this.service.logonData.userType;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_2__gameDataModel__["a" /* GAME01 */](this.service.logonData.projectID, this.service.logonData.sessionID);
        console.log("WIDGET");
        this.loadGameData();
        //this.stressTestGame() // RUN THE STRESS TEST ON THE GAME
    };
    GAME01MasterComponent.prototype.loadGameData = function () {
        var _this = this;
        this.service._GAME_LIVE.subscribe(function (res) {
            _this.GAME_LIVE_STREAM = res[0];
        });
    };
    // SPAWN GAME 
    GAME01MasterComponent.prototype.spawnNewGame = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_2__gameDataModel__["a" /* GAME01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); // crate a blank game
        this.service.spawnGame(this.spawnGameData, this.service.logonData.sessionID); //save the game to the database
        //TODO:active the new game
        this.screenController.screen = null; // close the spawn screen
    };
    // MANAGE GAME DATA
    GAME01MasterComponent.prototype.editGameData = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_2__gameDataModel__["a" /* GAME01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.spawnGameData.gameInputs = JSON.parse(JSON.stringify(this.GAME_LIVE_STREAM.gameInputs));
        this.screenController.screen = 'gameData';
    };
    GAME01MasterComponent.prototype.loadFromOSession = function () {
        var _this = this;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_2__gameDataModel__["a" /* GAME01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.service._SESSION_LIVE.take(1).subscribe(function (res) {
            if (res[0]) {
                var projectData = res[0].data;
                _this.spawnGameData.gameInputs.personas = projectData.personas;
                _this.spawnGameData.gameInputs.locations = projectData.locations;
                _this.spawnGameData.gameInputs.situations = projectData.situations;
            }
        });
    };
    GAME01MasterComponent.prototype.loadFromOW = function () {
        var _this = this;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_2__gameDataModel__["a" /* GAME01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.service.getProject(this.service.logonData.OWProjectID).take(1).subscribe(function (res) {
            if (res[0]) {
                var projectData = res[0].data;
                _this.spawnGameData.gameInputs.personas = projectData.personas;
                _this.spawnGameData.gameInputs.locations = projectData.locations;
                _this.spawnGameData.gameInputs.situations = projectData.situations;
            }
        });
    };
    GAME01MasterComponent.prototype.loadFromGame = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_2__gameDataModel__["a" /* GAME01 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.spawnGameData.gameInputs = this.GAME_LIVE_STREAM.gameInputs;
    };
    GAME01MasterComponent.prototype.removeFromGameData = function (itemIndex, itemObject) {
        this.spawnGameData.gameInputs[itemObject].splice(itemIndex, 1);
    };
    GAME01MasterComponent.prototype.addToGameData = function (adddata, itemObject) {
        if (itemObject == 'personas') {
            adddata = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["c" /* OW_persona */](__WEBPACK_IMPORTED_MODULE_5_angular2_uuid__["UUID"].UUID(), adddata.name);
        } // Add the required ID to the persons
        if (!this.spawnGameData.gameInputs[itemObject]) {
            this.spawnGameData.gameInputs[itemObject] = [];
        } /// add the parent object is it doesnt exist
        this.spawnGameData.gameInputs[itemObject].push(adddata);
    };
    GAME01MasterComponent.prototype.updateGameData = function () {
        var _this = this;
        this.service.saveGameInputData_S(this.spawnGameData.gameInputs, this.GAME_LIVE_STREAM._id).subscribe(function (res) {
            console.log("Game Inputs Saved");
            _this.screenController.screen = null;
        });
    };
    GAME01MasterComponent.prototype.saveUserEntry = function (entryData) {
        this.service.storeResultToGameInstance_S(this.GAME_LIVE_STREAM._id, [entryData]).take(1).subscribe(function (res) { console.log(res); });
    };
    GAME01MasterComponent.prototype.exitGame = function () {
        this.service.exitGame();
    };
    // SAVE PROJECT DATA
    // makeJiraTickets(){
    //     this.JIRAservice.addTickets(this.GAME_LIVE_STREAM.gameResults).subscribe(res => {console.log(res)});
    //     // this.GAME_LIVE_STREAM.gameResults.map(story => {
    //     //      this.JIRAservice.addTicket(story).subscribe(res => {console.log(res)});
    //     // })
    //     }
    GAME01MasterComponent.prototype.saveSessionData = function () {
        this.screenController.saveInProgress = true;
        var gameResults = { userStories: this.GAME_LIVE_STREAM.gameResults };
        this.service.storeGameResultToSession(gameResults, this.GAME_LIVE_STREAM.gameInstance.sessionID);
        this.screenController.saveInProgress = false;
    };
    GAME01MasterComponent.prototype.saveProjectData = function () {
        var _this = this;
        this.screenController.saveInProgress = true;
        this.service.getProject(this.GAME_LIVE_STREAM.gameInstance.projectID).take(1).subscribe(function (res) {
            var mergedResults = [];
            var projectData = res[0];
            var gameResults = _this.GAME_LIVE_STREAM.gameResults;
            if (!projectData['data']) {
                projectData['data'] = [];
            } // if no previous results have been saved results then push the new empty object first
            mergedResults = projectData['data']; // add all existing project results array
            if (!mergedResults['userStories']) {
                mergedResults['userStories'] = [];
            } // if no previous userstory results then push the new empty object first
            gameResults.map(function (userstory) {
                mergedResults['userStories'].push(userstory); // push in the game result
            });
            _this.service.saveResultsToProject(mergedResults, _this.GAME_LIVE_STREAM.gameInstance.projectID);
        });
    };
    // GAME STRESS TESTING
    GAME01MasterComponent.prototype.stressTestGame = function () {
        if (this.service.logonData.userType != 'user') {
            return 0;
        }
        console.log("GAME STRESS TEST STARTED"); //only run if type is user, not admin
        this.stressTestIteration();
    };
    GAME01MasterComponent.prototype.stressTestIteration = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__["Observable"].timer(2000).subscribe(function () { return _this.stressTestIteration(); });
        console.log(".");
        var story = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["d" /* OW_userStory */];
        story.personaName = "Game Stress Test";
        this.saveUserEntry(story);
    };
    return GAME01MasterComponent;
}());
GAME01MasterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME01game-master',
        template: __webpack_require__(508),
        styles: [__webpack_require__(486)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__JIRAservice__["a" /* JiraService */]) === "function" && _b || Object])
], GAME01MasterComponent);

var _a, _b;
//# sourceMappingURL=master.component.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME05ControllerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GAME05ControllerComponent = (function () {
    function GAME05ControllerComponent(service, JIRAservice) {
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.targetUserstory = 0;
        this.SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
        this.screenController = { timerOn: false, showPrintStories: false };
    }
    GAME05ControllerComponent.prototype.ngOnInit = function () { };
    GAME05ControllerComponent.prototype.print = function () {
        window.print();
    };
    // IN-GAME MANAGEMENT
    GAME05ControllerComponent.prototype.processRawResultsData = function () {
        var inProgress, rawData;
        if (!inProgress && this.GAME_LIVE_STREAM && this.GAME_LIVE_STREAM.gameResults.length) {
            rawData = JSON.parse(JSON.stringify(this.GAME_LIVE_STREAM.gameResults)); // check the loop is not already running and that game results actually exist
            inProgress = true; //prevent two loops at once
            var uniqueItems = rawData.map(function (item) { return item.ID; }).filter(function (value, index, self) { return self.indexOf(value) == index; });
            this.filteredLiveResultsData = uniqueItems.map(function (unique) {
                var itemsThisStory = rawData.filter(function (userStory) { return userStory.ID == unique; });
                var votesThisStory = itemsThisStory.reduce(function (acc, val) { return acc + val.investment; }, 0);
                var finalResult = itemsThisStory[0]; // load the base data for the first sotry in the set
                finalResult.investment = votesThisStory; // Update the votes data
                return finalResult;
            });
            inProgress = false;
        }
        if (this.service.debug) {
            console.log("---LIVE GAME DATA UPDATE GAME05---", this.filteredLiveResultsData);
        }
    };
    // GAME ADMIN  
    GAME05ControllerComponent.prototype.openGame = function () {
        this.service.activateGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME05ControllerComponent.prototype.closeGame = function () {
        this.service.inactivateGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME05ControllerComponent.prototype.lockGame = function () {
        this.service.closeGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME05ControllerComponent.prototype.exitGame = function () {
        this.service.exitGame();
    };
    return GAME05ControllerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])('GameLive'),
    __metadata("design:type", Object)
], GAME05ControllerComponent.prototype, "GAME_LIVE_STREAM", void 0);
GAME05ControllerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME05controller',
        template: __webpack_require__(509),
        styles: [__webpack_require__(487)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__JIRAservice__["a" /* JiraService */]) === "function" && _b || Object])
], GAME05ControllerComponent);

var _a, _b;
//# sourceMappingURL=controller.component.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gameDataModel__ = __webpack_require__(104);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME05gameComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// Define the results output structure fot his game
var GAME05gameComponent = (function () {
    function GAME05gameComponent(fb) {
        this.fb = fb;
        this.gameInstanceEnd = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.saveUserInput = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.subs = [];
        this.saveInProgress = false;
        this.screenController = { showKeyError: false, activeCards: [], remainingCards: [], targetUserstory: null, resultsRefreshInProgress: false, screen: 'gameInstructions', showDone: false, timeValue: 5, voteValue: null, buttonHilights: { next: false, back: false } };
        this.SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
    }
    GAME05gameComponent.prototype.ngOnInit = function () {
        this.GAME_LOCAL = new __WEBPACK_IMPORTED_MODULE_2__gameDataModel__["a" /* GAME05 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID);
        this.screenController.remainingCards = this.GAME_LIVE_STREAM.gameInputs.voteCardsAvailable;
    };
    GAME05gameComponent.prototype.ngOnDestroy = function () {
    };
    //PLAYER CALLS
    GAME05gameComponent.prototype.openUserStory = function (userStoryKey) {
        var targetStory = this.GAME_LIVE_STREAM.gameInputs.userStories.find(function (story) { return story.key == userStoryKey; });
        if (!targetStory) {
            this.screenController.showKeyError = true;
        }
        else {
            this.screenController.targetUserstory = targetStory.ID;
        }
    };
    GAME05gameComponent.prototype.saveResults = function () {
        var _this = this;
        if (this.saveInProgress) {
            return;
        } // prevent two saves being triggered together
        this.saveInProgress = true;
        this.screenController.screen = 'done';
        this.screenController.activeCards.map(function (activeCard) {
            var targetStory = _this.GAME_LIVE_STREAM.gameInputs.userStories.find(function (story) { return story.ID == activeCard.ID; });
            targetStory.investment = activeCard.cards.reduce(function (total, val) { return total += val; }, 0);
            _this.GAME_LOCAL.gameResults.push(targetStory);
            _this.gameInstanceEnd.emit(_this.GAME_LOCAL.gameResults);
        });
    };
    GAME05gameComponent.prototype.targetUserStory = function () {
        var _this = this;
        if (this.screenController.targetUserstory) {
            return this.GAME_LIVE_STREAM.gameInputs.userStories.find(function (story) { return story.ID == _this.screenController.targetUserstory; });
        }
        else {
            return null;
        }
    };
    // storyVote(voteValue:number){
    //   this.screenController.voteValue = voteValue;
    //   var resultToPush = this.GAME_LIVE_STREAM.gameInputs.userStories.find(story => story.ID == this.screenController.targetUserstory);
    //   resultToPush.investment = voteValue;
    //   this.GAME_LOCAL.gameResults.push(
    //     resultToPush
    //   )
    //   this.screenController.targetUserstory  = null;
    // }
    GAME05gameComponent.prototype.getStoryCards = function () {
        var _this = this;
        var active = this.screenController.activeCards.find(function (card) { return card.ID == _this.screenController.targetUserstory; });
        if (active) {
            return active.cards;
        }
        else {
            return null;
        }
    };
    GAME05gameComponent.prototype.storyAddCard = function (cardIndex) {
        var _this = this;
        if (!this.screenController.targetUserstory) {
            return;
        } //exit is a user story is not active
        if (!this.screenController.activeCards.find(function (card) { return card.ID == _this.screenController.targetUserstory; })) {
            this.screenController.activeCards.push({
                ID: this.screenController.targetUserstory,
                cards: []
            });
        }
        var cardValue = this.screenController.remainingCards[cardIndex]; //get the value of the card
        var pushLocation = this.screenController.activeCards.findIndex(function (card) { return card.ID == _this.screenController.targetUserstory; });
        this.screenController.activeCards[pushLocation].cards.push(cardValue); // add the card to the selected story
        this.screenController.remainingCards.splice(cardIndex, 1); // remove the card from the available list
    };
    GAME05gameComponent.prototype.storyRemoveCard = function (cardIndex) {
        var _this = this;
        console.log(this.screenController.activeCards.find(function (card) { return card.ID == _this.screenController.targetUserstory; }));
        var cardValue = this.screenController.activeCards.find(function (card) { return card.ID == _this.screenController.targetUserstory; }).cards[cardIndex]; //get the value of the card
        var spliceLocation = this.screenController.activeCards.findIndex(function (card) { return card.ID == _this.screenController.targetUserstory; });
        this.screenController.remainingCards.push(cardValue); // add the card to the remaining list
        this.screenController.activeCards[spliceLocation].cards.splice(cardIndex, 1); // remove the card from the active list
    };
    //STANDARD GAME SCREEN FUNCTIONS
    GAME05gameComponent.prototype.changeScreen = function (newScreen) {
        this.screenController.screen = newScreen;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    };
    return GAME05gameComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])('GameLive'),
    __metadata("design:type", Object)
], GAME05gameComponent.prototype, "GAME_LIVE_STREAM", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], GAME05gameComponent.prototype, "gameInstanceEnd", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], GAME05gameComponent.prototype, "userType", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], GAME05gameComponent.prototype, "saveUserInput", void 0);
GAME05gameComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME05minigame',
        template: __webpack_require__(510),
        styles: [__webpack_require__(488)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */]) === "function" && _a || Object])
], GAME05gameComponent);

var _a;
//# sourceMappingURL=minigame.component.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__gameDataModel__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME05MasterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GAME05MasterComponent = (function () {
    function GAME05MasterComponent(service, JIRAservice) {
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.userType = "user"; //default to the user
        this.screenController = { screen: null, voteCap: -1, saveInProgress: false };
    }
    GAME05MasterComponent.prototype.ngOnInit = function () {
        this.userType = this.service.logonData.userType;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME05 */](this.service.logonData.projectID, this.service.logonData.sessionID);
        console.log("GAME 05 WIDGET");
        this.loadGameData();
    };
    GAME05MasterComponent.prototype.ngOnDestroy = function () {
        if (this.sub_gameLive) {
            this.sub_gameLive.unsubscribe();
        }
    };
    GAME05MasterComponent.prototype.loadGameData = function () {
        var _this = this;
        this.sub_gameLive = this.service._GAME_LIVE.subscribe(function (res) {
            _this.GAME_LIVE_STREAM = res[0];
        });
    };
    // MANAGE GAME DATA
    GAME05MasterComponent.prototype.editGameData = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME05 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.spawnGameData.gameInputs = JSON.parse(JSON.stringify(this.GAME_LIVE_STREAM.gameInputs));
        this.screenController.screen = 'gameData';
    };
    GAME05MasterComponent.prototype.loadFromOW = function () {
        var _this = this;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME05 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.service.getProject(this.service.logonData.OWProjectID).subscribe(function (res) {
            if (res[0]) {
                var projectData = res[0].data;
                var userStories = _this.addUniqueKeys(projectData.userStories);
                _this.spawnGameData.gameInputs.userStories = _this.applyVoteCap(userStories, _this.screenController.voteCap);
            }
        });
    };
    GAME05MasterComponent.prototype.loadFromOSession = function () {
        var _this = this;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME05 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.service._SESSION_LIVE.take(1).subscribe(function (res) {
            if (res) {
                console.log("ewfew", res);
                var projectData = res[0].data;
                var userStories = _this.addUniqueKeys(projectData.userStories);
                _this.spawnGameData.gameInputs.userStories = _this.applyVoteCap(userStories, _this.screenController.voteCap);
            }
        });
    };
    GAME05MasterComponent.prototype.addUniqueKeys = function (userStories) {
        for (var x = 0; x < userStories.length; x++) {
            userStories[x]['key'] = "us" + (x + 10);
            console.log(userStories[x]['key']);
        }
        return userStories;
    };
    GAME05MasterComponent.prototype.applyVoteCap = function (userStories, voteCap) {
        if (voteCap > -1) {
            return userStories.filter(function (story) { return story.vote >= voteCap; });
        }
        else {
            return userStories;
        }
    };
    GAME05MasterComponent.prototype.loadFromGame = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME05 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.spawnGameData.gameInputs = this.GAME_LIVE_STREAM.gameInputs;
    };
    GAME05MasterComponent.prototype.removeFromGameData = function (itemIndex, itemObject) {
        this.spawnGameData.gameInputs[itemObject].splice(itemIndex, 1);
    };
    GAME05MasterComponent.prototype.addToGameData = function (adddata, itemObject) {
        if (!this.spawnGameData.gameInputs[itemObject]) {
            this.spawnGameData.gameInputs[itemObject] = [];
        } /// add the parent object is it doesnt exist
        this.spawnGameData.gameInputs[itemObject].push(adddata);
    };
    GAME05MasterComponent.prototype.updateGameData = function () {
        var _this = this;
        this.service.saveGameInputData_S(this.spawnGameData.gameInputs, this.GAME_LIVE_STREAM._id).take(1).subscribe(function (res) {
            _this.screenController.screen = null;
        });
    };
    GAME05MasterComponent.prototype.exitGame = function () {
        this.service.exitGame();
    };
    // SAVE USER RESULTS
    GAME05MasterComponent.prototype.saveUserResults = function (gameResultData) {
        if (!gameResultData) {
            return 0;
        }
        this.service.storeResultToGameInstance_S(this.GAME_LIVE_STREAM._id, gameResultData).take(1).subscribe(function (res) { console.log(res); });
    };
    // SAVE PROJECT DATA
    GAME05MasterComponent.prototype.saveSessionData = function () {
        this.screenController.saveInProgress = true;
        var processedRestults = this.processRawResultsData(this.GAME_LIVE_STREAM.gameResults);
        var gameResults = { userStories: this.processRawResultsData(processedRestults) };
        this.service.storeGameResultToSession(gameResults, this.GAME_LIVE_STREAM.gameInstance.sessionID);
        this.screenController.saveInProgress = false;
    };
    GAME05MasterComponent.prototype.saveProjectData = function () {
        var _this = this;
        this.screenController.saveInProgress = true;
        this.service.getProject(this.GAME_LIVE_STREAM.gameInstance.projectID).take(1).subscribe(function (res) {
            var mergedResults = [];
            var projectData = res[0];
            var gameResults = _this.processRawResultsData(_this.GAME_LIVE_STREAM.gameResults);
            console.log("PROCESSED", gameResults);
            mergedResults = projectData['data']; // add all existing project results array
            gameResults.map(function (userstory) {
                var targetStoryIndex = mergedResults['userStories'].findIndex(function (mergedResult) { return mergedResult.ID == userstory.ID; }); // find the object index in the merged data
                if (targetStoryIndex < 0) {
                    mergedResults['userStories'].push(userstory);
                    targetStoryIndex = mergedResults['userStories'].length - 1;
                }
                mergedResults['userStories'][targetStoryIndex].investment = userstory.investment; //update the object in the merged data
                console.log("NEW STORY PUSHED TO PROJECT", mergedResults['userStories'], targetStoryIndex);
            });
            _this.service.saveResultsToProject(mergedResults, _this.GAME_LIVE_STREAM.gameInstance.projectID);
        });
    };
    GAME05MasterComponent.prototype.processRawResultsData = function (rawResults) {
        var _this = this;
        var rawData = JSON.parse(JSON.stringify(rawResults));
        var uniqueItems = rawData.map(function (item) { return item.ID; }).filter(function (value, index, self) { return self.indexOf(value) == index; });
        return uniqueItems.map(function (unique) {
            var itemsThisStory = rawData.filter(function (userStory) { return userStory.ID == unique; });
            var votesThisStory = itemsThisStory.reduce(function (acc, val) { return acc + val.investment; }, 0);
            var finalResult = _this.GAME_LIVE_STREAM.gameInputs.userStories.find(function (story) { return story.ID == unique; }); // the raw story data from the game inputs to avoid issues wth duplicates in the game results
            finalResult.investment = votesThisStory; // Update the votes data from the results
            return finalResult;
        });
    };
    return GAME05MasterComponent;
}());
GAME05MasterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME05game-master',
        template: __webpack_require__(511),
        styles: [__webpack_require__(489)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__JIRAservice__["a" /* JiraService */]) === "function" && _b || Object])
], GAME05MasterComponent);

var _a, _b;
//# sourceMappingURL=master.component.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME03ControllerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GAME03ControllerComponent = (function () {
    function GAME03ControllerComponent(service, JIRAservice) {
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.targetUserstory = 0;
        this.SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
        this.screenController = { timerOn: false };
    }
    GAME03ControllerComponent.prototype.ngOnInit = function () { };
    // IN-GAME MANAGEMENT
    GAME03ControllerComponent.prototype.toggleTimer = function () {
        var newControllerData = JSON.parse(JSON.stringify(this.GAME_LIVE_STREAM.controller)); // make a static copy of the live vaibile to work with
        console.log(newControllerData);
        if (newControllerData.timerOn) {
            newControllerData.timerOn = false;
        }
        else {
            this.screenController.timerOn = true;
            newControllerData.timerOn = true;
        }
        this.service.updateGameControllerData(newControllerData, this.GAME_LIVE_STREAM._id, this.GAME_LIVE_STREAM._id);
    };
    GAME03ControllerComponent.prototype.processRawResultsData = function () {
        var inProgress, rawData;
        if (!inProgress && this.GAME_LIVE_STREAM && this.GAME_LIVE_STREAM.gameResults.length) {
            rawData = JSON.parse(JSON.stringify(this.GAME_LIVE_STREAM.gameResults)); // check the loop is not already running and that game results actually exist
            inProgress = true; //prevent two loops at once
            var uniqueItems = rawData.map(function (item) { return item.ID; }).filter(function (value, index, self) { return self.indexOf(value) == index; });
            this.filteredLiveResultsData = uniqueItems.map(function (unique) {
                var itemsThisStory = rawData.filter(function (userStory) { return userStory.ID == unique; });
                var votesThisStory = itemsThisStory.reduce(function (acc, val) { return acc + val.vote; }, 0);
                var finalResult = itemsThisStory[0]; // load the base data for the first sotry in the set
                finalResult.vote = votesThisStory; // Update the votes data
                return finalResult;
            });
            inProgress = false;
        }
        if (this.service.debug) {
            console.log("---LIVE GAME DATA UPDATE GAME03---", this.filteredLiveResultsData);
        }
    };
    // GAME ADMIN  
    GAME03ControllerComponent.prototype.openGame = function () {
        this.service.activateGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME03ControllerComponent.prototype.closeGame = function () {
        this.service.inactivateGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME03ControllerComponent.prototype.lockGame = function () {
        this.service.closeGameInstance(this.GAME_LIVE_STREAM._id, this.service.logonData.sessionID);
    };
    GAME03ControllerComponent.prototype.exitGame = function () {
        this.service.exitGame();
    };
    return GAME03ControllerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])('GameLive'),
    __metadata("design:type", Object)
], GAME03ControllerComponent.prototype, "GAME_LIVE_STREAM", void 0);
GAME03ControllerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME03controller',
        template: __webpack_require__(512),
        styles: [__webpack_require__(490)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__JIRAservice__["a" /* JiraService */]) === "function" && _b || Object])
], GAME03ControllerComponent);

var _a, _b;
//# sourceMappingURL=controller.component.js.map

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__gameDataModel__ = __webpack_require__(105);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME03gameComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// Define the results output structure fot his game
var GAME03gameComponent = (function () {
    function GAME03gameComponent(fb) {
        this.fb = fb;
        this.gameInstanceEnd = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.saveUserInput = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.subs = [];
        this.screenController = { targetUserstory: 0, resultsRefreshInProgress: false, screen: 'gameInstructions', showDone: false, timeValue: 5, voteValue: null, buttonHilights: { next: false, back: false } };
        this.SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
    }
    GAME03gameComponent.prototype.ngOnInit = function () {
        this.GAME_LOCAL = new __WEBPACK_IMPORTED_MODULE_3__gameDataModel__["a" /* GAME03 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID);
        this.timerTick();
    };
    GAME03gameComponent.prototype.ngOnDestroy = function () {
        if (this.subs['gametimer']) {
            this.subs['gametimer'].unsubscribe();
        }
    };
    //PLAYER CALLS
    GAME03gameComponent.prototype.timerTick = function () {
        var _this = this;
        if (this.GAME_LIVE_STREAM && this.GAME_LIVE_STREAM.controller.timerOn && this.screenController.screen != "gameInstructions" && this.targetUserStory()) {
            console.log(this.screenController.timeValue);
            this.screenController.timeValue -= 1;
        }
        if (this.screenController.timeValue <= 0) {
            this.screenController.voteValue = 5;
            this.storyVote(0);
        }
        this.subs['gametimer'] = __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].timer(1000).subscribe(function () { return _this.timerTick(); });
    };
    GAME03gameComponent.prototype.swipe = function (currentIndex, action) {
        var _this = this;
        if (action === void 0) { action = this.SWIPE_ACTION.RIGHT; }
        var vote;
        if (action == "swiperight") {
            this.storyVote(1);
            this.screenController.buttonHilights.next = true;
            __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].timer(500).take(1).subscribe(function () { return _this.resetButtonHilights(); });
        }
        else {
            this.storyVote(0);
            this.screenController.buttonHilights.back = true;
            __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].timer(500).take(1).subscribe(function () { return _this.resetButtonHilights(); });
        }
    };
    GAME03gameComponent.prototype.targetUserStory = function () {
        return this.GAME_LIVE_STREAM.gameInputs.userStories[this.screenController.targetUserstory];
    };
    GAME03gameComponent.prototype.resetButtonHilights = function () {
        this.screenController.buttonHilights = { next: false, back: false };
    };
    GAME03gameComponent.prototype.storyVote = function (voteValue) {
        this.screenController.voteValue = voteValue;
        var resultToPush = this.GAME_LIVE_STREAM.gameInputs.userStories[this.screenController.targetUserstory];
        resultToPush.vote = voteValue;
        this.GAME_LOCAL.gameResults.push(resultToPush);
        this.screenController.targetUserstory++;
        if (this.screenController.targetUserstory > this.GAME_LIVE_STREAM.gameInputs.userStories.length - 1) {
            this.gameInstanceEnd.emit(this.GAME_LOCAL.gameResults);
        }
        this.screenController.voteValue = -1;
        this.screenController.timeValue = 5;
    };
    //STANDARD GAME SCREEN FUNCTIONS
    GAME03gameComponent.prototype.changeScreen = function (newScreen) {
        this.screenController.screen = newScreen;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    };
    return GAME03gameComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])('GameLive'),
    __metadata("design:type", Object)
], GAME03gameComponent.prototype, "GAME_LIVE_STREAM", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], GAME03gameComponent.prototype, "gameInstanceEnd", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], GAME03gameComponent.prototype, "userType", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], GAME03gameComponent.prototype, "saveUserInput", void 0);
GAME03gameComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME03minigame',
        template: __webpack_require__(513),
        styles: [__webpack_require__(491)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormBuilder */]) === "function" && _a || Object])
], GAME03gameComponent);

var _a;
//# sourceMappingURL=minigame.component.js.map

/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__gameDataModel__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GAME03MasterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GAME03MasterComponent = (function () {
    function GAME03MasterComponent(service, JIRAservice) {
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.userType = "user"; //default to the user
        this.screenController = { screen: null, saveInProgress: false };
    }
    GAME03MasterComponent.prototype.ngOnInit = function () {
        this.userType = this.service.logonData.userType;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME03 */](this.service.logonData.projectID, this.service.logonData.sessionID);
        console.log("WIDGET");
        this.loadGameData();
    };
    GAME03MasterComponent.prototype.ngOnDestroy = function () {
        if (this.sub_gameLive) {
            this.sub_gameLive.unsubscribe();
        }
    };
    GAME03MasterComponent.prototype.loadGameData = function () {
        var _this = this;
        this.sub_gameLive = this.service._GAME_LIVE.subscribe(function (res) {
            _this.GAME_LIVE_STREAM = res[0];
        });
    };
    // MANAGE GAME DATA
    GAME03MasterComponent.prototype.editGameData = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME03 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.spawnGameData.gameInputs = JSON.parse(JSON.stringify(this.GAME_LIVE_STREAM.gameInputs));
        this.screenController.screen = 'gameData';
    };
    GAME03MasterComponent.prototype.loadFromOW = function () {
        var _this = this;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME03 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.service.getProject(this.service.logonData.OWProjectID).take(1).subscribe(function (res) {
            if (res[0]) {
                var projectData = res[0].data;
                _this.spawnGameData.gameInputs.userStories = projectData.userStories;
            }
        });
    };
    GAME03MasterComponent.prototype.loadFromOSession = function () {
        var _this = this;
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME03 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.service._SESSION_LIVE.take(1).subscribe(function (res) {
            if (res[0]) {
                var projectData = res[0].data;
                _this.spawnGameData.gameInputs.userStories = projectData.userStories;
            }
        });
    };
    GAME03MasterComponent.prototype.loadFromGame = function () {
        this.spawnGameData = new __WEBPACK_IMPORTED_MODULE_1__gameDataModel__["a" /* GAME03 */](this.GAME_LIVE_STREAM.gameInstance.projectID, this.GAME_LIVE_STREAM.gameInstance.sessionID); //reset the object for the new data
        this.spawnGameData.gameInputs = this.GAME_LIVE_STREAM.gameInputs;
    };
    GAME03MasterComponent.prototype.removeFromGameData = function (itemIndex, itemObject) {
        this.spawnGameData.gameInputs[itemObject].splice(itemIndex, 1);
    };
    GAME03MasterComponent.prototype.addToGameData = function (adddata, itemObject) {
        if (!this.spawnGameData.gameInputs[itemObject]) {
            this.spawnGameData.gameInputs[itemObject] = [];
        } /// add the parent object is it doesnt exist
        this.spawnGameData.gameInputs[itemObject].push(adddata);
    };
    GAME03MasterComponent.prototype.updateGameData = function () {
        var _this = this;
        this.service.saveGameInputData_S(this.spawnGameData.gameInputs, this.GAME_LIVE_STREAM._id).take(1).subscribe(function (res) {
            _this.screenController.screen = null;
            _this.service.inactivateGameInstance(_this.GAME_LIVE_STREAM._id, _this.GAME_LIVE_STREAM.gameInstance.sessionID);
        });
    };
    GAME03MasterComponent.prototype.exitGame = function () {
        this.service.exitGame();
    };
    // SAVE USER RESULTS
    GAME03MasterComponent.prototype.saveUserResults = function (gameResultData) {
        if (!gameResultData) {
            return 0;
        }
        this.service.storeResultToGameInstance_S(this.GAME_LIVE_STREAM._id, gameResultData).take(1).subscribe(function (res) { console.log(res); });
    };
    // SAVE PROJECT DATA
    GAME03MasterComponent.prototype.saveSessionData = function () {
        this.screenController.saveInProgress = true;
        var processedRestults = this.processRawResultsData(this.GAME_LIVE_STREAM.gameResults);
        var gameResults = { userStories: this.processRawResultsData(processedRestults) };
        this.service.storeGameResultToSession(gameResults, this.GAME_LIVE_STREAM.gameInstance.sessionID);
        this.screenController.saveInProgress = false;
    };
    GAME03MasterComponent.prototype.saveProjectData = function () {
        var _this = this;
        this.screenController.saveInProgress = true;
        this.service.getProject(this.GAME_LIVE_STREAM.gameInstance.projectID).subscribe(function (res) {
            var mergedResults = [];
            var projectData = res[0];
            var gameResults = _this.processRawResultsData(_this.GAME_LIVE_STREAM.gameResults);
            console.log("PROCESSED", gameResults);
            mergedResults = projectData['data']; // add all existing project results array
            gameResults.map(function (userstory) {
                var targetStoryIndex = mergedResults['userStories'].findIndex(function (mergedResult) { return mergedResult.ID == userstory.ID; }); // find the object index in the merged data
                if (targetStoryIndex < 0) {
                    mergedResults['userStories'].push(userstory);
                    targetStoryIndex = mergedResults['userStories'].length - 1;
                    console.log("NEW STORY PUSHED TO PROJECT", mergedResults['userStories'], targetStoryIndex);
                }
                mergedResults['userStories'][targetStoryIndex].vote = userstory.vote; //update the object in the merged data
            });
            _this.service.saveResultsToProject(mergedResults, _this.GAME_LIVE_STREAM.gameInstance.projectID);
        });
    };
    GAME03MasterComponent.prototype.processRawResultsData = function (rawResults) {
        var _this = this;
        var rawData = JSON.parse(JSON.stringify(rawResults));
        var uniqueItems = rawData.map(function (item) { return item.ID; }).filter(function (value, index, self) { return self.indexOf(value) == index; });
        return uniqueItems.map(function (unique) {
            var itemsThisStory = rawData.filter(function (userStory) { return userStory.ID == unique; });
            var votesThisStory = itemsThisStory.reduce(function (acc, val) { return acc + val.vote; }, 0);
            var finalResult = _this.GAME_LIVE_STREAM.gameInputs.userStories.find(function (story) { return story.ID == unique; }); // the raw story data from the game inputs to avoid issues wth duplicates in the game results
            finalResult.vote = votesThisStory; // Update the votes data from the results
            return finalResult;
        });
    };
    return GAME03MasterComponent;
}());
GAME03MasterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'GAME03game-master',
        template: __webpack_require__(514),
        styles: [__webpack_require__(492)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__service__["a" /* ProjectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__JIRAservice__["a" /* JiraService */]) === "function" && _b || Object])
], GAME03MasterComponent);

var _a, _b;
//# sourceMappingURL=master.component.js.map

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GameFooterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GameFooterComponent = (function () {
    function GameFooterComponent() {
        this.OnClickPlay = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
    }
    GameFooterComponent.prototype.ngOnInit = function () {
    };
    GameFooterComponent.prototype.playClick = function () {
        this.OnClickPlay.emit();
    };
    return GameFooterComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], GameFooterComponent.prototype, "OnClickPlay", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], GameFooterComponent.prototype, "screenController", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], GameFooterComponent.prototype, "GameStream", void 0);
GameFooterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'app-game-footer',
        template: __webpack_require__(515),
        styles: [__webpack_require__(493)]
    }),
    __metadata("design:paramtypes", [])
], GameFooterComponent);

//# sourceMappingURL=game-footer.component.js.map

/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__minigames_minigame_user_story_poems_gameDataModel__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__minigames_minigame_user_story_tinder_gameDataModel__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__minigames_minigame_user_story_silent_auction_gameDataModel__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__maxigames_maxigame_roadmap_explorer_gameDataModel__ = __webpack_require__(147);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GameSpawnerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GameSpawnerComponent = (function () {
    function GameSpawnerComponent(service) {
        this.service = service;
        this.spawning = false;
        this.closeMe = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.titleWarning = false;
    }
    ;
    GameSpawnerComponent.prototype.ngOnInit = function () { };
    GameSpawnerComponent.prototype.spawnGame = function (gameData) {
        var _this = this;
        this.service.screenLockout = { lock: true, message: "Spawning game to session..." };
        this.service.spawnGame_S(gameData, this.sessionID).subscribe(function (res) {
            _this.service.screenLockout.lock = false;
            _this.closeMe.emit(true);
        });
    };
    GameSpawnerComponent.prototype.cancel = function () { this.closeMe.emit(true); };
    // ADD GAMES HERE
    GameSpawnerComponent.prototype.GAME01 = function (gameTitle) {
        if (!gameTitle) {
            this.titleWarning = true;
            return 0;
        }
        var newGame = new __WEBPACK_IMPORTED_MODULE_2__minigames_minigame_user_story_poems_gameDataModel__["a" /* GAME01 */](this.projectID, this.sessionID);
        newGame.gameInstance.name = gameTitle;
        this.spawnGame(newGame);
    };
    GameSpawnerComponent.prototype.GAME03 = function (gameTitle) {
        if (!gameTitle) {
            this.titleWarning = true;
            return 0;
        }
        var newGame = new __WEBPACK_IMPORTED_MODULE_3__minigames_minigame_user_story_tinder_gameDataModel__["a" /* GAME03 */](this.projectID, this.sessionID);
        newGame.gameInstance.name = gameTitle;
        this.spawnGame(newGame);
    };
    GameSpawnerComponent.prototype.GAME05 = function (gameTitle) {
        if (!gameTitle) {
            this.titleWarning = true;
            return 0;
        }
        var newGame = new __WEBPACK_IMPORTED_MODULE_4__minigames_minigame_user_story_silent_auction_gameDataModel__["a" /* GAME05 */](this.projectID, this.sessionID);
        newGame.gameInstance.name = gameTitle;
        this.spawnGame(newGame);
    };
    GameSpawnerComponent.prototype.MAXI01 = function (gameTitle) {
        if (!gameTitle) {
            this.titleWarning = true;
            return 0;
        }
        var newGame = new __WEBPACK_IMPORTED_MODULE_5__maxigames_maxigame_roadmap_explorer_gameDataModel__["a" /* MAXI01 */](this.projectID, this.sessionID);
        newGame.gameInstance.name = gameTitle;
        this.spawnGame(newGame);
    };
    return GameSpawnerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], GameSpawnerComponent.prototype, "sessionID", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], GameSpawnerComponent.prototype, "projectID", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], GameSpawnerComponent.prototype, "closeMe", void 0);
GameSpawnerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'game-spawner',
        template: __webpack_require__(516),
        styles: [__webpack_require__(494)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__service__["a" /* ProjectService */]) === "function" && _a || Object])
], GameSpawnerComponent);

var _a;
//# sourceMappingURL=game-spawner.component.js.map

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(34);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilityLoaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UtilityLoaderComponent = (function () {
    function UtilityLoaderComponent(router) {
        this.router = router;
        this.closeMe = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* EventEmitter */]();
        this.titleWarning = false;
    }
    ;
    UtilityLoaderComponent.prototype.ngOnInit = function () { };
    UtilityLoaderComponent.prototype.loadUtility = function () { };
    UtilityLoaderComponent.prototype.cancel = function () { this.closeMe.emit(true); };
    return UtilityLoaderComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], UtilityLoaderComponent.prototype, "sessionID", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* Input */])(),
    __metadata("design:type", Object)
], UtilityLoaderComponent.prototype, "projectID", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
    __metadata("design:type", Object)
], UtilityLoaderComponent.prototype, "closeMe", void 0);
UtilityLoaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'utility-loader',
        template: __webpack_require__(519),
        styles: [__webpack_require__(497)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
], UtilityLoaderComponent);

var _a;
//# sourceMappingURL=utility-loader.component.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dataModels__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__artifacts_risks_component__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_uuid__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_uuid___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_uuid__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoalsUtilityComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// Define the results output structure fot his game
var GoalsUtilityComponent = (function () {
    function GoalsUtilityComponent(fb, service, JIRAservice) {
        this.fb = fb;
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.screenController = { saveSessionConfirm: false, addForm: { show: true }, screen: 'gameInstructions', resultsExist: false };
        this.editShow = false;
        this.riskTypeArtifacts = new __WEBPACK_IMPORTED_MODULE_2__artifacts_risks_component__["a" /* RiskTypeAtrifacts */];
        this.saveinProgress = false;
        this.targetItem = { index: null, data: null };
        this.Form = this.fb.group({
            name: ["", __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* Validators */].required],
            description: ["", __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* Validators */].required],
            documentation: [],
        });
    }
    GoalsUtilityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.projectID = this.service.logonData.OWProjectID;
        //Stop the polling loop as it is not needed
        this.service.serviceData.sessionCall = 0;
        this.service.serviceData.gameCall = 0;
        //load the project data
        this.service.getProject(this.projectID).take(1).subscribe(function (res) {
            _this.PROJECT = res[0];
            console.log("PROJECT DATA", _this.PROJECT);
        });
    };
    GoalsUtilityComponent.prototype.updateItem = function (itemIndex, Data) {
        var item = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["a" /* OW_goal */](__WEBPACK_IMPORTED_MODULE_3_angular2_uuid__["UUID"].UUID(), this.Form.controls.name.value);
        item.name = this.Form.controls.name.value;
        item.description = this.Form.controls.description.value;
        item.document = this.Form.controls.documentation.value;
        this.PROJECT.data.goals[itemIndex] = item;
        this.Form.reset();
        this.editShow = false;
        console.log(this.PROJECT);
    };
    GoalsUtilityComponent.prototype.addNewItem = function () {
        var newItem = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["a" /* OW_goal */](__WEBPACK_IMPORTED_MODULE_3_angular2_uuid__["UUID"].UUID(), 'New Item');
        this.PROJECT.data.goals.push(newItem);
        this.showEdit(newItem, this.PROJECT.data.goals.length - 1);
    };
    GoalsUtilityComponent.prototype.removeItem = function (itemIndex) {
        this.PROJECT.data.goals.splice(itemIndex, 1);
    };
    GoalsUtilityComponent.prototype.showEdit = function (data, index) {
        if (!data.document) {
            data.document = "";
        }
        this.Form.setValue({
            name: data.name,
            description: data.description,
            documentation: data.document
        });
        this.targetItem.index = index;
        this.editShow = true;
    };
    GoalsUtilityComponent.prototype.saveToProject = function () {
        this.saveinProgress = true;
        this.service.saveResultsToProject(this.PROJECT.data, this.projectID);
    };
    GoalsUtilityComponent.prototype.ngOnDestroy = function () {
        //restart the polling loop
        this.service.serviceData.sessionCall = 1;
    };
    GoalsUtilityComponent.prototype.RiskToSatement = function (riskIndex) {
        var riskStatement;
        if (riskIndex > 4) {
            riskStatement = "EXTREME";
        }
        else if (riskIndex > 3) {
            riskStatement = "VERY HIGH";
        }
        else if (riskIndex > 2) {
            riskStatement = "HIGH";
        }
        else if (riskIndex > 1) {
            riskStatement = "MODERATE";
        }
        else {
            riskStatement = "LOW";
        }
        return riskStatement;
    };
    return GoalsUtilityComponent;
}());
GoalsUtilityComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'utility-goals',
        template: __webpack_require__(520),
        styles: [__webpack_require__(498)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_6__angular_forms__["c" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_forms__["c" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__service__["a" /* ProjectService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__JIRAservice__["a" /* JiraService */]) === "function" && _c || Object])
], GoalsUtilityComponent);

var _a, _b, _c;
//# sourceMappingURL=utility.component.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dataModels__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_uuid__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_uuid___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_uuid__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonaUtilityComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// Define the results output structure fot his game
var PersonaUtilityComponent = (function () {
    function PersonaUtilityComponent(fb, service, JIRAservice) {
        this.fb = fb;
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.screenController = { saveSessionConfirm: false, addForm: { show: true }, screen: 'gameInstructions', resultsExist: false };
        this.editPersonaShow = false;
        this.saveinProgress = false;
        this.targetPersona = { index: null, data: null };
        this.Form = this.fb.group({
            name: ["", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* Validators */].required],
            avatarUrl: ["", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* Validators */].required],
            excerpt: ["", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* Validators */].required],
            documentation: [],
        });
    }
    PersonaUtilityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.projectID = this.service.logonData.OWProjectID;
        //Stop the polling loop as it is not needed
        this.service.serviceData.sessionCall = 0;
        this.service.serviceData.gameCall = 0;
        //load the project data
        this.service.getProject(this.projectID).take(1).subscribe(function (res) {
            _this.PROJECT = res[0];
            console.log("PROJECT DATA", _this.PROJECT);
        });
    };
    PersonaUtilityComponent.prototype.updatePersona = function (itemIndex, personaData) {
        var newPersona = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["c" /* OW_persona */](__WEBPACK_IMPORTED_MODULE_2_angular2_uuid__["UUID"].UUID(), this.Form.controls.name.value);
        newPersona.avatarUrl = this.Form.controls.avatarUrl.value;
        newPersona.excerpt = this.Form.controls.excerpt.value;
        newPersona.documentationUrl = this.Form.controls.documentation.value;
        this.PROJECT.data.personas[itemIndex] = newPersona;
        this.Form.reset();
        this.editPersonaShow = false;
    };
    PersonaUtilityComponent.prototype.addNewPersona = function () {
        var newPersona = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["c" /* OW_persona */](__WEBPACK_IMPORTED_MODULE_2_angular2_uuid__["UUID"].UUID(), 'New Persona');
        this.PROJECT.data.personas.push(newPersona);
        this.showEdit(newPersona, this.PROJECT.data.personas.length - 1);
    };
    PersonaUtilityComponent.prototype.removePersona = function (itemIndex) {
        this.PROJECT.data.personas.splice(itemIndex, 1);
    };
    PersonaUtilityComponent.prototype.showEdit = function (data, index) {
        if (!data.avatarUrl) {
            data.avatarUrl = "";
        }
        if (!data.excerpt) {
            data.excerpt = "";
        }
        if (!data.documentationUrl) {
            data.documentationUrl = "";
        }
        this.Form.setValue({
            name: data.name,
            avatarUrl: data.avatarUrl,
            excerpt: data.excerpt,
            documentation: data.documentationUrl
        });
        this.targetPersona.index = index;
        this.editPersonaShow = true;
    };
    PersonaUtilityComponent.prototype.saveToProject = function () {
        this.saveinProgress = true;
        this.service.saveResultsToProject(this.PROJECT.data, this.projectID);
    };
    PersonaUtilityComponent.prototype.ngOnDestroy = function () {
        //restart the polling loop
        this.service.serviceData.sessionCall = 1;
    };
    return PersonaUtilityComponent;
}());
PersonaUtilityComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'utillity-persona',
        template: __webpack_require__(521),
        styles: [__webpack_require__(499)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__service__["a" /* ProjectService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__JIRAservice__["a" /* JiraService */]) === "function" && _c || Object])
], PersonaUtilityComponent);

var _a, _b, _c;
//# sourceMappingURL=utillity.component.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dataModels__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__artifacts_risks_component__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_uuid__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_uuid___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_uuid__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__JIRAservice__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RisksUtilityComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// Define the results output structure fot his game
var RisksUtilityComponent = (function () {
    function RisksUtilityComponent(fb, service, JIRAservice) {
        this.fb = fb;
        this.service = service;
        this.JIRAservice = JIRAservice;
        this.screenController = { saveSessionConfirm: false, addForm: { show: true }, screen: 'gameInstructions', resultsExist: false };
        this.editShow = false;
        this.riskTypeArtifacts = new __WEBPACK_IMPORTED_MODULE_2__artifacts_risks_component__["a" /* RiskTypeAtrifacts */];
        this.saveinProgress = false;
        this.targetItem = { index: null, data: null };
        this.Form = this.fb.group({
            name: ["", __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* Validators */].required],
            probability: ["", __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* Validators */].required],
            impact: ["", __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* Validators */].required],
            linkedRiskType: ["", __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* Validators */].required],
            documentation: [],
        });
    }
    RisksUtilityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.projectID = this.service.logonData.OWProjectID;
        //Stop the polling loop as it is not needed
        this.service.serviceData.sessionCall = 0;
        this.service.serviceData.gameCall = 0;
        //load the project data
        this.service.getProject(this.projectID).take(1).subscribe(function (res) {
            _this.PROJECT = res[0];
            console.log("PROJECT DATA", _this.PROJECT);
        });
    };
    RisksUtilityComponent.prototype.updateItem = function (itemIndex, Data) {
        var _this = this;
        var item = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["b" /* OW_risk */](__WEBPACK_IMPORTED_MODULE_3_angular2_uuid__["UUID"].UUID(), this.Form.controls.name.value, { name: "Budget", avatarUrl: "" });
        item.name = this.Form.controls.name.value;
        item.probability = this.Form.controls.probability.value;
        item.impact = this.Form.controls.impact.value;
        item.type = this.riskTypeArtifacts.artifacts.find(function (item) { return item.name == _this.Form.controls.linkedRiskType.value; });
        item.document = this.Form.controls.documentation.value;
        this.PROJECT.data.actors.risks[itemIndex] = item;
        this.Form.reset();
        this.editShow = false;
        console.log(this.PROJECT);
    };
    RisksUtilityComponent.prototype.addNewItem = function () {
        var newItem = new __WEBPACK_IMPORTED_MODULE_1__dataModels__["b" /* OW_risk */](__WEBPACK_IMPORTED_MODULE_3_angular2_uuid__["UUID"].UUID(), 'New Item', { name: "", avatarUrl: "" });
        this.PROJECT.data.actors.risks.push(newItem);
        this.showEdit(newItem, this.PROJECT.data.actors.risks.length - 1);
    };
    RisksUtilityComponent.prototype.removeItem = function (itemIndex) {
        this.PROJECT.data.actors.risks.splice(itemIndex, 1);
    };
    RisksUtilityComponent.prototype.showEdit = function (data, index) {
        if (!data.document) {
            data.document = "";
        }
        this.Form.setValue({
            name: data.name,
            probability: data.probability,
            impact: data.impact,
            linkedRiskType: data.type.name,
            documentation: data.document
        });
        this.targetItem.index = index;
        this.editShow = true;
    };
    RisksUtilityComponent.prototype.saveToProject = function () {
        this.saveinProgress = true;
        this.service.saveResultsToProject(this.PROJECT.data, this.projectID);
    };
    RisksUtilityComponent.prototype.ngOnDestroy = function () {
        //restart the polling loop
        this.service.serviceData.sessionCall = 1;
    };
    RisksUtilityComponent.prototype.RiskToSatement = function (riskIndex) {
        var riskStatement;
        if (riskIndex > 4) {
            riskStatement = "EXTREME";
        }
        else if (riskIndex > 3) {
            riskStatement = "VERY HIGH";
        }
        else if (riskIndex > 2) {
            riskStatement = "HIGH";
        }
        else if (riskIndex > 1) {
            riskStatement = "MODERATE";
        }
        else {
            riskStatement = "LOW";
        }
        return riskStatement;
    };
    return RisksUtilityComponent;
}());
RisksUtilityComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'utility-risks',
        template: __webpack_require__(522),
        styles: [__webpack_require__(500)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_6__angular_forms__["c" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_forms__["c" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__service__["a" /* ProjectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__service__["a" /* ProjectService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__JIRAservice__["a" /* JiraService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__JIRAservice__["a" /* JiraService */]) === "function" && _c || Object])
], RisksUtilityComponent);

var _a, _b, _c;
//# sourceMappingURL=utility.component.js.map

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 480:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".holding{\n    z-index:-1;\n}\n\n\n.dataLight{\n    opacity:.3;\n    padding: 7px 10px;\n    display: block;\n    float: right;\n}\n\n.dataLightflash{\n    opacity:1;\n}\n\n.screenLockout{\n    position:fixed;\n    top:0px;\n    left:0px;\n    z-index:9999;\n    width:100%;\n    height:100vh;\n    background:rgba(0, 0, 0, 0.459);\n}\n.screenLockout .lockMessage{\n    width: 100%;\n    margin-top: 40vh;\n    padding: 10px;\n    background: black;\n    font-size: 1em;\n    color: white;\n    text-align:center;\n}\n\n.systemMessages{\n    top:0px;\n    right:0px;\n    color:white;\n    background:grey;\n    text-align:center;\n    font-size:.5em;\n    text-decoration:italic;\n    width:100%;\n    z-index:9999;\n    transition: all .3s;\n}\n.systemMessages p {\n    display:inline-block;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 481:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".btn-remove{\n    width:10px;\n    height:10px;\n    color:white;\n    background:rgb(150, 1, 1);\n}\n\n.btn-remove:after{\n    content:'X'\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 482:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, "button['submit']:disabled{background:grey}\n\ninput, textarea, select {\n    background: rgba(244, 222, 203, .2);\n    border: .5px dashed rgba(232, 202, 191, .5);\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 483:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".btn-remove{\n    width:10px;\n    height:10px;\n    color:rgb(150, 1, 1);\n}\n\n.btn-remove:after{\n    content:'x';\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n    font-size:.6em;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n.editGameData{\n    min-height:100vh;\n}\n\n.input-section{\n    margin-top:10px;border-top:thin solid grey;padding:10px;\n}\n\n.badge{margin-left:10px;}\n\ninput:focus{font-size:1em;}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 484:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".btn-remove{\n    width:10px;\n    height:10px;\n    color:white;\n    background:rgb(150, 1, 1);\n}\n\n.btn-remove:after{\n    content:'X'\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n\n.printSituations{\n    position: absolute;\n    top: 0px;\n    left: 0px;\n    width: 100%;\n    z-index:9999;\n    min-height:100%;\n    background:white;\n}\n.printSituationsContainer{\n    background:white;\n    page-break-inside: avoid;\n    font-size:30px;\n    padding-bottom:30px;\n    padding-top:30px;\n    padding-left:20%;\n    padding-right:20%;\n    margin:5% 0;\n    border-top:thin solid grey;\n    border-bottom:thin solid grey;\n}\n.printSituationsContainer h2{\n    text-align:center;\n}\n.printSituationsHeader{\n    display:block;\n    width:100%;\n    font-size:18px;\n    text-align:center;\n}\n\n.printSituationsFooter{\n    display:block;\n    width:100%;\n    color:black;\n    font-size:.10px;\n    text-align:center;\n}\n.printSituationsContainer * {\n    display:inline;\n    page-break-inside: avoid;\n}\n.printKey{\n    text-align:center;\n    font-size:14px;\n    margin-left:0%;\n    background:black;\n    padding:5px;\n    border-radius:10px;\n    color:white;\n}\n@media print{\n    .noPrint {display:none !important}\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 485:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, "button['submit']:disabled{background:grey}\n\ninput, textarea, select {\n    background: rgba(244, 222, 203, .2);\n    border: .5px dashed rgba(232, 202, 191, .5);\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 486:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".btn-remove{\n    width:10px;\n    height:10px;\n    color:rgb(150, 1, 1);\n}\n\n.btn-remove:after{\n    content:'x';\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n    font-size:.6em;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n.editGameData{\n    min-height:100vh;\n}\n\n.input-section{\n    margin-top:10px;border-top:thin solid grey;padding:10px;\n}\n\n.badge{margin-left:10px;}\n\ninput:focus{font-size:1em;}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 487:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".btn-remove{\n    width:10px;\n    height:10px;\n    color:white;\n    background:rgb(150, 1, 1);\n}\n\n.btn-remove:after{\n    content:'X'\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n\n.printStories{\n    position: absolute;\n    top: 0px;\n    left: 0px;\n    width: 100%;\n    z-index:9999;\n    min-height:100%;\n    background:white;\n}\n.printStoryContainer{\n    background:white;\n    page-break-inside: avoid;\n    font-size:30px;\n    padding-bottom:30px;\n    padding-top:30px;\n    padding-left:20%;\n    padding-right:20%;\n    margin:5% 0;\n    border-top:thin solid grey;\n    border-bottom:thin solid grey;\n}\n.printStoryHeader{\n    display:block;\n    width:100%;\n    font-size:18px;\n    text-align:center;\n}\n.printKey{\n    text-align:center;\n    margin-left:40%;\n    background:black;\n    padding:5px;\n    border-radius:10px;\n    color:white;\n}\n.printStoryFooter{\n    display:block;\n    width:100%;\n    color:black;\n    font-size:.10px;\n    text-align:center;\n}\n.printStoryContainer * {\n    display:inline;\n    page-break-inside: avoid;\n}\n@media print{\n    .noPrint {display:none !important}\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 488:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, "button['submit']:disabled{background:grey}\n\ninput, textarea, select {\n    background: rgba(244, 222, 203, .2);\n    border: .5px dashed rgba(232, 202, 191, .5);\n}\n\n\n\n\n@keyframes flash{\n    0%  {opacity:1}\n    50% {opacity:.2}\n    100% {opacity:1}\n}\n@keyframes fadeOut {\n    0%  {opacity:0}\n    100% {opacity:.2}\n}\n\n@keyframes swipeLeftRight {\n    0%  {margin-left:-100px}\n    100% {margin-left:100px;}\n}\n\n\n#mainGameSlide .emphasised{\n    text-transform:uppercase;\n}\n\n#mainGameSlide .inputGroup{\n    padding-top:20px;\n}\n\n.voteflash{\n    color:#f15f2d;\n    font-weight:bold;\n    animation: flash 1s ease-in-out 0s infinite alternate;\n}\n\n.timerStrap{\n    position: absolute;\n    width: 100%;\n    left: 0;\n    top:0;\n    margin: 20% 0 0 0;\n    height: 80vh;\n    text-align: center;\n    font-size: 20em;\n    opacity: .2;\n    animation: fadeOut 1s ease-in-out 0s infinite;\n}\n\n.swipeIcon{\n    width:100%;\n    text-align:center;\n    padding-top:40px;\n}\n.swipeIcon p{\n    font-size:.6em;\n    text-align:center;\n    display:block;\n}\n.swipeIcon img{\n    width:50px;\n    animation: swipeLeftRight 2s ease-in-out 0s infinite alternate;\n}\n\n.notifyBox{\n    width:100%;\n    text-align:center;\n    margin-top:5px;\n}\ninput{text-align:center;}\n.cardsAvailable{\n    padding:3px 0 30px 4%;\n    background: #00000059;\n}\n\n.activeSkill{\n    background: linear-gradient(45deg, #2d6131 20%, #28a745 60%, #64bd63);\n}\n\n.ResourceBy{\n    display:none;\n}\n.activeResourceBy{\n    display:block;\n}\n.resourcedByGroup{\n    position: absolute;\n    left: 0;\n    height: 100%;\n    top: 0;\n    width: 100%;\n    font-size: 3em;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 489:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".btn-remove{\n    width:10px;\n    height:10px;\n    color:white;\n    float:right;\n    cursor:pointer;\n    margin-right:10%;\n}\n\n.btn-remove:hover{\n    color:rgb(241, 125, 125)\n}\n\n.voteCap{\n   text-align: center;\n   font-size: 1.3em;\n   margin:0px auto;\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n    font-size:.6em;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n.editGameData{\n    min-height:100vh;\n}\n\n.input-section{\n    margin-top:10px;border-top:thin solid grey;padding:10px;\n}\n\n.badge{margin-left:10px;}\n\ninput:focus{font-size:1em;}\n\n.story{\n    border-bottom: thin solid grey;\n    margin-bottom: 20px;\n    padding-bottom: 5px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 490:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".btn-remove{\n    width:10px;\n    height:10px;\n    color:white;\n    background:rgb(150, 1, 1);\n}\n\n.btn-remove:after{\n    content:'X'\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 491:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, "button['submit']:disabled{background:grey}\n\ninput, textarea, select {\n    background: rgba(244, 222, 203, .2);\n    border: .5px dashed rgba(232, 202, 191, .5);\n}\n\n\n\n\n@keyframes flash{\n    0%  {opacity:1}\n    50% {opacity:.2}\n    100% {opacity:1}\n}\n@keyframes fadeOut {\n    0%  {opacity:0}\n    100% {opacity:.2}\n}\n\n@keyframes swipeLeftRight {\n    0%  {margin-left:-100px}\n    100% {margin-left:100px;}\n}\n\n\n#mainGameSlide .emphasised{\n    text-transform:uppercase;\n}\n\n#mainGameSlide .inputGroup{\n    padding-top:20px;\n}\n\n.voteflash{\n    color:#f15f2d;\n    font-weight:bold;\n    animation: flash 1s ease-in-out 0s infinite alternate;\n}\n\n.timerStrap{\n    position: absolute;\n    width: 100%;\n    left: 0;\n    top:0;\n    margin: 20% 0 0 0;\n    height: 80vh;\n    text-align: center;\n    font-size: 20em;\n    opacity: .2;\n    animation: fadeOut 1s ease-in-out 0s infinite;\n}\n\n.swipeIcon{\n    width:100%;\n    text-align:center;\n    padding-top:40px;\n}\n.swipeIcon p{\n    font-size:.6em;\n    text-align:center;\n    display:block;\n}\n.swipeIcon img{\n    width:50px;\n    animation: swipeLeftRight 2s ease-in-out 0s infinite alternate;\n}\n\n.notifyBox{\n    width:100%;\n    text-align:center;\n    margin-top:5px;\n}\n\n.activeSkill{\n    background: linear-gradient(45deg, #2d6131 20%, #28a745 60%, #64bd63);\n}\n\n.ResourceBy{\n    display:none;\n}\n.activeResourceBy{\n    display:block;\n}\n.resourcedByGroup{\n    position: absolute;\n    left: 0;\n    height: 100%;\n    top: 0;\n    width: 100%;\n    font-size: 3em;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 492:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".btn-remove{\n    width:10px;\n    height:10px;\n    color:white;\n    float:right;\n    cursor:pointer;\n    margin-right:10%;\n}\n\n.btn-remove:hover{\n    color:rgb(241, 125, 125)\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n    font-size:.6em;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n.editGameData{\n    min-height:100vh;\n}\n\n.input-section{\n    margin-top:10px;border-top:thin solid grey;padding:10px;\n}\n\n.badge{margin-left:10px;}\n\ninput:focus{font-size:1em;}\n\n.story{\n    border-bottom: thin solid grey;\n    margin-bottom: 20px;\n    padding-bottom: 5px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 493:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 494:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".card{\n    cursor:pointer;\n    background:rgba(255, 255, 255, 0.938);\n    margin:20px 0px;\n    max-width:900px;\n    margin:40px auto;\n}\n\ngameCard:hover{\n    background:white;\n    border:thick solid grey;\n}\n.card-body{\n    color:black !important;\n}\n\n.gameCard{\n    margin:20px 0px;\n    width:100%;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n.cancelBtn{\n    position:fixed;\n    bottom:0px;\n    left:0px;\n    width:100%;\n    z-index:9999\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n    font-size:1em;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 495:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".card{\n    background:rgba(255, 255, 255, 0.938);\n    margin:20px 0px;\n}\n\ncard:hover{\n    background:white;\n}\n.card-body{\n    color:black !important;\n}\n\n.gameCard{\n    margin:20px 0px;\n    width:100%;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 496:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".loginHeader h1{\n    font-family:Aclonica;\n    font-size:5em;\n}\n\n.textgradient{\n    background: linear-gradient(45deg, #726193 20%, #e37b7c 60%, #ffe4b4);\n    animation: shine 5s linear infinite;\n    background-size: 300% auto;\n    background-clip: text;\n    color: rgba(255,255,255,.7);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: rgba(255,255,255,.7);\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n#loginForm{\n    animation: fadeIn 1s ease-in 0s;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 497:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, ".card{\n    cursor:pointer;\n    background:rgba(255, 255, 255, 0.938);\n    margin:20px 0px;\n    max-width:900px;\n    margin:40px auto;\n}\n\ngameCard:hover{\n    background:white;\n    border:thick solid grey;\n}\n.card-body{\n    color:black !important;\n}\n\n.gameCard{\n    margin:20px 0px;\n    width:100%;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n.cancelBtn{\n    position:fixed;\n    bottom:0px;\n    left:0px;\n    width:100%;\n    z-index:9999\n}\n\ninput{    \n    background:transparent;\n    border:thin dashed #f15f2d;\n    color:white;\n    font-size:1em;\n}\n\ninput:focus{\n    font-size:2em;\n}\n\ninput:disabled{\n    background:transparent;\n    border:thin dashed grey;\n    color:grey;\n}\n\n.overlay{\n    position:fixed;\n    overflow:scroll;\n    width:100%;\n    min-height:100vh;\n    left:0px;\n    top:0px;\n    padding-bottom:40px;\n    z-index:9999;\n}\n\n.fullscreenSplash{\n    position:absolute;\n    width:100%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 498:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, "\n\nbutton['submit']:disabled{background:grey}\n\ninput, textarea, select {\n    background: rgba(244, 222, 203, .2);\n    border: .5px dashed rgba(232, 202, 191, .5);\n}\n\n\n.overlay{\n    position:fixed;\n    top:0px;\n    left:0px;\n    z-index:9999;\n}\n\n.title-block{\n    width:100%;\n    position:relative;\n    display:inline-block;\n}\n\n.avatar{\n    width:30px;\n    height:auto;\n    border-radius:20px;\n    bottom:5px;\n}\n.avatar-title{\n    text-transform:uppercase;\n}\n\n.page-title{\n    margin-bottom:20px;\n}\n\n/* The slider itself */\n.slider {\n    -webkit-appearance: none;  /* Override default CSS styles */\n    -moz-appearance: none;\n         appearance: none;\n    width: 100%; /* Full-width */\n    height: 25px; /* Specified height */\n    outline: none; /* Remove outline */\n    opacity: 0.7; /* Set transparency (for mouse-over effects on hover) */ /* 0.2 seconds transition on hover */\n    transition: opacity .2s;\n}\n\n/* Mouse-over effects */\n.slider:hover {\n    opacity: 1; /* Fully shown on mouse-over */\n}\n\n/* The slider handle (use -webkit- (Chrome, Opera, Safari, Edge) and -moz- (Firefox) to override default look) */ \n.slider::-webkit-slider-thumb {\n    -webkit-appearance: none; /* Override default look */\n    appearance: none;\n    width: 25px; /* Set a specific slider handle width */\n    height: 25px; /* Slider handle height */\n    background: rgb(211, 94, 16); /* Green background */\n    cursor: pointer; /* Cursor on hover */\n}\n\n.slider::-moz-range-thumb {\n    width: 25px; /* Set a specific slider handle width */\n    height: 25px; /* Slider handle height */\n    background: rgb(211, 94, 16); /* Green background */\n    cursor: pointer; /* Cursor on hover */\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 499:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, "button['submit']:disabled{background:grey}\n\ninput, textarea, select {\n    background: rgba(244, 222, 203, .2);\n    border: .5px dashed rgba(232, 202, 191, .5);\n}\n\n.overlay{\n    position:fixed;\n    top:0px;\n    left:0px;\n    z-index:9999;\n}\n\n.avatar{\n    width:100%;\n    height:auto;\n    border-radius:20px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 500:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(10)(false);
// imports


// module
exports.push([module.i, "\n\nbutton['submit']:disabled{background:grey}\n\ninput, textarea, select {\n    background: rgba(244, 222, 203, .2);\n    border: .5px dashed rgba(232, 202, 191, .5);\n}\n\n\n.overlay{\n    position:fixed;\n    top:0px;\n    left:0px;\n    z-index:9999;\n}\n\n.title-block{\n    width:100%;\n    position:relative;\n    display:inline-block;\n}\n\n.avatar{\n    width:30px;\n    height:auto;\n    border-radius:20px;\n    bottom:5px;\n}\n.avatar-title{\n    text-transform:uppercase;\n}\n\n.page-title{\n    margin-bottom:20px;\n}\n\n/* The slider itself */\n.slider {\n    -webkit-appearance: none;  /* Override default CSS styles */\n    -moz-appearance: none;\n         appearance: none;\n    width: 100%; /* Full-width */\n    height: 25px; /* Specified height */\n    outline: none; /* Remove outline */\n    opacity: 0.7; /* Set transparency (for mouse-over effects on hover) */ /* 0.2 seconds transition on hover */\n    transition: opacity .2s;\n}\n\n/* Mouse-over effects */\n.slider:hover {\n    opacity: 1; /* Fully shown on mouse-over */\n}\n\n/* The slider handle (use -webkit- (Chrome, Opera, Safari, Edge) and -moz- (Firefox) to override default look) */ \n.slider::-webkit-slider-thumb {\n    -webkit-appearance: none; /* Override default look */\n    appearance: none;\n    width: 25px; /* Set a specific slider handle width */\n    height: 25px; /* Slider handle height */\n    background: rgb(211, 94, 16); /* Green background */\n    cursor: pointer; /* Cursor on hover */\n}\n\n.slider::-moz-range-thumb {\n    width: 25px; /* Set a specific slider handle width */\n    height: 25px; /* Slider handle height */\n    background: rgb(211, 94, 16); /* Green background */\n    cursor: pointer; /* Cursor on hover */\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 502:
/***/ (function(module, exports) {

module.exports = "\n<!-- <ng-container *ngIf=\"!projectService.logonData else game\" >\n\t<app-login></app-login>\n</ng-container> -->\n\n<!-- <ng-template #game> -->\n\t<div *ngIf=\"projectService.userType == 'admin'\" class=\"systemMessages\">\n\t\t<p>{{projectService.systemMessage.latestMessage}}</p>\n\t\t<span class=\"dataLight bg-playEggplant text-playSand\" [ngClass]=\"{'dataLightflash': projectService.systemMessage.sessionDataLoaded}\">S</span>\n\t\t<span class=\"dataLight bg-playSand text-playEggplant\" [ngClass]=\"{'dataLightflash':projectService.systemMessage.gameDataLoaded}\">G</span>\n\t</div>\n\t<div class=\"content-wrap\">\n\t\t\t\t<router-outlet></router-outlet>   \n\t\t\t\t<div style=\"clear:both;\"></div>\n\t</div>\n<!-- </ng-template> -->\n\n<div class=\"screenLockout\" *ngIf=\"projectService.screenLockout.lock\">\n\t\t<div class=\"lockMessage bg-playEggplant text-playSand\">\n\t\t\t{{projectService.screenLockout.message}}\n\t\t</div>\n</div>\n\n"

/***/ }),

/***/ 503:
/***/ (function(module, exports) {

module.exports = "\n    <div class=\"adminInstructions container-flex\">\n        <div class=\"row overlay bg-playSand text-playEggplant\">\n            <div class=\"section\">\n                <div class=\"col-md-12\">\n                    <h2>{{GAME_LIVE_STREAM.gameInstance.name}}</h2>\n                    <p>{{GAME_LIVE_STREAM.gameInstance.description}}</p>\n                </div>\n                    <div class=\"col-md-12\">\n                    <div class=\"alert alert-danger text-center\">Game is {{GAME_LIVE_STREAM.gameInstance.status}}</div>\n                    <div class=\"btn-group btn-block\">\n                        <div class=\"btn-group btn-block\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'active'\">\n                            <button class=\"btn col bg-success text-white\" (click)=\"openGame()\">Activate Game</button>\n                            <button class=\"btn col bg-danger text-white\" (click)=\"exitGame()\">Exit Game</button>\n                        </div>\n                        <button class=\"btn col bg-danger text-white\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'active'\" (click)=\"closeGame()\">Deactivate Game</button>\n                    </div>\n                </div>\n                <div class=\"col-md-12\" style=\"padding-top:10px;\">\n                    <!-- Instructions -->\n                </div>\n            </div>\n\n            <div class=\"section bg-playEggplant\">\n                    <!-- controller actions -->\n            </div>\n\n            <ng-container>\n                <div class=\"section\">\n                    <h4> RESULTS </h4>\n                    Hotspot: XXX interactions\n                </div>\n                <button class=\"btn btn-block btn-danger btn-large\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'ready'\" (click)=\"lockGame()\">End game with these results</button>\n            </ng-container>\n        </div>\n    </div>\n\n\n\n\n\n"

/***/ }),

/***/ 504:
/***/ (function(module, exports) {

module.exports = "\n<ng-container *ngIf=\"GAME_LIVE_STREAM\">\n<main id=\"content\" class=\"content bg-playGrape text-playSand\" role=\"main\" >\n<h1 class=\"page-title\">{{GAME_LIVE_STREAM.gameInstance.name}}</h1>\n\n<!-- BILLBOARD -->\n       <ng-container *ngIf=\"userType == 'billboard' else userScreen\" >\n            <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'active' && logonData.presentationID\">\n                    <!-- // The G slides must be hidden, rather than unloaded with *ngIf, so they hold their place in the presentation -->\n                      <div [ngClass]=\"{'hideSlides':SESSION_CONTROLLER_STREAM.activeGameInstance}\" class='embed-container embed-responsive embed-responsive-4by3'>\n                              <iframe [src]=\"slidesPresentationUrl\" frameborder=\"0\" width=\"1440\" height=\"839\" allowfullscreen=\"true\" mozallowfullscreen=\"true\" webkitallowfullscreen=\"true\"></iframe>\n                      </div>\n                      <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }.embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 105%; }.punch-viewer-content-fixed{width:100% !important; margin:0px !important}</style>\n            </ng-container>                  \n            <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'open'\">\n                    <div class=\"slide\">\n                        <!-- Billboard view -->\n                    </div>\n            </ng-container>\n        </ng-container>\n\n\n<!-- GAME -->\n<ng-template #userScreen>\n        <div id=\"mainGameSlide container-flex\">\n\n                <div class=\"row strap play-gradient1\"  *ngIf=\"screenController.screen == 0 && GAME_LIVE_STREAM.gameInstance.status == 'active' else showInstructions\" >\n                <div class=\"backgroundWords\"></div>\n                    <!-- a hack to make scroll tot op work on mobile -->\n                    <script>$(yourSelector).addClass(\"androidFix\").scrollTop(0).removeClass(\"androidFix\")</script> \n                     <div class=\"col-md-12 strap-inner\">\n                         <ng-container *ngIf=\"GAME_LIVE_STREAM.controller.activeCards\">\n                                <!-- USER INTERFACE -->\n                                <p>{{ getActiveView() }}</p>\n                         </ng-container>\n                    </div>\n                </div>\n\n                <ng-template #showInstructions>\n                    <div class=\"row overlay bg-playShell\">\n                            <div class=\"section\">\n                                <h2 class=\"text-playEggplant\">Defining the user by writing their journey as a poem</h2>\n                            </div>\n                            <div style=\"background-image:linear-gradient(rgba(20,20,20, 1), rgba(20,20,20, .5)),url(assets/img/example.png)\" class=\"section\" >\n                                <h1>How to play user poems</h1>\n                                <p><img class=\"gameHowToImg\" src=\"/assets/img/introScreens/userpoemsIntro.png\"></p>\n                            </div>\n                            <div class=\"section bg-playOrange\">\n                                    <h2 class=\"text-playSand\">One persona will be active at a time to add your user peoms. We have made it really easy, just follow the bounching ball... Don't over think too much, just put yourself in that persona' shoes. <br><br/></h2>\n                                    <h2 *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'open'\">Press play below to begin</h2>\n                            </div>\n                        </div>\n                </ng-template>\n\n            <!-- FOOTER -->\n            <!-- <app-game-footer (OnClickPlay)=\"changeScreen(0)\" [screenController]=\"screenController\" [GameStream]=\"GAME_LIVE_STREAM\" ></app-game-footer> -->\n          </div>\n</ng-template>\n\n    \n\n\n</main>\n</ng-container>\n\n<ng-template #loader>\n\n</ng-template>"

/***/ }),

/***/ 505:
/***/ (function(module, exports) {

module.exports = "\n\n<ng-container *ngIf=\"GAME_LIVE_STREAM\">\n\n        <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'closed' else showResults\"> <!--cked if the game has been completed and locked, is so then show the results !-->\n            <!-- ADMIN SCREEN -->\n            <ng-container *ngIf=\"userType == 'admin' else gameScreen\">\n                <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'waiting input data' || screenController.screen == 'gameData'; else gameController\">\n                        <!-- CREATE/EDIT GAME DATA-->\n                        <div class=\"container-flex editGameData bg-playEggplant text-white\">\n                                <div class=\"row\">\n                                    <div class=\"btn-group btn-block\" style=\"margin-bottom:20px;\">\n                                        <!-- <button class=\"btn col btn-success\" (click)=\"loadFromJira()\">JIRA data</button>\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromOW()\">Overwatch data</button> -->\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromGame()\">Game data</button>\n                                    </div>\n                                    <h1 class=\"text-center text-playOrange\"> GAME INPUTS</h1>\n                                    <div class=\"col-md-12\">\n                                        <h3 style=\"margin-top:30px;border-bottom:thin solid white;\">Game details</h3>\n                                        <p><strong>name:</strong> {{GAME_LIVE_STREAM.gameInstance.name}}</p>\n                                        <p><strong>Template:</strong> {{GAME_LIVE_STREAM.gameInstance.gameTemplate}}</p>\n                                        <p><strong>Description:</strong>  {{GAME_LIVE_STREAM.gameInstance.description}}</p>\n                                    </div>\n                                    <div class=\"col-md-12 text-playSand\">\n                                            <!-- CUSTOM GAME SPAWN CONTROLLER FIELDS -->\n                                            <div class=\"input-section\">\n                                                <h4 class=\"text-playOrange\">Hotspots</h4>\n                                                <div *ngFor=\"let hotspot of spawnGameData.gameInputs.RFIDhotspots; let i = index\" class=\"story text-playSand bg-playEggplant\">  \n                                                    <input #hotspotCardID placholder=\"Enter the RFID card UID\" value=\"{{hotspot.RFIDcardID}}\">\n                                                    <input #hotspotData placholder=\"Enter the hotspot data\" value=\"{{hotspot.data}}\">\n                                                </div>\n                                            </div>\n                                            <!-- CUSTOM GAME SPAWN CONTROLLER FIELDS -->\n                                    </div>\n                                    <div class=\"btn-group btn-block\" style=\"margin-top:20px;\">\n                                        <button class=\"btn btn-warning col\" (click)=\"updateGameData()\">Save to game</button>\n                                        <button class=\"btn btn-danger col\" (click)=\"screenController.screen = null;\">Cancel</button>\n                                    </div>\n                                </div>\n                            </div>\n                </ng-container>\n                <ng-template #gameController>\n                        <button class=\"btn btn-block btn-playEggplant\" style=\"border-radius:none;\" (click)=\"editGameData()\">Update Game Inputs</button>\n                        <MAXI01controller [GameLive]=\"GAME_LIVE_STREAM\"></MAXI01controller>\n                </ng-template>\n            </ng-container>\n\n\n\n            <!-- MAIN GAME SCREEN LOADER  -->\n            <ng-template #gameScreen>\n                    <MAXI01minigame [GameLive]=\"GAME_LIVE_STREAM\" (gameInstanceEnd)=\"saveUserEntry($event)\" [userType]=\"userType\" (saveUserInput)=\"saveUserEntry($event)\"></MAXI01minigame>\n            </ng-template>\n</ng-container>\n\n<!-- RESULTS REPORT --> <!-- game locked container -->\n<ng-template #showResults>\n        <div class=\"container-flex gameReport\">\n            <div class=\"btn-group btn-block actionButtons\" style=\"margin-bottom:20px;\" *ngIf=\"userType == 'admin'\">\n                <button class=\"btn col bg-danger text-white\" (click)=\"exitGame()\">Exit Game</button>\n                <button class=\"btn btn-warning col\" (click)=\"saveProjectData()\">Add to Overwatch Project Data</button>\n            </div>\n            <h1 class=\"text-white text-center text-playSand\"> GAME RESULTS</h1>\n            <div class=\"col-md-12 text-playSand\" style=\"margin-top:10px;border-top:thin solid grey;padding:10px;\" *ngFor=\"let addedUserStory of GAME_LIVE_STREAM.gameResults\">\n                    <blockquote>\n                    I'm <i class=\"text-playOrange\">{{addedUserStory.personaName}}</i>, <br>\n                    When i'm {{addedUserStory.location}} <br>\n                    and {{addedUserStory.situation}}, <br>\n                    I want to <i class=\"text-playOrange\">{{addedUserStory.want}}</i> <br>\n                    in order to <i class=\"text-playOrange\">{{addedUserStory.reason}}</i> <br>\n                    </blockquote>\n            </div>\n        </div>\n</ng-template>\n\n"

/***/ }),

/***/ 506:
/***/ (function(module, exports) {

module.exports = "\n    <div class=\"adminInstructions container-flex\">\n        <div class=\"row overlay bg-playSand text-playEggplant\">\n            <div class=\"section\">\n                <div class=\"col-md-12\">\n                    <h2>{{GAME_LIVE_STREAM.gameInstance.name}}</h2>\n                    <p>{{GAME_LIVE_STREAM.gameInstance.description}}</p>\n                </div>\n                    <div class=\"col-md-12\">\n                    <div class=\"alert alert-danger text-center\">Game is {{GAME_LIVE_STREAM.gameInstance.status}}</div>\n                    <div class=\"btn-group btn-block\">\n                        <div class=\"btn-group btn-block\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'active'\">\n                            <button class=\"btn col bg-success text-white\" (click)=\"openGame()\">Activate Game</button>\n                            <button class=\"btn col bg-danger text-white\" (click)=\"exitGame()\">Exit Game</button>\n                        </div>\n                        <button class=\"btn col bg-danger text-white\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'active'\" (click)=\"closeGame()\">Deactivate Game</button>\n                    </div>\n                </div>\n                <div class=\"col-md-12\" style=\"padding-top:10px;\">\n                    <!--  Game Facillitator Instructions -->\n                    <button *ngIf=\"!screenController.showprintSituations\" class=\"btn btn-block bg-playOrange\" (click)=\"screenController.showprintSituations = true\">Print User Situations</button>\n                    <!--  Game Facillitator Instructions -->\n                </div>\n            </div>\n\n            <div class=\"section bg-playEggplant\">\n                <div class=\"printSituations\" *ngIf=\"screenController.showprintSituations\">\n                        <div  *ngFor=\"let situation of GAME_LIVE_STREAM.gameInputs.situations\"> \n                            <div class=\"printSituationsContainer\">\n                                <div class=\"printSituationsHeader\"><span style=\"font-size:24px;font-family:aclonica;\">PLAY </span> <i> - workshops by The Digital Project Managers</i></div>\n                                <br><br>\n                                <div class=\"printKey\">USER SITUATION</div>\n                                <br><br>\n                                <hr>\n                                    <h2> {{situation}}</h2>\n                                <hr>\n                                <br><br><br>\n                                <small style=\"color:grey;font-size:10px\">\n                                SESSION: {{GAME_LIVE_STREAM.gameInstance.sessionID}} |     \n                                GAME: {{GAME_LIVE_STREAM.gameInstance._id}} | \n                                TEMPLATE: {{GAME_LIVE_STREAM.gameInstance.gameTemplate}} \n                                </small>\n\n                            </div>\n                        </div>\n                        <button class=\"btn btn-danger btn-block noPrint \" (click)=\"screenController.showprintSituations = false\">Close</button>\n                </div>\n\n                <p class=\"text-playSand small\">Select the first persona to begin the game:</p>\n                <div data-toggle=\"buttons\" style=\"overflow:scroll;\"> \n                        <button style=\"margin:10px;\" class=\"btn block-level btn-grey\" *ngFor=\"let persona of GAME_LIVE_STREAM.gameInputs.personas\" [ngClass]=\"{'btn-success': GAME_LIVE_STREAM.controller.activePersona == persona.ID}\" (click)=\"updateActivePersona(persona.ID)\">{{persona.name}}\n                        </button>\n                </div>\n\n            </div>\n\n            <ng-container *ngIf=\"activePersonaUserStories()\">\n                <div class=\"section\">\n                    <h4> RESULTS FOR THIS PERSONA </h4>\n                    <div class=\"col-md-12\" style=\"margin-bottom:5px;\" *ngFor=\"let addedUserStory of activePersonaUserStories()\">\n                            <blockquote>\n                            I'm {{addedUserStory.personaName}}, <br>\n                            When i'm <span class=\"lowercase\">{{addedUserStory.location}} </span><br>\n                            and <span class=\"lowercase\">{{addedUserStory.situation}}, </span><br>\n                            I want to <span class=\"lowercase\">{{addedUserStory.want}} </span><br>\n                            in order to <span class=\"lowercase\">{{addedUserStory.reason}} </span><br>\n                            </blockquote>\n                    </div>\n                </div>\n                <button class=\"btn btn-block btn-danger btn-large\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'ready'\" (click)=\"lockGame()\">End game with these results</button>\n            </ng-container>\n        </div>\n        <!-- <app-game-admin-buttons (OnDiscard)=\"discardGame()\" (OnSave)=\"gameInstanceClosePostback()\"></app-game-admin-buttons> -->\n    </div>\n\n\n\n\n\n"

/***/ }),

/***/ 507:
/***/ (function(module, exports) {

module.exports = "\n<ng-container *ngIf=\"GAME_LIVE_STREAM\">\n<main id=\"content\" class=\"content bg-playGrape text-playSand\" role=\"main\" >\n\n<!-- BILLBOARD -->\n       <ng-container *ngIf=\"userType == 'billboard' else userScreen\" >                 \n                    <div class=\"slide\" style=\"padding:5%;\">\n                        <ng-container *ngIf=\"activePersonaData() else noPersonaActive\">\n                            <h1 style=\"margin-top:0px;margin-bottom:30px;\"> {{ activePersonaUserStories().length}} stories for  {{activePersonaData().name}}</h1>\n                            <div class=\"row\">\n                                <blockquote class=\"col col-md-4\" style=\"text-align:left; color:white;border-left:medium solid white;margin-top:30px;\" *ngFor=\"let addedUserStory of reverseArray(activePersonaUserStories())\">\n                                        When I am <span class=\"lowercase\">{{addedUserStory.location}} </span>\n                                        and <span class=\"lowercase\">{{addedUserStory.situation}},</span> <br>\n                                        I want to <span class=\"lowercase\">{{addedUserStory.want}},</span> <br>\n                                        In order to <span class=\"lowercase\">{{addedUserStory.reason}}.</span> <br>\n                                </blockquote>\n                            </div>\n                        </ng-container>\n                        <ng-template noPersonaActive><div class=\"col-sm-12\">Waiting for the first persona...</div></ng-template>\n                    </div>\n        </ng-container>\n\n\n<!-- GAME -->\n<ng-template #userScreen>\n        <h1 class=\"page-title\">{{GAME_LIVE_STREAM.gameInstance.name}}</h1>\n        <div id=\"mainGameSlide container-flex\">\n\n                <div class=\"row strap play-gradient1\"  *ngIf=\"screenController.screen == 0 && GAME_LIVE_STREAM.gameInstance.status == 'active' else showInstructions\" >\n                <div class=\"backgroundWords\"></div>\n                    <!-- a hack to make scroll tot op work on mobile -->\n                    <script>$(yourSelector).addClass(\"androidFix\").scrollTop(0).removeClass(\"androidFix\")</script> \n                     <div class=\"col-md-12 strap-inner\">\n                           <ng-container *ngIf=\"activePersonaData() else noPersonaActive\">\n                            <p>I am <strong style=\"text-transform:uppercase;font-size:1.3em;\" class=\"text-playSand\">{{activePersonaData().name}},</strong></p>\n                            \n                            <form [formGroup]=\"storyForm\" (ngSubmit)=\"addUserStory($event)\">\n                                <div class=\"inputgroup\">\n                                    <p>When i'm \n                                        <select formControlName=\"location\">  \n                                            <option  *ngFor=\"let location of GAME_LIVE_STREAM.gameInputs.locations\" value=\"{{location}}\">{{location}}</option>\n                                        </select> \n                                    </p>\n                                </div>\n                                <div class=\"inputgroup\">\n                                    <p>And\n                                        <select formControlName=\"situation\">\n                                            <option *ngFor=\"let situation of GAME_LIVE_STREAM.gameInputs.situations\" value=\"{{situation}}\"  >{{situation}}</option>\n                                        </select> \n                                    </p>\n                                </div>\n                                <div class=\"inputgroup\" >\n                                    <p>I want to ...\n                                        <textarea required rows=\"4\" style=\"width:100%;\" formControlName=\"want\"></textarea> \n                                    </p>\n                                  </div>\n                                <div class=\"inputgroup\" >\n                                    <p>In order to ...\n                                        <textarea required rows=\"4\" style=\"width:100%;\" formControlName=\"reason\" ></textarea>\n                                    </p>\n                                </div>\n                                <button type=\"submit\" class=\"btn btn-action col col-sx-12\" [disabled]=\"!screenController.addForm.show || !storyForm.valid\">\n                                        <ng-container *ngIf=\"screenController.addForm.show else adding\" style=\"text-transform: uppercase;\">\n                                            Add this story\n                                        </ng-container>\n                                        <ng-template #adding>Adding Story...</ng-template>\n                                </button>\n                            </form>\n                        </ng-container>\n                        <ng-template #noPersonaActive>\n                            <p style=\"text-align:center;\">Waiting for the first persona...</p>\n                        </ng-template>\n                    </div>\n                </div>\n\n                <ng-template #showInstructions>\n                    <div class=\"row overlay bg-playShell\">\n                            <div class=\"section\">\n                                <h2 class=\"text-playEggplant\">Defining the user by writing their journey as a statement</h2>\n                            </div>\n                            <div style=\"background-image:linear-gradient(rgba(20,20,20, 1), rgba(20,20,20, .5)),url(assets/img/example.png)\" class=\"section\" >\n                                <h1>How to play user poems</h1>\n                                <p><img class=\"gameHowToImg\" src=\"/assets/img/introScreens/userpoemsIntro.png\"></p>\n                            </div>\n                            <div class=\"section bg-playOrange\">\n                                    <h2 class=\"text-playSand\">One persona will be active at a time to add your user peoms. We have made it really easy, just follow the bounching ball... Don't over think too much, just put yourself in that persona's shoes. <br><br/></h2>\n                                    <h2 *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'open'\">Press play below to begin</h2>\n                            </div>\n                        </div>\n                </ng-template>\n\n            <!-- FOOTER -->\n            <app-game-footer (OnClickPlay)=\"changeScreen(0)\" [screenController]=\"screenController\" [GameStream]=\"GAME_LIVE_STREAM\" ></app-game-footer>\n          </div>\n</ng-template>\n\n    \n\n\n</main>\n</ng-container>\n\n<ng-template #loader>\n\n</ng-template>"

/***/ }),

/***/ 508:
/***/ (function(module, exports) {

module.exports = "\n\n<ng-container *ngIf=\"GAME_LIVE_STREAM\">\n\n        <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'closed' else showResults\"> <!--cked if the game has been completed and locked, is so then show the results !-->\n            <!-- ADMIN SCREEN -->\n            <ng-container *ngIf=\"userType == 'admin' else gameScreen\">\n                <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'waiting input data' || screenController.screen == 'gameData'; else gameController\">\n                        <!-- CREATE/EDIT GAME DATA-->\n                        <div class=\"container-flex editGameData bg-playEggplant text-white\">\n                                <div class=\"row\">\n                                    <div class=\"btn-group btn-block\" style=\"margin-bottom:20px;\">\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromOSession()\">Session Store</button>\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromOW()\">Overwatch data</button>\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromGame()\">Game data</button>\n                                    </div>\n                                    <h1 class=\"text-center text-playOrange\"> GAME INPUTS</h1>\n                                    <div class=\"col-md-12\">\n                                        <h3 style=\"margin-top:30px;border-bottom:thin solid white;\">Game details</h3>\n                                        <p><strong>name:</strong> {{GAME_LIVE_STREAM.gameInstance.name}}</p>\n                                        <p><strong>Template:</strong> {{GAME_LIVE_STREAM.gameInstance.gameTemplate}}</p>\n                                        <p><strong>Description:</strong>  {{GAME_LIVE_STREAM.gameInstance.description}}</p>\n                                    </div>\n                                    <div class=\"col-md-12 text-playSand\">\n                                                <div class=\"input-section\">\n                                                    <h4 class=\"text-playOrange\">Locations</h4>\n                                                    <div *ngFor=\"let location of spawnGameData.gameInputs.locations; let i = index\" class=\"badge text-playSand bg-playEggplant\">  \n                                                        {{location}}\n                                                        <span class=\"btn-remove\" (click)=\"removeFromGameData(i, 'locations')\"></span>\n                                                    </div>\n                                                    <input #addlocation (keyup.enter)=\"addToGameData(addlocation.value, 'locations');addlocation.value = ''\">\n                                                </div>\n                                    \n                                                <div class=\"input-section\">\n                                                    <h4 class=\"text-playOrange\"> Situations </h4>\n                                                    <span *ngFor=\"let situation of spawnGameData.gameInputs.situations  let i = index\" class=\"badge text-playSand bg-playEggplant\">\n                                                        {{situation}}\n                                                        <span class=\"btn-remove\" (click)=\"removeFromGameData(i, 'situations')\"></span>\n                                                    </span>\n                                                    <input #addsituation (keyup.enter)=\"addToGameData(addsituation.value, 'situations');addsituation.value=''\" >\n                                                </div>\n                                    \n                                                <div class=\"input-section\">\n                                                    <h4 class=\"text-playOrange\"> Personas </h4>\n                                                    <p *ngFor=\"let persona of spawnGameData.gameInputs.personas  let i = index\">\n                                                        <strong>{{persona.name}}</strong> {{persona.description}}\n                                                        <span class=\"btn-remove\" (click)=\"removeFromGameData(i, 'personas')\"></span>\n                                                        </p>\n                                                    <!-- INPUT DISABLED BECAUSE OF PERSONA LINKING ISSUES -->\n                                                    <!-- <input #addpersona (keyup.enter)=\"addToGameData({name:addpersona.value}, 'personas');addpersona.value = ''\" > -->\n                                                </div>\n                                    </div>\n                                    <div class=\"btn-group btn-block\" style=\"margin-top:20px;\">\n                                        <button class=\"btn btn-warning col\" (click)=\"updateGameData()\">Save to game</button>\n                                        <button class=\"btn btn-danger col\" (click)=\"screenController.screen = null;\">Cancel</button>\n                                    </div>\n                                </div>\n                            </div>\n                </ng-container>\n                <ng-template #gameController>\n                        <button class=\"btn btn-block btn-playEggplant\" style=\"border-radius:none;\" (click)=\"editGameData()\">Update Game Inputs</button>\n                        <GAME01controller [GameLive]=\"GAME_LIVE_STREAM\"></GAME01controller>\n                </ng-template>\n            </ng-container>\n\n\n\n            <!-- MAIN GAME SCREEN LOADER  -->\n            <ng-template #gameScreen>\n                    <GAME01minigame [GameLive]=\"GAME_LIVE_STREAM\" (gameInstanceEnd)=\"saveUserEntry($event)\" [userType]=\"userType\" (saveUserInput)=\"saveUserEntry($event)\"></GAME01minigame>\n            </ng-template>\n</ng-container>\n\n<!-- RESULTS REPORT --> <!-- game locked container -->\n<ng-template #showResults>\n        <div class=\"container-flex gameReport\">\n            <div class=\"btn-group btn-block actionButtons\" style=\"margin-bottom:20px;\" *ngIf=\"userType == 'admin'\">\n                <button class=\"btn col bg-danger text-white\" (click)=\"exitGame()\">Exit Game</button>\n                <button class=\"btn btn-warning col\"  (click)=\"saveSessionData()\">Add to Session Store</button>\n            </div>\n            <h1 class=\"text-white text-center text-playSand\"> GAME RESULTS</h1>\n            <div class=\"col-md-12 text-playSand\" style=\"margin-top:10px;border-top:thin solid grey;padding:10px;\" *ngFor=\"let addedUserStory of GAME_LIVE_STREAM.gameResults\">\n                    <blockquote>\n                    I'm <i class=\"text-playOrange\">{{addedUserStory.personaName}}</i>, <br>\n                    When i'm {{addedUserStory.location}} <br>\n                    and {{addedUserStory.situation}}, <br>\n                    I want to <i class=\"text-playOrange\">{{addedUserStory.want}}</i> <br>\n                    in order to <i class=\"text-playOrange\">{{addedUserStory.reason}}</i> <br>\n                    </blockquote>\n            </div>\n            <button *ngIf=\"userType == 'admin'\" class=\"btn btn-warning btn-block\" (click)=\"saveProjectData()\">Add to Overwatch Data</button>\n        </div>\n</ng-template>\n\n"

/***/ }),

/***/ 509:
/***/ (function(module, exports) {

module.exports = "\n    <div class=\"adminInstructions container-flex\">\n        <div class=\"row overlay bg-playSand text-playEggplant\">\n            <div class=\"section\">\n                <div class=\"col-md-12\">\n                    <h2>{{GAME_LIVE_STREAM.gameInstance.name}}</h2>\n                    <p>{{GAME_LIVE_STREAM.gameInstance.description}}</p>\n                </div>\n                    <div class=\"col-md-12\">\n                    <div class=\"alert alert-danger text-center\">Game is {{GAME_LIVE_STREAM.gameInstance.status}}</div>\n                    <div class=\"btn-group btn-block\">\n                        <div class=\"btn-group btn-block\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'active'\">\n                            <button class=\"btn col bg-success text-white\" (click)=\"openGame()\">Activate Game</button>\n                            <button class=\"btn col bg-danger text-white\" (click)=\"exitGame()\">Exit Game</button>\n                        </div>\n                        <button class=\"btn col bg-danger text-white\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'active'\" (click)=\"closeGame()\">Deactivate Game</button>\n                    </div>\n                </div>\n\n                <div class=\"col-md-12\" style=\"padding-top:10px;\">\n                    <!--  Game Facillitator Instructions -->\n                    <!--  Game Facillitator Instructions -->\n                </div>\n            </div>\n\n            <div class=\"section bg-playEggplant\">\n                    <!-- Game Controlls -->\n                    <div class=\"printStories\" *ngIf=\"screenController.showPrintStories\">\n                            <div  *ngFor=\"let userStory of GAME_LIVE_STREAM.gameInputs.userStories\"> \n                                <div class=\"printStoryContainer\">\n                                    <div class=\"printStoryHeader\"><span style=\"font-size:24px;;font-family:aclonica;\">PLAY </span> <i> - workshops by The Digital Project Managers</i></div>\n\n                                    <br><br>\n                                    <h1 class=\"printKey\">{{userStory.key}}</h1>\n                                    <br><br>\n\n                                    <hr>\n                                        <p>I'm {{userStory.personaName}}, </p><br>\n                                        <p>When i'm <strong>{{userStory.location}}</strong> and <strong>{{userStory.situation}}</strong></p>\n                                        <br>\n                                        <p>I want <strong>{{userStory.want}} </strong> order to <strong> {{userStory.reason}} </strong> </p>\n                                    <hr>\n\n                                    <br><br><br>\n\n                                    <small style=\"color:grey;font-size:10px\">\n                                    SESSION: {{GAME_LIVE_STREAM.gameInstance.sessionID}} |     \n                                    GAME: {{GAME_LIVE_STREAM.gameInstance._id}} | \n                                    TEMPLATE: {{GAME_LIVE_STREAM.gameInstance.gameTemplate}} \n                                    </small>\n\n                                </div>\n                            </div>\n                            <button class=\"btn btn-danger btn-block noPrint \" (click)=\"screenController.showPrintStories = false\">Cancel</button>\n                            <button class=\"btn btn-danger btn-block noPrint \" (click)=\"print()\">Print</button>\n                    </div>\n                    <button *ngIf=\"!screenController.showPrintStories\" class=\"btn btn-block btn-success\" (click)=\"screenController.showPrintStories = true\">Print User Stories</button>\n                    <!-- Game Controlls -->\n            </div>\n\n            <ng-container>\n                <div class=\"section\">\n                    <button class=\"btn btn-warning pull-right\" (click)=\"processRawResultsData()\">REFRESH</button>\n                    <h4> RESULTS</h4>\n\n                    <!-- Game Live Results -->\n                    <ng-container *ngIf=\"filteredLiveResultsData else noResults\">\n                            <div class=\"col-md-12 small text-left\" style=\"padding:0% 10%;\"  *ngFor=\"let userStory of filteredLiveResultsData\"> \n                                    {{userStory.personaName}}, <br> When i'm {{userStory.location}} and {{userStory.situation}} <br/> \n                                    I want {{userStory.want}} in order to {{userStory.reason}} <br> \n                                    <span class=\"text-playOrange\">VOTE: {{userStory.vote}} |<strong> INVEST: {{userStory.investment}}</strong></span>\n                                    <hr>\n                            </div>\n                    </ng-container>\n                    <ng-template #noResults ><div class=\"col-sm-12 small text-left\" style=\"padding:20px 10%;\"  >No results to show</div> </ng-template>\n                    <!-- Game Live Results -->\n\n                </div>\n                <button class=\"btn btn-block btn-danger btn-large\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'ready'\" (click)=\"lockGame()\">End game with these results</button>\n            </ng-container>\n        </div>\n        <!-- <app-game-admin-buttons (OnDiscard)=\"discardGame()\" (OnSave)=\"gameInstanceClosePostback()\"></app-game-admin-buttons> -->\n    </div>\n\n\n\n"

/***/ }),

/***/ 510:
/***/ (function(module, exports) {

module.exports = "\n<ng-container *ngIf=\"GAME_LIVE_STREAM\">\n<main id=\"content\" class=\"content bg-playGrape text-playSand\" role=\"main\" >\n<h1 class=\"page-title\">{{GAME_LIVE_STREAM.gameInstance.name}}</h1>\n\n\n<!-- BILLBOARD -->\n       <ng-container *ngIf=\"userType == 'billboard' else userScreen\" >\n            <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'active' && logonData.presentationID\">\n                    <!-- // The G slides must be hidden, rather than unloaded with *ngIf, so they hold their place in the presentation -->\n                      <div [ngClass]=\"{'hideSlides':SESSION_CONTROLLER_STREAM.activeGameInstance}\" class='embed-container embed-responsive embed-responsive-4by3'>\n                              <iframe [src]=\"slidesPresentationUrl\" frameborder=\"0\" width=\"1440\" height=\"839\" allowfullscreen=\"true\" mozallowfullscreen=\"true\" webkitallowfullscreen=\"true\"></iframe>\n                      </div>\n                      <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }.embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 105%; }.punch-viewer-content-fixed{width:100% !important; margin:0px !important}</style>\n            </ng-container>                  \n            <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'open'\">\n                <div class=\"slide\" style=\"color:white;\">\n                    <h1> AUCTION </h1>\n                </div>\n            </ng-container>\n        </ng-container>\n\n\n<!-- GAME -->\n<ng-template #userScreen>\n        <div id=\"mainGameSlide container-flex \">\n        <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInputs.userStories else noData\">\n                <ng-container *ngIf=\"screenController.screen == 0 && GAME_LIVE_STREAM.gameInstance.status == 'active'\">\n                        <ng-container *ngIf=\"targetUserStory() else loadUserstory\">\n                                <button  *ngIf=\"targetUserStory()\" class=\"btn  bg-playOrange btn-block large\" (click)=\"screenController.targetUserstory = null\">Save Vote</button>\n                                <div class=\"row strap play-gradient1\" style=\"padding:0% 0% 30px 0px;\">\n                                        <!-- a hack to make scroll tot op work on mobile -->\n                                        <script>$(yourSelector).addClass(\"androidFix\").scrollTop(0).removeClass(\"androidFix\")</script>\n                                        \n                                        <div style=\"padding:5%\">\n                                                <div class=\"voteCard-wrap spinInRight\" *ngFor=\"let card of getStoryCards();let i = index\">\n                                                        <span class=\"voteChip\" (click)=\"storyRemoveCard(i)\"> <img  id=\"votechip-{{i}}\" src=\"assets/img/casino_chip.png\" class=\" \"><i>{{card}}</i></span>\n                                                </div>\n                                        </div>\n                                        <div class=\"col-md-12 strap-inner\" style=\"margin-top:0;padding-top:0px;\">\n                                                <ng-container *ngIf=\"targetUserStory()\">\n                                                        <div class=\"col align-items-center text-interplaySand\" style=\"padding-top:20px;\" >\n                                                                <div class=\"col inputgroup\">\n                                                                        I'm <span class=\"emphasised\">{{targetUserStory().personaName}}</span> \n                                                                </div>\n                                                                <div class=\"col inputgroup\">\n                                                                        When I'm <span class=\"emphasised\">{{targetUserStory().location}}</span>\n                                                                </div>\n                                                                <div class=\"col  inputgroup\">\n                                                                        and feeling <span class=\"emphasised\">{{targetUserStory().situation}}</span>\n                                                                </div>\n                                                                <div class=\"col inputgroup\">\n                                                                        <span class=\"emphasised\">I want to {{targetUserStory().want}}</span>\n                                                                </div>\n                                                                <div class=\"col  inputgroup\" >\n                                                                        In order to ... <span class=\"emphasised\">{{targetUserStory().reason}}</span>\n                                                                </div>\n                                                        </div>\n                                        \n                                                </ng-container>\n                                        </div>\n                                </div>\n                        </ng-container>\n\n                        <ng-template #loadUserstory>\n                                <div class=\"row strap play-gradient1\" style=\"min-height:50vh\">\n                                        <ng-container *ngIf=\"!screenController.remainingCards.length\">\n                                                        <p class=\"alert alert-success text-center\" style=\"width:100%;padding:10px;\" >No vote tokens are remaining. <br> Submit your entry with the button below, or remove votes by entering another user story key.</p>\n                                                        <button [disabled]=\"saveInProgress\" (click)=\"saveResults()\" class=\"btn btn-block btn-success large\" style=\"padding: 50px 20px;margin-bottom:40px;\">I'm done!</button>\n                                        </ng-container>\n\n                                        <div *ngIf=\"!targetUserStory()\" style=\"margin:0px auto;\">\n                                                <i class=\"badge badge-danger small\" *ngIf=\"screenController.showKeyError\" style=\"text-align: center;\n                                                width: 100%;\">Key not found</i>\n                                                <input (click)=\"screenController.showKeyError = null\" class=\"form-control border-gradient large\" #userStoryKey placeholder=\"Story key\">\n                                                <button class=\"btn bg-playOrange btn-block large\" [disabled]=\"!userStoryKey.value\" (click)=\"openUserStory(userStoryKey.value)\">OPEN</button>\n                                        </div>\n                                </div>\n                        </ng-template>\n\n                        <div class=\"col align-items-center cardsAvailable\" *ngIf=\"screenController.remainingCards.length\">\n                                <strong style=\"padding-left:5px;width:100%;display:block;\"> YOUR TOKENS </strong>\n                                <div class=\"voteCard-wrap\" *ngFor=\"let card of screenController.remainingCards;let i = index\">\n                                        <span class=\"voteChip\" (click)=\"storyAddCard(i)\"> <img  id=\"votechip-{{i}}\" src=\"assets/img/casino_chip.png\" class=\" \"><i>{{card}}</i></span>\n                                </div>\n                        </div>\n                </ng-container>\n\n                <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'active' && screenController.screen == 'done'\">\n                        <div class=\"row strap play-gradient1\">\n                                <div class=\"col-md-12 strap-inner\" style=\"margin-top:0;padding-top:0px;\">\n                                         <p> All Done! Waiting for the game to finish.</p>\n                                </div>\n                        </div>\n                </ng-container>\n\n                <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'active' || screenController.screen == 'gameInstructions'\">\n                        <div class=\"row overlay bg-playSand\">\n                                <div class=\"section\">\n                                        <h2 class=\"text-playEggplant\">This game sets the scene for what user stories we should focus on the most.</h2>\n                                </div>\n                                <div style=\"background-image:linear-gradient(rgba(20,20,20, 1), rgba(20,20,20, .5)),url(assets/img/example.png)\" class=\"section\">\n                                        <h1>How to play Story Silent Auction</h1>\n                                        <p><img class=\"gameHowToImg\" src=\"/assets/img/introScreens/auctionIntro.png\"></p>\n                                </div>\n                                <div class=\"section bg-playOrange\">\n                                        <h2 class=\"text-playSand\">You have 100 points worth of tokens to put towards your favorite stories. But not all the tokens are equal, so you have to decide which story you really like the best.<br/><br/>\n                                        Press play below to begin.</h2>\n                                </div>\n                                <img  src=\"assets/img/casino_chip.png\" style=\"display:none\"> <!-- image pre-loader !-->\n                            </div>\n                </ng-container>\n\n                <!-- FOOTER -->\n                <app-game-footer (OnClickPlay)=\"changeScreen(0)\" [screenController]=\"screenController\" [GameStream]=\"GAME_LIVE_STREAM\" ></app-game-footer>\n        </ng-container>\n        <ng-template #noData>\n                <p> Waiting for the session data to be loaded into the game...</p>\n        </ng-template>\n        </div>\n</ng-template>\n\n    \n\n\n</main>\n</ng-container>\n\n<ng-template #loader>\n\n</ng-template>"

/***/ }),

/***/ 511:
/***/ (function(module, exports) {

module.exports = "\n\n<ng-container *ngIf=\"GAME_LIVE_STREAM\">\n\n        <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'closed' else showResults\"> <!--cked if the game has been completed and locked, is so then show the results !-->\n            <!-- ADMIN SCREEN -->\n            <ng-container *ngIf=\"userType == 'admin' else gameScreen\">\n                <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'waiting input data' || screenController.screen == 'gameData'; else gameController\">\n                        <!-- CREATE/EDIT GAME DATA-->\n                        <div class=\"container-flex editGameData bg-playEggplant text-white\">\n                                <div class=\"row\">\n                                    <div class=\"btn-group btn-block\" style=\"margin-bottom:20px;\">\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromOSession()\">Session Store</button>\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromOW()\">Overwatch data</button>\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromGame()\">Game data</button>\n                                    </div>\n\n                                    <input class=\"voteCap block-level\" style=\"width:100%;\" #voteCap (blur)=\"screenController.voteCap = voteCap.value\" placeholder=\"enter # of votes\"> \n                                    <p class=\"small text-center\" style=\"width:100%;\" >Change the vote cap to filter out stories with lower votes from the data load. Enter -1 to allow all stories. Click a green buton above the start the load.</p> \n\n                                    <h1 class=\"text-center text-playOrange\"> GAME INPUTS</h1>\n                                    <div class=\"col-md-12\">\n                                        <h3 style=\"margin-top:30px;border-bottom:thin solid white;\">Game details</h3>\n                                        <p><strong>Name:</strong> {{GAME_LIVE_STREAM.gameInstance.name}}</p>\n                                        <p><strong>Template:</strong> {{GAME_LIVE_STREAM.gameInstance.gameTemplate}}</p>\n                                        <p><strong>Description:</strong>  {{GAME_LIVE_STREAM.gameInstance.description}}</p>\n                                    </div>\n                                    <div class=\"col-md-12 text-playSand\">\n                                                <!-- CUSTOM GAME SPAWN CONTROLLER FIELDS -->\n                                                <div class=\"input-section\">\n                                                    <div class=\"col\" style=\"text-align:center;\">\n                                                        <span class=\"small\" *ngIf=\"spawnGameData.gameInputs.userStories\">{{spawnGameData.gameInputs.userStories.length}} stories loaded </span> \n                                                    </div>\n                                                    <h4 class=\"text-playOrange\">User Stories</h4>\n                                                    <div *ngFor=\"let userStory of spawnGameData.gameInputs.userStories; let i = index\" class=\"story text-playSand bg-playEggplant\">  \n                                                        <span class=\"btn-remove badge\" (click)=\"removeFromGameData(i, 'userStories')\">remove</span>\n                                                        I'm <i class=\"text-playOrange\">{{userStory.personaName}}</i>, <br>\n                                                        When i'm {{userStory.location}} and {{userStory.situation}}, <br>\n                                                        I want to <i class=\"text-playOrange\">{{userStory.want}}</i> <br>\n                                                        in order to <i class=\"text-playOrange\">{{userStory.reason}}</i> <br>\n                                                        VOTE: {{userStory.vote}} <br>\n                                                    </div>\n                                                </div>\n                                                <!-- CUSTOM GAME SPAWN CONTROLLER FIELDS -->\n                                    </div>\n                                    <div class=\"btn-group btn-block\" style=\"margin-top:20px;\">\n                                        <button class=\"btn btn-warning col\" (click)=\"updateGameData()\">Save to game</button>\n                                        <button class=\"btn btn-danger col\" (click)=\"screenController.screen = null;\">Cancel</button>\n                                    </div>\n                                </div>\n                            </div>\n                </ng-container>\n                <ng-template #gameController>\n                        <button class=\"btn btn-block btn-playEggplant\" style=\"border-radius:none;\" (click)=\"editGameData()\">Update Game Inputs</button>\n                        <GAME05controller [GameLive]=\"GAME_LIVE_STREAM\"></GAME05controller>\n                </ng-template>\n            </ng-container>\n\n            <!-- MAIN GAME SCREEN LOADER  -->\n            <ng-template #gameScreen>\n                    <GAME05minigame [GameLive]=\"GAME_LIVE_STREAM\" (gameInstanceEnd)=\"saveUserResults($event)\" [userType]=\"userType\" (saveUserInput)=\"null\"></GAME05minigame>\n            </ng-template>\n\n</ng-container>\n\n<!-- RESULTS REPORT --> <!-- (game locked container) -->\n<ng-template #showResults>\n        <div class=\"container-flex gameReport\">\n            <div class=\"btn-group btn-block actionButtons\" style=\"margin-bottom:20px;\" *ngIf=\"userType == 'admin'\">\n                <button class=\"btn col bg-danger text-white\" (click)=\"exitGame()\">Exit Game</button>\n                <button class=\"btn btn-warning col\"  (click)=\"saveSessionData()\">Add to Session Store</button>\n            </div>\n            <h1 class=\"text-white text-center text-playSand\"> GAME RESULTS</h1>\n            <div class=\"col-md-12 text-playSand\" style=\"margin-top:10px;border-top:thin solid grey;padding:10px;\" *ngFor=\"let addedUserStory of processRawResultsData(GAME_LIVE_STREAM.gameResults)\">\n                    <blockquote>\n                    I'm <i class=\"text-playOrange\">{{addedUserStory.personaName}}</i>, <br>\n                    When i'm {{addedUserStory.location}} <br>\n                    and {{addedUserStory.situation}}, <br>\n                    I want to <i class=\"text-playOrange\">{{addedUserStory.want}}</i> <br>\n                    in order to <i class=\"text-playOrange\">{{addedUserStory.reason}}</i> <br>\n                    <p>Vote: {{addedUserStory.vote}} | <span class=\"text-playOrange\"><strong> INVEST: {{addedUserStory.investment}}</strong></span></p>\n                    </blockquote>\n            </div>\n            <div style=\"clear:both;\"></div>\n            <button *ngIf=\"userType == 'admin'\" class=\"btn btn-warning btn-block\" (click)=\"saveProjectData()\">Add to Overwatch Data</button>\n        </div>\n</ng-template>\n\n"

/***/ }),

/***/ 512:
/***/ (function(module, exports) {

module.exports = "\n    <div class=\"adminInstructions container-flex\">\n        <div class=\"row overlay bg-playSand text-playEggplant\">\n            <div class=\"section\">\n                <div class=\"col-md-12\">\n                    <h2>{{GAME_LIVE_STREAM.gameInstance.name}}</h2>\n                    <p>{{GAME_LIVE_STREAM.gameInstance.description}}</p>\n                </div>\n                    <div class=\"col-md-12\">\n                    <div class=\"alert alert-danger text-center\">Game is {{GAME_LIVE_STREAM.gameInstance.status}}</div>\n                    <div class=\"btn-group btn-block\">\n                        <div class=\"btn-group btn-block\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'active'\">\n                            <button class=\"btn col bg-success text-white\" (click)=\"openGame()\">Activate Game</button>\n                            <button class=\"btn col bg-danger text-white\" (click)=\"exitGame()\">Exit Game</button>\n                        </div>\n                        <button class=\"btn col bg-danger text-white\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'active'\" (click)=\"closeGame()\">Deactivate Game</button>\n                    </div>\n                </div>\n\n                <div class=\"col-md-12\" style=\"padding-top:10px;\">\n                    <!--  Game Facillitator Instructions -->\n\n                    <!--  Game Facillitator Instructions -->\n                </div>\n            </div>\n\n            <div class=\"section bg-playEggplant\">\n                    <!-- Game Controlls -->\n                    <div class=\"btn-group fullwidth col col-xs-10\" data-toggle=\"buttons\"> \n                            <button class=\"btn btn-playOrange btn-block\" [ngClass]=\"{'btn-danger':!GAME_LIVE_STREAM.controller.timerOn}\" (click)=\"toggleTimer()\">\n                                <ng-container *ngIf=\"!GAME_LIVE_STREAM.controller.timerOn else timerStarted\"> TIMER OFF (click to start timer) </ng-container>\n                                <ng-template #timerStarted> TIMER RUNNING (click to stop)</ng-template>\n                            </button>\n                    </div>\n                    <!-- Game Controlls -->\n            </div>\n\n            <ng-container>\n                <div class=\"section\">\n                    <h4> RESULTS</h4>\n\n                    <!-- Game Live Results -->\n                    <button class=\"btn btn-warning\" (click)=\"processRawResultsData()\">REFRESH</button>\n                    <ng-container *ngIf=\"filteredLiveResultsData else noResults\">\n                            <div class=\"col-md-12 small text-left\" style=\"padding:0% 10%;\"  *ngFor=\"let userStory of filteredLiveResultsData\"> \n                                    I'm {{userStory.personaName}}, <br> When i'm {{userStory.location}} and {{userStory.situation}} <br/> \n                                    I want {{userStory.want}} in order to {{userStory.reason}} <br> \n                                    <span class=\"text-playOrange\">VOTE: {{userStory.vote}}</span>\n                                    <hr>\n                            </div>\n                    </ng-container>\n                    <ng-template #noResults ><div class=\"col-sm-12 small text-left\" style=\"padding:20px 10%;\"  >No results to show</div> </ng-template>\n                    <!-- Game Live Results -->\n\n                </div>\n                <button class=\"btn btn-block btn-danger btn-large\" *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'ready'\" (click)=\"lockGame()\">End game with these results</button>\n            </ng-container>\n        </div>\n        <!-- <app-game-admin-buttons (OnDiscard)=\"discardGame()\" (OnSave)=\"gameInstanceClosePostback()\"></app-game-admin-buttons> -->\n    </div>\n\n\n\n"

/***/ }),

/***/ 513:
/***/ (function(module, exports) {

module.exports = "\n<ng-container *ngIf=\"GAME_LIVE_STREAM\">\n<main id=\"content\" class=\"content bg-playGrape text-playSand\" role=\"main\" >\n<h1 class=\"page-title\">{{GAME_LIVE_STREAM.gameInstance.name}}</h1>\n\n\n<!-- BILLBOARD -->\n       <ng-container *ngIf=\"userType == 'billboard' else userScreen\" >\n            <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'active' && logonData.presentationID\">\n                    <!-- // The G slides must be hidden, rather than unloaded with *ngIf, so they hold their place in the presentation -->\n                      <div [ngClass]=\"{'hideSlides':SESSION_CONTROLLER_STREAM.activeGameInstance}\" class='embed-container embed-responsive embed-responsive-4by3'>\n                              <iframe [src]=\"slidesPresentationUrl\" frameborder=\"0\" width=\"1440\" height=\"839\" allowfullscreen=\"true\" mozallowfullscreen=\"true\" webkitallowfullscreen=\"true\"></iframe>\n                      </div>\n                      <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }.embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 105%; }.punch-viewer-content-fixed{width:100% !important; margin:0px !important}</style>\n            </ng-container>                  \n            <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'open'\">\n                <div class=\"slide\" style=\"color:white;\">\n                    <h1> TINDER </h1>\n                </div>\n            </ng-container>\n        </ng-container>\n\n\n<!-- GAME -->\n<ng-template #userScreen>\n        <div id=\"mainGameSlide container-flex\">\n                <div class=\"row strap play-gradient1\" *ngIf=\"screenController.screen == 0 && GAME_LIVE_STREAM.gameInputs.userStories && GAME_LIVE_STREAM.gameInstance.status == 'active' else showInstructions\">\n                        <div class=\"timerStrap align-items-center\" *ngIf=\"targetUserStory() && GAME_LIVE_STREAM.controller.timerOn\">\n                                <span>{{screenController.timeValue}}</span>\n                        </div>\n                        <!-- a hack to make scroll tot op work on mobile -->\n                        <script>$(yourSelector).addClass(\"androidFix\").scrollTop(0).removeClass(\"androidFix\")</script>\n                        <div class=\"col-md-12 strap-inner\">\n                                <ng-container *ngIf=\"targetUserStory()\">\n                                        <div class=\"col align-items-center text-interplaySand\" style=\"padding-top:20px;\">\n                                                        <div class=\"col inputgroup\">\n                                                                I'm <span class=\"emphasised\">{{targetUserStory().personaName}}</span> \n                                                        </div>\n                                                        <div class=\"col inputgroup\">\n                                                                When I'm <span class=\"emphasised\">{{targetUserStory().location}}</span>\n                                                        </div>\n                                                        <div class=\"col  inputgroup\">\n                                                                and feeling <span class=\"emphasised\">{{targetUserStory().situation}}</span>\n                                                        </div>\n                                                        <div class=\"col inputgroup\">\n                                                                <span class=\"emphasised\">I want to {{targetUserStory().want}}</span>\n                                                        </div>\n                                                        <div class=\"col  inputgroup\" >\n                                                                In order to ... <span class=\"emphasised\">{{targetUserStory().reason}}</span>\n                                                        </div>\n                                                        <div class=\"col  swipeIcon\"  (swipeleft)=\"swipe(idx, $event.type)\" (swiperight)=\"swipe(idx, $event.type)\">\n                                                                <span class=\"align-left text-interplayOrange\" style=\"float:left;opacity:.6\" [ngClass]=\"{'voteflash':screenController.buttonHilights.back}\" (click)=\"storyVote(0)\">OUT</span>\n                                                                <span class=\"align-right text-interplayOrange\" style=\"float:right;opacity:.6\" [ngClass]=\"{'voteflash':screenController.buttonHilights.next}\" (click)=\"storyVote(1)\">IN</span>\n                                                                <img src=\"./assets/img/swipeLeftRight.png\">\n                                                                <p>MAKE YOUR CHOICE BY SWIPING</p>\n                                                        </div>\n                                                </div>                                                                     \n                                </ng-container>\n                                <div class=\"col align-items-center text-interplaySand\" *ngIf=\"!targetUserStory()\"  >\n                                        <div class=\"section\" style=\"text-align:center;\">\n                                                <p> All done !</p>\n                                        </div>\n                                </div>\n                        </div>\n                </div>\n\n\n                <ng-template #showInstructions>\n                        <div class=\"row overlay bg-playSand\">\n                                <div class=\"section\">\n                                        <h2 class=\"text-playEggplant\">Story Tinder is a quick cull of the lower priorities.</h2>\n                                </div>\n                                <div style=\"background-image:linear-gradient(rgba(20,20,20, 1), rgba(20,20,20, .5)),url(assets/img/example.png)\" class=\"section\">\n                                        <h1>How to play story Tinder</h1>\n                                        <p><img class=\"gameHowToImg\" src=\"/assets/img/introScreens/tinderIntro.png\"></p>\n                                </div>\n                                <div class=\"section bg-playOrange\">\n                                        <h2 class=\"text-playSand\">Our user statments are done but unfortunately not all of them will be so relevant to your product. Like Tinder, simply swipe towards the 'YES' or 'NO' to choose the stories you like. <br/><br/>\n                                        Press play below to begin.</h2>\n                                </div>\n                            </div>\n                </ng-template>\n\n                <!-- FOOTER -->\n                <app-game-footer (OnClickPlay)=\"changeScreen(0)\" [screenController]=\"screenController\" [GameStream]=\"GAME_LIVE_STREAM\" ></app-game-footer>\n        </div>\n</ng-template>\n\n    \n\n\n</main>\n</ng-container>\n\n<ng-template #loader>\n\n</ng-template>"

/***/ }),

/***/ 514:
/***/ (function(module, exports) {

module.exports = "\n\n<ng-container *ngIf=\"GAME_LIVE_STREAM\">\n\n        <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status != 'closed' else showResults\"> <!--cked if the game has been completed and locked, is so then show the results !-->\n            <!-- ADMIN SCREEN -->\n            <ng-container *ngIf=\"userType == 'admin' else gameScreen\">\n                <ng-container *ngIf=\"GAME_LIVE_STREAM.gameInstance.status == 'waiting input data' || screenController.screen == 'gameData'; else gameController\">\n                        <!-- CREATE/EDIT GAME DATA-->\n                        <div class=\"container-flex editGameData bg-playEggplant text-white\">\n                                <div class=\"row\">\n                                    <div class=\"btn-group btn-block\" style=\"margin-bottom:20px;\">\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromOSession()\">Session Store</button>\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromOW()\">Overwatch data</button>\n                                        <button class=\"btn col btn-success\" (click)=\"loadFromGame()\">Game data</button>\n                                    </div>\n                                    <h1 class=\"text-center text-playOrange\"> GAME INPUTS</h1>\n                                    <div class=\"col-md-12\">\n                                        <h3 style=\"margin-top:30px;border-bottom:thin solid white;\">Game details</h3>\n                                        <p><strong>Name:</strong> {{GAME_LIVE_STREAM.gameInstance.name}}</p>\n                                        <p><strong>Template:</strong> {{GAME_LIVE_STREAM.gameInstance.gameTemplate}}</p>\n                                        <p><strong>Description:</strong>  {{GAME_LIVE_STREAM.gameInstance.description}}</p>\n                                    </div>\n                                    <div class=\"col-md-12 text-playSand\">\n                                                <!-- CUSTOM GAME SPAWN CONTROLLER FIELDS -->\n                                                <div class=\"input-section\">\n                                                    <h4 class=\"text-playOrange\">User Stories</h4>\n                                                    <div *ngFor=\"let userStory of spawnGameData.gameInputs.userStories; let i = index\" class=\"story text-playSand bg-playEggplant\">  \n                                                        <span class=\"btn-remove badge\" (click)=\"removeFromGameData(i, 'userStories')\">remove</span>\n                                                        I'm <i class=\"text-playOrange\">{{userStory.personaName}}</i>, <br>\n                                                        When i'm  <i class=\"text-playOrange\">{{userStory.location}}</i> and \n                                                                <i class=\"text-playOrange\">{{userStory.situation}}, </i><br>\n                                                        I want to <i class=\"text-playOrange\">{{userStory.want}}</i> <br>\n                                                        in order to <i class=\"text-playOrange\">{{userStory.reason}}</i> <br>\n                                                    </div>\n                                                </div>\n                                                <!-- CUSTOM GAME SPAWN CONTROLLER FIELDS -->\n                                    </div>\n                                    <div class=\"btn-group btn-block\" style=\"margin-top:20px;\">\n                                        <button class=\"btn btn-warning col\" (click)=\"updateGameData()\">Save to game</button>\n                                        <button class=\"btn btn-danger col\" (click)=\"screenController.screen = null;\">Cancel</button>\n                                    </div>\n                                </div>\n                            </div>\n                </ng-container>\n                <ng-template #gameController>\n                        <button class=\"btn btn-block btn-playEggplant\" style=\"border-radius:none;\" (click)=\"editGameData()\">Update Game Inputs</button>\n                        <GAME03controller [GameLive]=\"GAME_LIVE_STREAM\"></GAME03controller>\n                </ng-template>\n            </ng-container>\n\n            <!-- MAIN GAME SCREEN LOADER  -->\n            <ng-template #gameScreen>\n                    <GAME03minigame [GameLive]=\"GAME_LIVE_STREAM\" (gameInstanceEnd)=\"saveUserResults($event)\" [userType]=\"userType\" (saveUserInput)=\"null\"></GAME03minigame>\n            </ng-template>\n\n</ng-container>\n\n<!-- RESULTS REPORT --> <!-- (game locked container) -->\n<ng-template #showResults>\n        <div class=\"container-flex gameReport\">\n            <div class=\"btn-group btn-block actionButtons\" style=\"margin-bottom:20px;\" *ngIf=\"userType == 'admin'\">\n                <button class=\"btn col bg-danger text-white\" (click)=\"exitGame()\">Exit Game</button>\n                <button class=\"btn btn-warning col\" (click)=\"saveSessionData()\">Add to Session Store</button>\n            </div>\n            <h1 class=\"text-white text-center text-playSand\"> GAME RESULTS</h1>\n            <div class=\"col-md-12 text-playSand\" style=\"margin-top:10px;border-top:thin solid grey;padding:10px;\" *ngFor=\"let addedUserStory of processRawResultsData(GAME_LIVE_STREAM.gameResults)\">\n                    <blockquote>\n                    I'm <i class=\"text-playOrange\">{{addedUserStory.personaName}}</i>, <br>\n                    When i'm {{addedUserStory.location}} <br>\n                    and {{addedUserStory.situation}}, <br>\n                    I want to <i class=\"text-playOrange\">{{addedUserStory.want}}</i> <br>\n                    in order to <i class=\"text-playOrange\">{{addedUserStory.reason}}</i> <br>\n                    <span class=\"text-playOrange\">vote: {{addedUserStory.vote}}</span>\n                    </blockquote>\n            </div>\n            <div style=\"clear:both;\"></div>\n            <button *ngIf=\"userType == 'admin'\" class=\"btn btn-warning btn-block\" (click)=\"saveProjectData()\">Add to Overwatch Data</button>\n        </div>\n</ng-template>\n\n"

/***/ }),

/***/ 515:
/***/ (function(module, exports) {

module.exports = "<ng-container *ngIf=\"screenController.screen != 'gameInstructions' else startGame\">\n        <div id=\"screenFooter\" class=\"page-footer\">\n            <p>GAME IN PROGRESS</p>\n            <h1>PLAY</h1>\n        </div>\n    </ng-container>\n    <ng-template #startGame>\n        <div *ngIf=\"GameStream.gameInstance.status == 'active' else waitingStart\" class=\"page-footer btn-play bg-gradient\">\n            <h1 (click)=\"playClick()\">PLAY</h1>\n        </div>\n        <ng-template #waitingStart>\n                <div id=\"screenFooter\" class=\"page-footer\">\n                        <p>WAITING FOR GAME TO START</p>\n                    </div>\n        </ng-template>\n    </ng-template>\n"

/***/ }),

/***/ 516:
/***/ (function(module, exports) {

module.exports = "\n \n <div class=\"fullscreenSplash\">\n            <h2> CREATE A NEW GAME </h2>\n            <input style=\"margin:30px 0px;width:80%;text-align:center\" #gameTitle placeholder=\"Enter your game title\" >\n            <div class=\"alert alert-danger\" style=\"width:80%;text-align:center;margin:0px auto;\" *ngIf=\"titleWarning\">Enter a title for your game</div>\n\n            <h4  style=\"margin-top:30px;\"> Select a game to create</h4>\n\n\n            <!-- GAME CARD USER STORY POEMS -->\n            <div class=\"card\" (click)=\"GAME01(gameTitle.value)\">\n                <div class=\"card-body align-items-center text-center\">\n                    <div class=\"col col-md-12\"> \n                        <h3>User Story Poems</h3> \n                        <i class=\"small\">Define user stories for each persona using a poem format. Work sell with user persona cards.</i>\n                        <p><strong>GROUP SIZE: 4-10</strong></p>\n                        <p class=\"small\">INPUTS: User Personas, User Situations, User Locations</p>\n                    </div>\n                </div>                \n            </div>\n\n            <!-- GAME CARD USER STORY TINDER -->\n            <div class=\"card\" (click)=\"GAME03(gameTitle.value)\">\n                    <div class=\"card-body align-items-center text-center\">\n                        <div class=\"col col-md-12\"> \n                            <h3>User Story Tinder</h3> \n                            <i class=\"small\">Quickly get the top user stories from a biulk list. Good for gaining focus on a few stories after a divergent user story creation game.</i>\n                            <p><strong>GROUP SIZE: any</strong></p>\n                            <p class=\"small\">INPUTS: User Stories</p>\n                        </div>\n                    </div>                \n            </div>\n\n            <!-- GAME CARD USER STORY SILENT AUCTION -->\n            <div class=\"card\" (click)=\"GAME05(gameTitle.value)\">\n                <div class=\"card-body align-items-center text-center\">\n                    <div class=\"col col-md-12\"> \n                        <h3>User Story Silent Auction</h3> \n                        <i class=\"small\">A value-based pririortisation od user stories which forces each user to weight their commitment towards on core item..</i>\n                        <p><strong>GROUP SIZE: more than 4</strong></p>\n                        <p class=\"small\">INPUTS: User Stories</p>\n                    </div>\n                </div>                \n            </div>\n\n            <!-- GAME CARD RFID ROADMAP EXPLORER -->\n            <div class=\"card\" (click)=\"MAXI01(gameTitle.value)\">\n                    <div class=\"card-body align-items-center text-center\">\n                        <div class=\"col col-md-12\"> \n                            <h3>RFID: Roadmap Explorer</h3> \n                            <i class=\"small\">Using the PLAY controller, this game allows exploration of roadmaps and workflow diagrams on a organisational level.</i>\n                            <p><strong>GROUP SIZE: any</strong></p>\n                            <p class=\"small\">INPUTS: Printed game-enabled roadmap, roadmap hotspot infosheets, PLAY controller</p>\n                        </div>\n                    </div>                \n                </div>\n\n            <button class=\"btn btn-block cancelBtn btn-danger text-white\" (click)=\"cancel()\"> Cancel</button>\n </div>\n                       "

/***/ }),

/***/ 517:
/***/ (function(module, exports) {

module.exports = "<ng-container *ngIf=\"service.logonData && SESSION_LIVE_STREAM\">\n    <ng-container *ngIf=\"service.userType == 'billboard' || service.userType == 'user' else adminHoldingScreen\">\n            <div class=\"fullscreenSplash\">\n                <h1>PLAY</h1>\n                <span class=\"gameTitle\"> {{service.logonData.accessKeyMessage}} </span>\n                <div class=\"loader\"></div>\n                <p>Waiting for the game to start...</p>\n            </div>\n    </ng-container>\n\n    <ng-template #adminHoldingScreen>\n            <div class=\"fullscreenSplash\" *ngIf=\"!showSpawnScreen\">\n                    <h1>PLAY</h1>\n                    <span class=\"gameTitle\">{{service.logonData.accessKeyMessage}} </span>\n                    <div class=\"loader\"></div>\n            \n                    <div class=\"container-flex row\">\n                        <ng-container *ngIf=\"SESSION_LIVE_STREAM else loading\">\n                            <div class=\"btn-group btn-block\">\n                                <button class=\"btn btn-block bg-playOrange\" style=\"margin: 10px 0px;\" (click)=\"showUtilityLoader = true\">Open Utility</button>\n\n                                <button class=\"btn btn-block btn-success\" style=\"margin: 10px 0px;\" (click)=\"showSpawnScreen = true\">Create Game</button>\n                            </div>\n                    \n                            <ng-container *ngIf=\"gamesForSession else noPreviousGames\">\n                                <div class=\"gameCard bg-white text-black\" *ngFor=\"let gameInstance of gamesForSession\">\n                                    <div class=\"card-body align-items-center text-center\">\n                                        <h3>{{gameInstance.gameInstance.name}}</h3>\n                                        <i>{{gameInstance.gameInstance.status}}</i>\n                                        <p> Template: {{gameInstance.gameInstance.gameTemplate}}\n                                        <p class=\"small\">{{gameInstance.gameInstance.description}}</p>\n                                        <button  class=\"btn-success btn\" style=\"margin:0px auto;\" *ngIf=\"SESSION_LIVE_STREAM.activeGameInstance != gameInstance._id else activeGame\" (click)=\"updateActivegameInstance(gameInstance._id)\">ACTIVATE</button>\n                                                <!-- <button style=\"width:20%;\"class=\"btn-danger btn right\" *ngIf=\"SESSION_LOCAL .activeGameInstance != gameInstance.instanceID\"><span class=\"fa fa-chain-broken\"></span></button> -->\n                                        <ng-template #activeGame><span class=\"btn btn-success\"> GAME ACTIVE</span></ng-template>\n                                    </div>                \n                                </div>\n                            </ng-container>\n                            <ng-template #noPreviousGames><span class=\"col-md-12\">No previous games avaialble</span></ng-template>\n                        </ng-container>\n                        <ng-template #loading>Loading ...</ng-template>\n                    </div>\n            </div>\n            <game-spawner *ngIf=\"showSpawnScreen\" [sessionID]=\"SESSION_LIVE_STREAM._id\" [projectID]=\"SESSION_LIVE_STREAM.OWProjectID\" (closeMe)=\"showSpawnScreen = false\"></game-spawner>\n\n            <utility-loader *ngIf=\"showUtilityLoader\" (closeMe)=\"showUtilityLoader = false\"></utility-loader>\n    </ng-template>\n</ng-container>"

/***/ }),

/***/ 518:
/***/ (function(module, exports) {

module.exports = "\n<ng-container *ngIf=\"!service.logonData else loading\">\n<div class=\"slide container-flex play-gradient1\">\n  <div class=\"loginHeader\">\n    <h1>PLAY</h1>\n  </div>\n\n  <div class=\"row align-items-center\" id=\"loginForm\" style=\"padding-top:20%\">\n    <div class=\"col-sm-12 inputgroup\">\n      <input type=\"text\" style=\"text-align:center\" (keyup.enter)=\"doLogin(accessKey.value)\" [disabled]=\"validationInProgress\" class=\"form-control border-gradient\" id=\"accessKey\" #accessKey placeholder=\"Access Key\">\n      <button style=\"margin-top:20px;\" class=\"btn bg-gradient\" [disabled]=\"validationInProgress\" (click)=\"doLogin(accessKey.value)\">OPEN SESSION</button>\n      <p *ngIf=\"validationInProgress\">Transferring you to the session</p>\n      <p class=\"bg-danger text-white\" *ngIf=\"loginFailure\">Access key not found</p>\n    </div>\n  </div>\n</div>\n</ng-container>\n\n<ng-template #loading>\n    <div class=\"fullscreenSplash holding\">\n      <h1>PLAY</h1><br/>\n      <span *ngIf=\"service.logonData\" class=\"gameTitle\">{{service.logonData.accessKeyMessage}}</span>\n      <div class=\"loader\"></div>\n    </div>\n</ng-template>"

/***/ }),

/***/ 519:
/***/ (function(module, exports) {

module.exports = "\n <div class=\"overlay\">\n <div class=\"fullscreenSplash\">\n            <h2> OPEN A WORLD UTILITY </h2>\n\n            <h4  style=\"margin-top:30px;\"> Select a utility to open</h4>\n\n            <!-- UTILLITY PERSONAS -->\n            <div class=\"card\" (click)=\"this.router.navigate(['/utillities/personas'])\">\n                <div class=\"card-body align-items-center text-center\">\n                    <div class=\"col col-md-12\"> \n                        <h3>User Persona Mangement</h3> \n                        <i class=\"small\">Manage the available personas in the current world.</i>\n                        <p><strong>USES: Play data</strong></p>\n                    </div>\n                </div>            \n            </div>\n\n            <!-- UTILLITY RISKS -->\n            <div class=\"card\"  (click)=\"this.router.navigate(['/utillities/risks'])\">\n                <div class=\"card-body align-items-center text-center\">\n                    <div class=\"col col-md-12\"> \n                        <h3>Risk Actor Mangement</h3> \n                        <i class=\"small\">Manage the risks in the current world.</i>\n                        <p><strong>USES: Play data</strong></p>\n                    </div>\n                </div>            \n            </div>\n\n            <!-- UTILLITY GOALS -->\n            <div class=\"card\"  (click)=\"this.router.navigate(['/utillities/goals'])\">\n                <div class=\"card-body align-items-center text-center\">\n                    <div class=\"col col-md-12\"> \n                        <h3>Goal  Mangement</h3> \n                        <i class=\"small\">Manage the goals in the current world.</i>\n                        <p><strong>USES: Play data</strong></p>\n                    </div>\n                </div>            \n            </div>\n            \n\n            <button class=\"btn btn-block cancelBtn btn-danger text-white\" (click)=\"cancel()\"> Cancel</button>\n </div>\n </div>"

/***/ }),

/***/ 520:
/***/ (function(module, exports) {

module.exports = "\n<ng-container *ngIf=\"PROJECT\">\n<main id=\"content\" class=\"content bg-playSand text-playEggplant\" role=\"main\" >\n    <h1 class=\"page-title\">GOAL UTILITY</h1>\n    <div class=\"section\">\n            <div class=\"col col-md-12\" style=\"margin-bottom:5px;\" *ngFor=\"let goal of PROJECT.data.goals; let i = index\">\n                    <blockquote>\n                        <div class=\"title-block\">\n                            <h2> {{goal.name}}</h2>\n                        </div>\n                        <p>{{ goal.description}} </p>\n\n                        <a class=\"badge bg-playEggplant text-playSand\" target=\"_blank\" [href]=\"goal.documentUrl\">Open documentation page</a>\n                        \n                    </blockquote>\n                    <button class=\"btn bg-danger text-white\" (click)=\"removeItem(i)\"> REMOVE </button>\n                    <button class=\"btn bg-playEggplant text-playSand pull-right\" (click)=\"showEdit(goal, i)\"> EDIT </button>\n                    <hr>\n            </div>\n            <button style=\"text-align:center\" class=\"btn bg-playEggplant text-playSand btn-action\" (click)=\"addNewItem()\"> ADD NEW GOAL</button>\n            <hr>\n            <button [disabled]=\"saveinProgress\" (click)=\"saveToProject()\" style=\"margin-bottom:20px;\" class=\"btn btn-action col col-sx-12\">SAVE TO PROJECT DATA</button>\n            <!-- <button (click)=\"editPersonaShow = false\" style=\"margin-bottom:20px;\" class=\"btn btn-action btn-danger col col-sx-12 small\">CLOSE</button> -->\n\n        </div>\n</main>\n</ng-container>\n\n\n<div class=\"fullscreenSplash overlay\" *ngIf=\"editShow\">\n<form [formGroup]=\"Form\" (ngSubmit)=\"updateItem(targetItem.index,$event)\">\n        <div class=\"inputgroup\" >\n            <p>Name\n                <input required style=\"width:100%;\" formControlName=\"name\">\n            </p>\n        </div>\n\n        <div class=\"inputgroup\" >\n                <p>Description:\n                    <input required style=\"width:100%;\"  formControlName=\"description\">\n                </p>\n        </div>\n        <div class=\"inputgroup\" >\n                <p>Docuemntation Link\n                    <input required  style=\"width:100%;\" formControlName=\"documentation\">\n                </p>\n        </div>\n\n        <button type=\"submit\" class=\"btn btn-action col col-sx-12\" style=\"margin-bottom:20px;\" [disabled]=\"!Form.valid\">\n                <ng-container style=\"text-transform: uppercase;\">\n                    Update\n                </ng-container>\n        </button>\n\n    </form>\n</div>\n\n<ng-template #loader>\n    loading...\n</ng-template>"

/***/ }),

/***/ 521:
/***/ (function(module, exports) {

module.exports = "\n<ng-container *ngIf=\"PROJECT\">\n<main id=\"content\" class=\"content bg-playSand text-playEggplant\" role=\"main\" >\n    <h1 class=\"page-title\">PERSONAS UTILLITY</h1>\n    <div class=\"section\">\n            <div class=\"col col-md-6\" style=\"margin-bottom:5px;\" *ngFor=\"let persona of PROJECT.data.personas; let i = index\">\n                    <blockquote>\n                        <h1 style=\"font-sixe:3em;text-align:center;text-transform:uppercase;font-weight:bold;margin-top:1em;\">\n                            {{persona.name}}\n                        </h1>\n                        <p><img class=\"avatar\" [src]=\"persona.avatarUrl\"></p>\n                        <p>{{persona.excerpt}}</p>\n                        <p>Documentation Link: {{persona.documentationUrl}}</p>\n                        <button class=\"btn bg-danger text-white\" (click)=\"removePersona(i)\"> REMOVE </button>\n                        <button class=\"btn bg-playEggplant text-playSand pull-right\" (click)=\"showEdit(persona, i)\"> EDIT </button>\n                    </blockquote>\n                    <hr>\n            </div>\n            <button style=\"text-align:center\" class=\"btn bg-playEggplant text-playSand btn-action\" (click)=\"addNewPersona()\"> ADD NEW PERSONA</button>\n            <hr>\n            <button [disabled]=\"saveinProgress\" (click)=\"saveToProject()\" style=\"margin-bottom:20px;\" class=\"btn btn-action col col-sx-12\">SAVE TO PROJECT DATA</button>\n            <!-- <button (click)=\"editPersonaShow = false\" style=\"margin-bottom:20px;\" class=\"btn btn-action btn-danger col col-sx-12 small\">CLOSE</button> -->\n\n        </div>\n</main>\n</ng-container>\n\n\n<div class=\"fullscreenSplash overlay\" *ngIf=\"editPersonaShow\">\n<form [formGroup]=\"Form\" (ngSubmit)=\"updatePersona(targetPersona.index,$event)\">\n        <div class=\"inputgroup\" >\n            <p>Name\n                <input required style=\"width:100%;\" formControlName=\"name\">\n            </p>\n        </div>\n\n        <div class=\"inputgroup\" >\n                <p>Avatar image URL\n                    <input required rows=\"4\" style=\"width:100%;\" formControlName=\"avatarUrl\">\n                </p>\n        </div>\n\n        <div class=\"inputgroup\" >\n                <p>Description\n                    <input required rows=\"4\" style=\"width:100%;\" formControlName=\"excerpt\">\n                </p>\n        </div>\n\n        <div class=\"inputgroup\" >\n                <p>Docuemntation Link\n                    <input required rows=\"4\" style=\"width:100%;\" formControlName=\"documentation\">\n                </p>\n        </div>\n\n        <button type=\"submit\" class=\"btn btn-action col col-sx-12\" style=\"margin-bottom:20px;\" [disabled]=\"!Form.valid\">\n                <ng-container style=\"text-transform: uppercase;\">\n                    Update\n                </ng-container>\n        </button>\n\n    </form>\n</div>\n\n<ng-template #loader>\n    loading...\n</ng-template>"

/***/ }),

/***/ 522:
/***/ (function(module, exports) {

module.exports = "\n<ng-container *ngIf=\"PROJECT\">\n<main id=\"content\" class=\"content bg-playSand text-playEggplant\" role=\"main\" >\n    <h1 class=\"page-title\">RISK ACTOR UTILITY</h1>\n    <div class=\"section\">\n            <div class=\"col col-md-12\" style=\"margin-bottom:5px;padding: 40px;border-bottom: thin solid grey;\" *ngFor=\"let risk of PROJECT.data.actors.risks; let i = index\">\n                    <blockquote>\n                        <div class=\"title-block\">\n                            <span style=\"text-transform:uppercase;font-size:1.2em\">\n                                    {{risk.name}}\n                            </span>\n                        </div>\n                        <span class=\"badge bg-playSand text-playEggplant\" style=\"margin-right:10px;\">\n                                <img class=\"avatar\" [src]=\"risk.type.avatarUrl\">\n                                <span class=\"avatar-title\">{{risk.type.name}}</span>\n                        </span>\n                        <p>\n                            Probability:  {{ RiskToSatement(risk.probability)}}  |  \n                            Impact: {{ RiskToSatement(risk.impact)}}\n                        </p>\n                        <p>\n                            <a class=\"badge bg-playEggplant text-playSand\" target=\"_blank\" [href]=\"risk.documentUrl\">Open documentation page</a>\n                        </p>\n                        \n                    </blockquote>\n                    <button class=\"btn bg-danger text-white\" (click)=\"removeItem(i)\"> REMOVE </button>\n                    <button class=\"btn bg-playEggplant text-playSand pull-right\" (click)=\"showEdit(risk, i)\"> EDIT </button>\n            </div>\n            <div class=\"col col-md-12 bg-playEggplant text-playSand\" style=\"padding:20px;\">\n\n                <button style=\"text-align:center\" class=\"btn bg-playSand text-playEggplant btn-block btn-action\" (click)=\"addNewItem()\" style=\"margin-bottom:20px;\"> ADD NEW RISK</button>\n\n                <button [disabled]=\"saveinProgress\" (click)=\"saveToProject()\" style=\"margin-bottom:20px;\" class=\"btn btn-action btn-block\">SAVE TO PROJECT DATA</button>\n\n                <!-- <button (click)=\"editPersonaShow = false\" style=\"margin-bottom:20px;\" class=\"btn btn-action btn-danger col col-sx-12 small\">CLOSE</button> -->\n            </div>\n        </div>\n</main>\n</ng-container>\n\n\n<div class=\"fullscreenSplash overlay\" *ngIf=\"editShow\">\n<form [formGroup]=\"Form\" (ngSubmit)=\"updateItem(targetItem.index,$event)\">\n        <div class=\"inputgroup\" >\n            <p>Name\n                <input required style=\"width:100%;\" formControlName=\"name\">\n            </p>\n        </div>\n\n        <div class=\"inputgroup\" >\n                <p>Probability: {{RiskToSatement(Form.controls.probability.value)}}\n                    <input required style=\"width:100%;\" class=\"slider\" type=\"range\" min=\"1\" max=\"5\" step=\"1\" value=\"2\"  formControlName=\"probability\">\n                </p>\n        </div>\n        <div class=\"inputgroup\" >\n                <p>Impact: {{RiskToSatement(Form.controls.impact.value)}}\n                    <input required style=\"width:100%;\" class=\"slider\" type=\"range\" min=\"1\" max=\"5\" step=\"1\" value=\"2\" formControlName=\"impact\">\n                </p>\n        </div>\n\n        <div class=\"inputgroup\" >\n            <p> Linked risk type </p>\n            <select formControlName=\"linkedRiskType\"  style=\"width:100%;text-align:center;\" >\n                    <option style=\"width:100%;text-align:center;\" *ngFor=\"let artifact of riskTypeArtifacts.artifacts\" value=\"{{artifact.name}}\"  >{{artifact.name}}</option>\n            </select> \n        </div>\n\n        <div class=\"inputgroup\" >\n                <p>Docuemntation Link\n                    <input required rows=\"4\" style=\"width:100%;\" formControlName=\"documentation\">\n                </p>\n        </div>\n\n        <button type=\"submit\" class=\"btn btn-action col col-sx-12\" style=\"margin-bottom:20px;\" [disabled]=\"!Form.valid\">\n                <ng-container style=\"text-transform: uppercase;\">\n                    Update\n                </ng-container>\n        </button>\n\n    </form>\n</div>\n\n<ng-template #loader>\n    loading...\n</ng-template>"

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angular2_uuid__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angular2_uuid___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_angular2_uuid__);
/* unused harmony export RFIDhotspot */
/* unused harmony export gameInstance */
/* unused harmony export gameInstanceDetails */
/* unused harmony export sessionInstance */
/* unused harmony export projectData */
/* unused harmony export OW_actors */
/* unused harmony export OW_epic */
/* unused harmony export OW_activity */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return OW_userStory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OW_goal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return OW_risk; });
/* unused harmony export OW_risk_type */
/* unused harmony export OW_hero */
/* unused harmony export OW_hero_type */
/* unused harmony export OW_complexity */
/* unused harmony export OW_skill */
/* unused harmony export OW_Breakthrough */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return OW_persona; });
/* unused harmony export dataModels */

var RFIDhotspot = (function () {
    function RFIDhotspot(RFIDcardID) {
        this.RFIDcardID = RFIDcardID;
    }
    return RFIDhotspot;
}());

var gameInstance = (function () {
    function gameInstance() {
    }
    return gameInstance;
}());

var gameInstanceDetails = (function () {
    function gameInstanceDetails() {
    }
    return gameInstanceDetails;
}());

var sessionInstance = (function () {
    function sessionInstance(sessionName, projectID, sessionID) {
        this.status = "New";
        this.activeGameInstance = null;
        this.activeSessionPart = null;
        this.loading = false;
        this.sessionTemplate = "";
        this.status = "New";
        this.sessionID = sessionID;
        this.projectID = projectID;
    }
    return sessionInstance;
}());

//   export class project{
// 	public projectStatus:string = "Open";
//     public jiraProjectID:string = "";
//     public jiraInstanceUrl:string = "";
//     public clinetname:string = "";
//     public sessionDataUpdated:string = ";" //unix time of last session data updated in the database
//     public data:projectData;
// 	constructor(jiraProjectID){
// 		this.jiraProjectID = jiraProjectID;
// 	}
// }
var projectData = (function () {
    function projectData() {
        this.personas = [];
        this.emotions = [];
        this.situations = [];
        this.userstories = [];
    }
    //User story functions
    projectData.prototype.addUserStory = function (newStory) {
        this.userstories.push(newStory);
    };
    projectData.prototype.addStories = function (newStories) {
        var _this = this;
        newStories.map(function (newStory) {
            _this.userstories.push(newStory);
        });
    };
    projectData.prototype.updateStory = function (userStory) {
        var result = this.userstories.filter(function (story) { return story.ID == userStory.ID; })[0] = userStory;
        return result;
    };
    projectData.prototype.linkStorytoJira = function (userStory) {
        var result = this.userstories.filter(function (story) { return story.ID == userStory.ID; })[0].jiraTicketLink = userStory.jiraTicketLink;
        return result;
    };
    return projectData;
}());

var OW_actors = (function () {
    function OW_actors() {
    }
    return OW_actors;
}());

// OW DATA SETS //
// JIRA linkins
var OW_epic = (function () {
    function OW_epic() {
        this.linkedProjectGoals = [];
        this.budget = 0;
    }
    return OW_epic;
}());

var OW_activity = (function () {
    function OW_activity(name) {
        this.daysToComplete = 0;
        this.priority = 0;
        this.effortHours = 0;
        this.name = name;
    }
    return OW_activity;
}());

var OW_userStory = (function () {
    function OW_userStory() {
        this.projectID = "";
        this.want = "";
        this.reason = "";
        this.location = "";
        this.situation = "";
        this.investment = 0;
        this.vote = 0;
        this.personaName = "";
        this.ID = null;
        this.theme = "No Theme";
        this.ID = __WEBPACK_IMPORTED_MODULE_0_angular2_uuid__["UUID"].UUID();
    }
    return OW_userStory;
}());

//PLAY specific datasets
var OW_goal = (function () {
    function OW_goal(ID, name) {
        this.description = "";
        this.document = "#";
        this.linkedRisks = [];
        this.linkedSkills = [];
        this.linkedEpics = [];
        this._id = ID;
        this.name = name;
    }
    return OW_goal;
}());

var OW_risk = (function () {
    function OW_risk(ID, name, type) {
        this.probability = 0;
        this.impact = 0;
        this.document = "#";
        this._id = ID;
        this.name = name;
        this.type = type;
    }
    return OW_risk;
}());

var OW_risk_type = (function () {
    function OW_risk_type(name, avatarUrl) {
        this.name = name;
        this.avatarUrl = avatarUrl;
    }
    return OW_risk_type;
}());

var OW_hero = (function () {
    function OW_hero(ID, name, type) {
        this._id = ID;
        this.name = name;
        this.type = type;
    }
    return OW_hero;
}());

var OW_hero_type = (function () {
    function OW_hero_type(name, avatarUrl) {
        this.name = name;
        this.avatarUrl = avatarUrl;
    }
    return OW_hero_type;
}());

var OW_complexity = (function () {
    function OW_complexity(name) {
        this.skills = [];
        this.pmMethodologyIndex = 0;
        this.companySizeindex = 0;
        this.daysToCompletion = 0;
        this.complexityMatrix = 0;
        this.name = name;
    }
    return OW_complexity;
}());

var OW_skill = (function () {
    function OW_skill(name, ID) {
        this.resourcedBy = null;
        this.cost = 0;
        this.availabilityHrs = 0;
        this.vote = 0;
        this.ID = null;
        this.name = name;
        this.ID = ID;
    }
    return OW_skill;
}());

var OW_Breakthrough = (function () {
    function OW_Breakthrough(name, ID) {
        this.ID = null;
        this.linkedEpics = [];
        this.linkedProjectGoals = [];
        this.complexity = [];
        this.activities = [];
        this.skills = [];
        this.investment = 0;
        this.name = name;
        this.ID = ID;
    }
    return OW_Breakthrough;
}());

var OW_persona = (function () {
    function OW_persona(ID, name) {
        this.imageUrl = "";
        this.ID = ID;
        this.name = name;
    }
    return OW_persona;
}());

var dataModels = (function () {
    function dataModels() {
    }
    return dataModels;
}());

//# sourceMappingURL=dataModels.js.map

/***/ }),

/***/ 804:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(299);


/***/ })

},[804]);
//# sourceMappingURL=main.bundle.js.map